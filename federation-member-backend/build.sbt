import scala.sys.process.Process

ThisBuild / version           := "0.1.0-SNAPSHOT"
ThisBuild / scalaVersion      := "3.4.0"
ThisBuild / semanticdbEnabled := true
ThisBuild / scalafixOnCompile := true
ThisBuild / scalacOptions ++= Seq(
  "-deprecation",
  "-explain",
  "-explain-types",
  "-feature",
  "-new-syntax",
  "-print-lines",
  "-unchecked",
  "-Ykind-projector",
  "-Xmigration",
  "-rewrite",
  "-Wunused:all"
)

resolvers += ProjectResolvers.consenSys
resolvers += ProjectResolvers.jitpack
resolvers += ProjectResolvers.libP2pRepo

libraryDependencies ++= Seq(
  Dependencies.http4sEmber,
  Dependencies.http4sDsl,
  Dependencies.http4sCirce,
  Dependencies.directoryWatcher,
  Dependencies.docker,
  Dependencies.dockerTransport,
  Dependencies.logback,
  Dependencies.sttp,
  Dependencies.trileBackendCommons,
  Dependencies.typeSafeConfig,
  Dependencies.typeSafeScalaLogging
)

lazy val ensureDockerBuildx    = taskKey[Unit]("Ensure that docker buildx configuration exists")
lazy val dockerBuildWithBuildx = taskKey[Unit]("Build docker images using buildx")
lazy val dockerBuildxSettings = Seq(
  ensureDockerBuildx := {
    if (Process("docker buildx inspect multiplatform").! == 1) {
      Process("docker buildx create --use --name multiplatform", baseDirectory.value).!
    }
  },
  dockerBuildWithBuildx := {
    streams.value.log("Building and pushing image with Buildx")
    dockerAliases.value.foreach(alias =>
      Process(
        "docker buildx build --platform=linux/arm64,linux/amd64 --push -t " +
          alias + " .",
        baseDirectory.value / "target" / "docker" / "stage"
      ).!
    )
  },
  Docker / publish := Def
    .sequential(
      Docker / publishLocal,
      ensureDockerBuildx,
      dockerBuildWithBuildx
    )
    .value
)

Docker / packageName := "triledotlink/federation-member-backend"
dockerExposedPorts ++= Seq(9999)

lazy val trileFederationMemberBackend = (project in file("."))
  .settings(
    name                := "trile-federation-member-backend",
    Compile / mainClass := Some("acab.devcon0.trile.boot.Main"),
    dockerBaseImage     := "openjdk:21-jdk"
  )
  .settings(dockerBuildxSettings)
  .enablePlugins(JavaAppPackaging)
  .enablePlugins(DockerPlugin)
