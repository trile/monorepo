package acab.devcon0.trile.domain.ports.input

import acab.devcon0.trile.cqrs.Command
import acab.devcon0.trile.cqrs.CommandHandler
import acab.devcon0.trile.cqrs.Event
import acab.devcon0.trile.domain.dtos.CheckInStatus
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

final case class CheckInAckCommand(checkInStatus: CheckInStatus) extends Command[String]

sealed abstract class CheckInAckEvent[T]                    extends Event[T]
final case class CheckInAckErrorEvent(throwable: Throwable) extends CheckInAckEvent[Unit]
final case class CheckInAckSuccessEvent()                   extends CheckInAckEvent[Unit]

trait CheckInAckCommandHandler extends CommandHandler[CheckInAckCommand, String, CheckInAckEvent[?]]

object CheckInAckCommandImplicits {
  implicit class CheckInAckCommandEventFlattenOps(result: IO[CheckInAckEvent[?]]) {
    private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

    def flattenEvents: IO[Unit] = {
      result.flatMap {
        case CheckInAckSuccessEvent()        => IO.unit
        case CheckInAckErrorEvent(throwable) => handleErrorCase(throwable)
      }
    }

    private def handleErrorCase(exception: Throwable): IO[Unit] = {
      logger.error(s"CheckInAckCommandHandler exception=$exception") >>
        IO.raiseError[Unit](exception)
    }
  }
}
