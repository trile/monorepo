package acab.devcon0.trile.domain.adapters
import java.time.Instant

import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberHeartbeat
import acab.devcon0.trile.domain.ports.input._
import acab.devcon0.trile.domain.service.IpfsClusterService
import acab.devcon0.trile.domain.service.IpfsService
import acab.devcon0.trile.domain.service.P2pService
import cats.effect.IO
import org.typelevel.log4cats.slf4j.Slf4jLogger

class HeartbeatCommandHandlerImpl(
    ipfsClusterService: IpfsClusterService[IO],
    p2pService: P2pService[IO],
    ipfsService: IpfsService
) extends HeartbeatCommandHandler {

  Slf4jLogger.getLogger[IO]

  override def handle(cmd: HeartbeatCommand): IO[HeartbeatEvent[?]] = {
    handleInner(cmd)
      .map(_ => HeartbeatSuccessEvent())
      .handleError(throwable => HeartbeatErrorEvent(throwable = throwable))
  }

  private def handleInner(cmd: HeartbeatCommand): IO[Unit] = {
    for
      _               <- p2pService.setControllerStatusOnline()
      sharedFolderCid <- ipfsService.getSharedFolderCid
      message = FederationMemberHeartbeat(sharedFolderCid = sharedFolderCid, Instant.now())
      _ <- p2pService.publishFederationMemberHeartbeat(message)
      _ <- ipfsClusterService.setPeers(cmd.peers)
    yield ()
  }
}
