package acab.devcon0.trile.domain.ports.input

import acab.devcon0.trile.cqrs.Command
import acab.devcon0.trile.cqrs.CommandHandler
import acab.devcon0.trile.cqrs.Event
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

case class ForceSyncCommand() extends Command[String]

sealed abstract class ForceSyncEvent[T]              extends Event[T]
case class ForceSyncSuccessEvent()                   extends ForceSyncEvent[Unit]
case class ForceSyncErrorEvent(throwable: Throwable) extends ForceSyncEvent[Unit]

trait ForceSyncCommandHandler extends CommandHandler[ForceSyncCommand, String, ForceSyncEvent[?]]

object ForceSyncCommandImplicits {
  implicit class PingCommandEventFlattenOps(result: IO[ForceSyncEvent[?]]) {
    private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

    def flattenEvents: IO[Unit] = {
      result.flatMap {
        case ForceSyncSuccessEvent()        => IO.unit
        case ForceSyncErrorEvent(throwable) => handleErrorCase(throwable)
      }
    }

    private def handleErrorCase(exception: Throwable): IO[Unit] = {
      logger.error(s"exception=$exception") >>
        IO.raiseError[Unit](exception)
    }
  }
}
