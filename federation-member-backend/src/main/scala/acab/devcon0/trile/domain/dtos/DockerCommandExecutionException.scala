package acab.devcon0.trile.domain.dtos

case class DockerCommandExecutionException(exitCode: Long) extends Throwable
