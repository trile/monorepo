package acab.devcon0.trile.domain.service

import acab.devcon0.trile.domain.dtos._

trait IpfsClusterService[F[_]] {
  def getPeers: F[IpfsClusterPeers]
  def setPeers(peers: List[IpfsClusterPeer]): F[Unit]
  def setPinsCount(): F[Int]
  def getPinsCount: F[Int]
  def getId: F[IpfsClusterPeer]
}
