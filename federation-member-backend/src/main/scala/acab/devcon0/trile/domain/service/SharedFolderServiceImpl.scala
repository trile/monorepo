package acab.devcon0.trile.domain.service

import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path
import java.util.concurrent.atomic.AtomicReference

import scala.jdk.CollectionConverters._

import acab.devcon0.trile.configuration.Configuration
import cats.effect.IO

class SharedFolderServiceImpl(configuration: Configuration) extends SharedFolderService[IO] {

  private val sharedFolderFileCount: AtomicReference[Int] = new AtomicReference[Int](0)
  private val sharedFolderSize: AtomicReference[Long]     = new AtomicReference[Long](0)

  override def updateSharedFolderInformation(): IO[Unit] = {
    calculateSharedFolderFileCount
      .map(count => sharedFolderFileCount.set(count)) >>
      calculateSharedFolderSize
        .map(size => sharedFolderSize.set(size))

  }

  override def getSharedFolderFileCount: IO[Int] = {
    IO(sharedFolderFileCount.get())
  }

  override def getSharedFolderSize: IO[Long] = {
    IO(sharedFolderSize.get())
  }

  private def calculateSharedFolderSize: IO[Long] = {
    val path: Path = FileSystems.getDefault.getPath(configuration.sharedFolder)
    IO(
      Files
        .walk(path)
        .filter(p => Files.isRegularFile(p))
        .mapToLong(p => Files.size(p))
        .sum()
    )
  }

  private def calculateSharedFolderFileCount: IO[Int] = {
    val path: Path = FileSystems.getDefault.getPath(configuration.sharedFolder)
    IO(Files.walk(path).iterator().asScala.count(path => path.toFile.isFile))
  }

}
