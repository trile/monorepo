package acab.devcon0.trile.domain.ports.output

import acab.devcon0.trile.domain.dtos._
import cats.effect.IO

trait IpfsClusterClient[F[_]] {
  def getPeers: IO[IpfsClusterPeers]
  def getPinsCount: IO[Int]
  def getId: F[IpfsClusterPeer]
}
