package acab.devcon0.trile.domain.adapters

import acab.devcon0.trile.domain.ports.input._
import acab.devcon0.trile.domain.service.P2pService
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class CheckInAckCommandHandlerImpl(p2pService: P2pService[IO]) extends CheckInAckCommandHandler {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def handle(cmd: CheckInAckCommand): IO[CheckInAckEvent[?]] = {
    p2pService
      .setCheckInStatus(cmd.checkInStatus)
      .map(_ => CheckInAckSuccessEvent())
      .onError(throwable => IO(CheckInAckErrorEvent(throwable = throwable)))
  }
}
