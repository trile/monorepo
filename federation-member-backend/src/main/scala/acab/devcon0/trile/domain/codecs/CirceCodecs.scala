package acab.devcon0.trile.domain.codecs

import acab.devcon0.trile.domain.dtos.CheckInStatus
import acab.devcon0.trile.domain.dtos.ControllerStatus
import acab.devcon0.trile.domain.dtos.DockerCommandExecutionException
import acab.devcon0.trile.domain.dtos.IpfsAddResponse
import acab.devcon0.trile.domain.dtos.SyncStatus
import acab.devcon0.trile.domain.dtos.http.FederationMemberStatus
import io.circe._
import io.circe.generic.semiauto.deriveEncoder

object CirceCodecs {

  object Encoders {
    import acab.devcon0.trile.domain.codecs.IpfsClusterCodecs.Encoders.*
    implicit val syncStatus: Encoder[SyncStatus] = (syncStatus: SyncStatus) => {
      Json.fromString(syncStatus.toString)
    }
    implicit val checkInStatus: Encoder[CheckInStatus] = (checkInStatus: CheckInStatus) => {
      Json.fromString(checkInStatus.toString)
    }
    implicit val controllerStatus: Encoder[ControllerStatus] = (controllerStatus: ControllerStatus) => {
      Json.fromString(controllerStatus.toString)
    }
    implicit val federationMemberStatus: Encoder[FederationMemberStatus]                          = deriveEncoder
    implicit val dockerCommandExecutionExceptionEncoder: Encoder[DockerCommandExecutionException] = deriveEncoder
    implicit val IpfsAddResponseEncoder: Encoder[IpfsAddResponse]                                 = deriveEncoder
  }
}
