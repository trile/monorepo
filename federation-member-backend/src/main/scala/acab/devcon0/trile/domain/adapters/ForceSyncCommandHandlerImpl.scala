package acab.devcon0.trile.domain.adapters

import java.time.Instant

import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberSync
import acab.devcon0.trile.domain.ports.input._
import acab.devcon0.trile.domain.service.IpfsService
import acab.devcon0.trile.domain.service.P2pService
import cats.effect.IO
import org.typelevel.log4cats.slf4j.Slf4jLogger

class ForceSyncCommandHandlerImpl(ipfsService: IpfsService, p2pService: P2pService[IO])
    extends ForceSyncCommandHandler {

  Slf4jLogger.getLogger[IO]

  override def handle(cmd: ForceSyncCommand): IO[ForceSyncEvent[?]] = {
    handleInner()
      .map(_ => ForceSyncSuccessEvent())
      .handleError(throwable => ForceSyncErrorEvent(throwable = throwable))
  }

  private def handleInner(): IO[Unit] = {
    for
      sharedFolderIpfsCid <- ipfsService.getSharedFolderCid
      _                   <- p2pService.publishFederationMemberSync(getMessage(sharedFolderIpfsCid))
    yield ()
  }

  private def getMessage(sharedFolderCid: IpfsCid): FederationMemberSync = {
    FederationMemberSync(sharedFolderCid = sharedFolderCid, timestamp = Instant.now())
  }
}
