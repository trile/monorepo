package acab.devcon0.trile.output

import acab.devcon0.trile.configuration.IpfsClusterConfiguration
import acab.devcon0.trile.domain.codecs.IpfsClusterCodecs
import acab.devcon0.trile.domain.dtos.IpfsClusterPeer
import acab.devcon0.trile.domain.dtos.IpfsClusterPeers
import acab.devcon0.trile.domain.ports.output.IpfsClusterClient
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger
import sttp.client3._

class IpfsClusterClientImpl(ipfsClusterConfiguration: IpfsClusterConfiguration, sttpBackend: SttpBackend[Identity, Any])
    extends IpfsClusterClient[IO] {

  private implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def getPeers: IO[IpfsClusterPeers] = {
    ipfsClusterPeersRequest()
      .flatTap(logResponse(_, "getPeers"))
      .flatMap(response => IpfsClusterCodecs.Decoders.IpfsClusterPeers(response.body.getOrElse("")))
      .onError(throwable => logger.error(s"throwable=$throwable"))
  }

  override def getId: IO[IpfsClusterPeer] = {
    ipfsClusterPeerRequest()
      .flatTap(logResponse(_, "getId"))
      .flatMap(response => IpfsClusterCodecs.Decoders.IpfsClusterPeer(response.body.getOrElse("")))
      .onError(throwable => logger.error(s"throwable=$throwable"))
  }

  override def getPinsCount: IO[Int] = {
    ipfsClusterPinsRequest()
      .flatTap(logResponse(_, "getPins"))
      .flatMap(response => IpfsClusterCodecs.Decoders.IpfsClusterPins(response.body.getOrElse("")))
      .map(_.pins.size)
      .onError(throwable => logger.error(s"throwable=$throwable"))
  }

  private def ipfsClusterPeersRequest(): IO[Identity[Response[Either[String, String]]]] = IO {
    basicRequest
      .get(uri = uri"${ipfsClusterConfiguration.apiUrl}/peers")
      .send(sttpBackend)
  }

  private def ipfsClusterPeerRequest(): IO[Identity[Response[Either[String, String]]]] = IO {
    basicRequest
      .get(uri = uri"${ipfsClusterConfiguration.apiUrl}/id")
      .send(sttpBackend)
  }

  private def ipfsClusterPinsRequest(): IO[Identity[Response[Either[String, String]]]] = IO {
    basicRequest
      .get(uri = uri"${ipfsClusterConfiguration.apiUrl}/pins")
      .send(sttpBackend)
  }

  private def logResponse(response: Response[Either[String, String]], label: String): IO[Unit] = {
    logger.debug(s"label=$label code=${response.code} response=${response.toString.substring(0, 64)}")
  }
}
