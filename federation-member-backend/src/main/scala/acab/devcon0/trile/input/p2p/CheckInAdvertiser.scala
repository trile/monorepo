package acab.devcon0.trile.input.p2p

import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

import scala.concurrent.duration.Duration

import acab.devcon0.trile.domain.dtos.CheckInStatus
import acab.devcon0.trile.domain.dtos.NackException
import acab.devcon0.trile.domain.ports.input._
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class CheckInAdvertiser(checkInCommandHandler: CheckInCommandHandler, checkInAckQueryHandler: CheckInAckQueryHandler) {

  private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  def run(): IO[Unit] = {
    for
      _     <- logger.info(s"I'm awake, let's inform the controller")
      event <- checkInCommandHandler.handle(CheckInCommand())
      _ <- event match
        case CheckInSuccessEvent()        => waitForAck().recoverWith { case NackException() => run() }
        case CheckInErrorEvent(throwable) => IO.raiseError(throwable)
      _ <- logger.info(s"Check in process completed...")
    yield ()
  }

  private def waitForAck(): IO[Unit] = {
    checkAckAttempt(checkInAckQueryHandler.handle(CheckInAckQuery()), 100)
  }

  private def checkAckAttempt(io: IO[CheckInStatus], attemptsLeft: Int): IO[Unit] = {
    if attemptsLeft <= 0 then IO.raiseError(new TimeoutException("Reached max attempts"))
    else
      io
        .flatTap(checkInStatus => logger.debug(s"checkAckAttempt checkInStatus=$checkInStatus"))
        .delayBy(Duration(100, TimeUnit.MILLISECONDS))
        .flatMap(checkInStatus => {
          if checkInStatus.equals(CheckInStatus.ACK) then IO.unit
          else if checkInStatus.equals(CheckInStatus.NACK) then IO.raiseError(NackException())
          else checkAckAttempt(io, attemptsLeft - 1)
        })
  }
}
