package acab.devcon0.trile.input.p2p

import scala.concurrent.duration.DurationInt

import acab.devcon0.trile.domain.service.IpfsClusterService
import acab.devcon0.trile.utils.EffectsUtils
import cats.effect.IO
import cats.implicits.catsSyntaxMonadError
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class IpfsClusterPinCountFeed(ipfsClusterService: IpfsClusterService[IO]) {

  private implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  def run(): IO[Unit] = {
    logger.info(s"Ipfs Cluster pin count feed started") >>
      ipfsClusterService
        .setPinsCount()
        .recoverWith(_ => IO.unit)
        .attemptTap(EffectsUtils.attemptTLog)
        .flatTap(_ => IO.sleep(1.minutes))
        .foreverM
  }
}
