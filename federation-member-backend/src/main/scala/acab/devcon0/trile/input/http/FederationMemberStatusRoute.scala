package acab.devcon0.trile.input.http

import acab.devcon0.trile.domain.codecs.CirceCodecs.Encoders.federationMemberStatus
import acab.devcon0.trile.domain.ports.input.FederationMemberStatusQuery
import acab.devcon0.trile.domain.ports.input.FederationMemberStatusQueryHandler
import cats.effect._
import org.http4s.HttpRoutes
import org.http4s.circe.CirceEntityEncoder.circeEntityEncoder
import org.http4s.dsl.io._
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class FederationMemberStatusRoute(queryHandler: FederationMemberStatusQueryHandler) {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  val routes: HttpRoutes[IO] = HttpRoutes
    .of[IO] { case GET -> Root =>
      (for
        federationMemberStatus <- queryHandler.handle(FederationMemberStatusQuery())
        response               <- Ok(federationMemberStatus)
      yield response).handleErrorWith(throwable => logger.error(throwable.toString) >> BadRequest())

    }

  
}
