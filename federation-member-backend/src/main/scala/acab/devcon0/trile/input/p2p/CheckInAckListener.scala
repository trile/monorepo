package acab.devcon0.trile.input.p2p

import acab.devcon0.trile.configuration.P2pConfiguration
import acab.devcon0.trile.domain.codecs.P2pCodecs.Decoders.checkInAck
import acab.devcon0.trile.domain.dtos.CheckInStatus
import acab.devcon0.trile.domain.dtos.CheckInStatus.ACK
import acab.devcon0.trile.domain.dtos.pubsub.P2p
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Listener._
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberCheckInAck
import acab.devcon0.trile.domain.dtos.pubsub.P2p.MessageType
import acab.devcon0.trile.domain.ports.input.CheckInAckCommand
import acab.devcon0.trile.domain.ports.input.CheckInAckCommandHandler
import acab.devcon0.trile.domain.ports.input.CheckInAckCommandImplicits.CheckInAckCommandEventFlattenOps
import cats.effect._
import cats.effect.unsafe.IORuntime
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class CheckInAckListener(
    p2pListener: P2pListener,
    p2pConfiguration: P2pConfiguration,
    checkInAckCommandHandler: CheckInAckCommandHandler
) {

  private implicit val runtime: IORuntime = cats.effect.unsafe.IORuntime.global
  private val logger: Logger[IO]          = Slf4jLogger.getLogger[IO]

  def run(): IO[Unit] = {
    for
      topic <- IO(p2pConfiguration.topics.checkIn)
      listenerParams = Params(topic = topic, messageTypes = List(MessageType.Ack), mode = Mode.Directed)
      _ <- p2pListener.run[FederationMemberCheckInAck](listenerParams, message => triggerCommandInBackground(message))
    yield ()
  }

  private def triggerCommandInBackground(message: Message[FederationMemberCheckInAck]): IO[Unit] = IO {
    (for
      _ <- checkInAckCommandHandler.handle(CheckInAckCommand(CheckInStatus.ACK)).flattenEvents
      _ <- logger.debug(s"Check in ACK received over P2P pub/sub. p2pPeerId=${message.meta.from}}")
    yield ()).unsafeRunAndForget()
  }
}
