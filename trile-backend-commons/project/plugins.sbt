addSbtPlugin("org.jetbrains.scala" % "sbt-ide-settings"          % "1.1.1")
addSbtPlugin("com.github.cb372"    % "sbt-explicit-dependencies" % "0.2.16")
addSbtPlugin("ch.epfl.scala"       % "sbt-scalafix"              % "0.12.0")
addSbtPlugin("ch.epfl.scala"       % "sbt-bloop"                 % "1.5.15")

addDependencyTreePlugin
