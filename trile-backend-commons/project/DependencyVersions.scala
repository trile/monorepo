object DependencyVersions {
  val cats     = "3.5.3"
  val circe    = "0.14.6"
  val libP2p   = "1.1.0-RELEASE"
  val log4cats = "2.6.0"
}
