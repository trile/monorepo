package acab.devcon0.trile.utils

import cats.effect.IO
import org.typelevel.log4cats.Logger

object EffectsUtils {

  def forceOptionExistence[I](): Option[I] => IO[I] = {
    case Some(value) => IO.pure(value)
    case None        => IO.raiseError(IllegalArgumentException())
  }

  def attemptTLog[T](implicit logger: Logger[IO]): Either[Throwable, T] => IO[Unit] = {
    case Left(throwable) => logger.error(s"throwable=$throwable")
    case Right(value)    => logger.debug(s"value=$value")
  }
}
