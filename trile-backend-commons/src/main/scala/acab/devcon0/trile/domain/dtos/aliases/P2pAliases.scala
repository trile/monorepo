package acab.devcon0.trile.domain.dtos.aliases

type P2pPeerId     = String
type P2pKey        = String
