package acab.devcon0.trile.domain.codecs

import acab.devcon0.trile.domain.dtos
import acab.devcon0.trile.domain.dtos._
import cats.effect.IO
import io.circe._
import io.circe.jawn.decode

object IpfsCodecs {

  object Decoders {
    implicit val ipfsPeerRepoStat: Decoder[IpfsPeerRepoStat] = (c: HCursor) => {
      for
        repoSize   <- c.downField("RepoSize").as[Long]
        storageMax <- c.downField("StorageMax").as[Long]
        numObjects <- c.downField("NumObjects").as[Long]
        repoPath   <- c.downField("RepoPath").as[String]
        version    <- c.downField("Version").as[String]
      yield {
        dtos.IpfsPeerRepoStat(
          repoSize = repoSize,
          storageMax = storageMax,
          numObjects = numObjects,
          repoPath = repoPath,
          version = version
        )
      }
    }

    object IpfsPeerRepoStat {
      def apply(rawJson: String): IO[IpfsPeerRepoStat] = IO.fromEither(decode[IpfsPeerRepoStat](rawJson))
    }
  }
}
