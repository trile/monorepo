package acab.devcon0.trile.domain.dtos.pubsub

import java.time.Instant

import acab.devcon0.trile.domain.dtos.IpfsClusterPeer
import acab.devcon0.trile.domain.dtos.aliases._
import io.libp2p.core.pubsub.Topic

object P2p {

  final case class Message[T <: Data](meta: Metadata, data: T)
  final case class Metadata(messageType: MessageType, from: P2pPeerId, to: Option[P2pPeerId] = None)
  sealed trait Data

  object Listener {
    final case class Params(topic: Topic, messageTypes: List[MessageType], mode: Mode)

    enum Mode:
      case Broadcast, Directed
  }

  object Message {
    final case class FederationMemberSharingFolderUpdate(sharedFolderCid: IpfsCid, timestamp: Instant) extends Data

    final case class FederationMemberSync(sharedFolderCid: IpfsCid, timestamp: Instant) extends Data
    final case class FederationMemberSyncAck()                                          extends Data
    final case class FederationMemberSyncNack()                                         extends Data

    final case class FederationMemberCheckIn(
        p2pPeerId: P2pPeerId,
        nickname: FederationMemberNickname,
        ipfsPeerId: IpfsPeerId,
        sharedFolderCid: IpfsCid,
        ipfsClusterPeerId: IpfsClusterPeerId,
        ipfsClusterNodeName: IpfsClusterNodeName,
        timestamp: Instant
    ) extends Data
    final case class FederationMemberCheckInAck()  extends Data
    final case class FederationMemberCheckInNack() extends Data

    final case class FederationControllerHeartbeat(peers: List[IpfsClusterPeer])             extends Data
    final case class FederationMemberHeartbeat(sharedFolderCid: IpfsCid, timestamp: Instant) extends Data
  }

  enum MessageType:
    case Event, Request, Ack, Nack
}
