package acab.devcon0.files.templates.production.federationcontroller

object SslCertificate {
  def apply(): String = """#!/bin/bash

domains=(${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS} www.${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS} api.${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS} gateway.ipfs.${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS})
rsa_key_size=4096
data_path="${TRILE_IPFS_DOCKER_CONTAINER_DATA_CERTBOT_DIRECTORY}"
email="admin@${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS}"
staging=${TRILE_FEDERATION_CONTROLLER_CERTBOT_STAGING}

if [ ! -e "$data_path/conf/options-ssl-nginx.conf" ] || [ ! -e "$data_path/conf/ssl-dhparams.pem" ]; then
  echo "### Downloading recommended TLS parameters ..."
  mkdir -p "$data_path/conf"
  curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/_internal/tls_configs/options-ssl-nginx.conf > "$data_path/conf/options-ssl-nginx.conf"
  curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot/certbot/ssl-dhparams.pem > "$data_path/conf/ssl-dhparams.pem"
  echo
fi

echo "### Creating dummy certificate for $domains ..."
path="/etc/letsencrypt/live/$domains"
mkdir -p "$data_path/conf/live/$domains"
docker compose -f ${TRILE_FEDERATION_CONTROLLER_CONFIGURATION_FOLDER}/docker-compose.yaml run --rm --entrypoint "\
  openssl req -x509 -nodes -newkey rsa:$rsa_key_size -days 1\
    -keyout '$path/privkey.pem' \
    -out '$path/fullchain.pem' \
    -subj '/CN=localhost'" ${TRILE_FEDERATION_CONTROLLER_CERTBOT_SERVICE_NAME}
echo


# echo "### Starting nginx ..."
# docker-compose up --force-recreate -d ${TRILE_FEDERATION_CONTROLLER_CERTBOT_SERVICE_NAME}
# echo

echo "### Deleting dummy certificate for $domains ..."
docker compose -f ${TRILE_FEDERATION_CONTROLLER_CONFIGURATION_FOLDER}/docker-compose.yaml run --rm --entrypoint "\
  rm -Rf /etc/letsencrypt/live/$domains && \
  rm -Rf /etc/letsencrypt/archive/$domains && \
  rm -Rf /etc/letsencrypt/renewal/$domains.conf" ${TRILE_FEDERATION_CONTROLLER_CERTBOT_SERVICE_NAME}
echo


echo "### Requesting Let's Encrypt certificate for $domains ..."
#Join $domains to -d args
domain_args=""
for domain in "${domains[@]}"; do
  domain_args="$domain_args -d $domain"
done

# Enable staging mode if needed
if [ $staging != "0" ]; then staging_arg="--staging"; fi

docker compose -f ${TRILE_FEDERATION_CONTROLLER_CONFIGURATION_FOLDER}/docker-compose.yaml run --rm --entrypoint "\
  certbot certonly --webroot -w /var/www/certbot \
    $staging_arg \
    --register-unsafely-without-email \
    $domain_args \
    --rsa-key-size $rsa_key_size \
    --agree-tos \
    --force-renewal" ${TRILE_FEDERATION_CONTROLLER_CERTBOT_SERVICE_NAME}
echo

echo "### Reloading nginx ..."
docker compose -f ${TRILE_FEDERATION_CONTROLLER_CONFIGURATION_FOLDER}/docker-compose.yaml exec ${TRILE_FEDERATION_CONTROLLER_REVERSE_PROXY_SERVICE_NAME} nginx -s reload
""".stripMargin

}
