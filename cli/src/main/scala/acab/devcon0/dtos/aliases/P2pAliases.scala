package acab.devcon0.dtos.aliases

type P2pPeerId     = String
type P2pMessageKey = String
type P2pKey        = String
