package acab.devcon0.dtos

case class FederationMemberUpdateInputVariables(federationName: String, nickname: String)
