package acab.devcon0.services

import acab.devcon0.dtos.TrileCli.EnvironmentVariables.TrileFederationEnvVars

import scala.io.Source
import scala.util.Try

object EnvVarsFileLoader {

  def apply(fileAbsolutePath: String): TrileFederationEnvVars = {
    loadFedVars(fileAbsolutePath).getOrElse(Map())
  }

  private def loadFedVars(filePath: String): Try[Map[String, String]] = {
    Try(Source.fromFile(filePath))
      .map(source => (source, source.getLines()))
      .flatMap(tuple => {
        val source = tuple._1
        val lines  = tuple._2
        (for {
          fedVarsMap <- loadFedVarsMap(lines)
        } yield {
          (source, fedVarsMap)
        })
      })
      .flatMap(tuple => {
        tuple._1.close()
        Try(tuple._2)
      })
  }

  private def loadFedVarsMap(
      lines: Iterator[String]
  ): Try[Map[String, String]] = Try {
    lines
      .flatMap(line => {
        line.split("=", 2) match {
          case Array(key, value) => Some(key -> value)
          case _                 => None
        }
      })
      .toMap
  }
}
