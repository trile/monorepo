package acab.devcon0.services.shell

import scala.io.StdIn

object ConfirmAndRun {

  def apply(skipConfirmation: Boolean, action: () => Unit): Unit = {
    if skipConfirmation || userConfirms then action()
    else abort()
  }

  private def userConfirms: Boolean = {
    val confirmation: String = StdIn.readLine("Continue? [y/N]: ")
    confirmation.equals("y")
  }

  private def abort(): Unit = {
    PrintLn.yellow("Aborting...See you soon")
    sys.exit(0)
  }
}
