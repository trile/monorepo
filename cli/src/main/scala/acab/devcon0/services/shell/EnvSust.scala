package acab.devcon0.services.shell

import cats.effect.IO
import cats.implicits.catsSyntaxMonadError

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}

object EnvSust {
  def apply(
      envVarsMap: Map[String, String],
      templateContent: String,
      absoluteOutputPath: String
  ): IO[Unit] = {
    val cmdLabel = s"File generation: ${absoluteOutputPath.split('/').lastOption.getOrElse(absoluteOutputPath)}"
    IO(applyInner(envVarsMap, templateContent, absoluteOutputPath, true))
      .attemptTap[Unit](PrintLn.subStepAttemptTap(cmdLabel, _))
  }

  private def applyInner(
      envVarsMap: Map[String, String],
      templateContent: String,
      absoluteOutputPath: String,
      quiet: Boolean
  ): Unit = {
    if !quiet then println(s"> envsust < .. > $absoluteOutputPath ")

    val replacedContent = envVarsMap.foldLeft(templateContent) { (content, envVar) =>
      content.replace(
        s"$${${envVar._1}}",
        envVar._2.stripPrefix("\"").stripSuffix("\"")
      )
    }
    Files.write(
      Paths.get(absoluteOutputPath),
      replacedContent.getBytes(StandardCharsets.UTF_8)
    )
  }
}
