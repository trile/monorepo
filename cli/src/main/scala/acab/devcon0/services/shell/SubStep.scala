package acab.devcon0.services.shell

import cats.effect.IO

import scala.scalanative.libc.stdlib
import scala.scalanative.unsafe
import scala.scalanative.unsafe.{CInt, Zone, toCString}

object SubStep {

  def withoutProgress(cmd: String, label: String, wipingLineLength: Int): IO[Unit] = {
    ShellExecutor
      .getCmdIO(cmd)
      .flatMap(cInt => {
        if cInt != 0 then PrintLn.subStepReplaceError(label) >> IO.raiseError(NonZeroExitCode())
        else PrintLn.subStepReplaceOk(label, Some(wipingLineLength))
      })
  }

  def withProgress(cmd: String, label: String): IO[Unit] = {
    ShellExecutor
      .getCmdIO(appendProgress(cmd, label))
      .flatMap(cInt => {
        if cInt != 0 then PrintLn.subStepReplaceError(label) >> IO.raiseError(NonZeroExitCode())
        else PrintLn.subStepReplaceOk(label)
      })
  }

  private def appendProgress(cmd: String, label: String): String = {
    s"""$cmd &
pid=$$! # Process Id of the previous running command

Off='\\033[0m'
BBlue='\\033[1;34m'

i=0
while kill -0 $$pid 2>/dev/null
do
  for s in / - \\\\ \\|; do
    printf \"\\r \\033[1;34m [$$s]\\033[0m $label\"
    sleep .1
  done
  i=$$((i+1))
done
wait $$pid
exit $$?
""".stripMargin
  }
}

object ShellExecutor {

  def getCmdIO(cmd: String): IO[CInt] = {
    IO {
      Zone { implicit z =>
        stdlib.system(toCString(cmd))
      }
    }
  }

}

final case class NonZeroExitCode() extends Throwable
