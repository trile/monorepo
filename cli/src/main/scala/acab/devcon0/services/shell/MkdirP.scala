package acab.devcon0.services.shell

import cats.effect.IO

object MkdirP {

  private val cmdLabel: String = "Ensuring folder exists"

  def apply(folderPath: String): IO[Unit] = {
    val cmd: String = getCmd(folderPath)
    val label       = s"$cmdLabel: ${folderPath.split('/').lastOption.getOrElse(folderPath)}"
    SubStep.withProgress(cmd, label)
  }

  private def getCmd(folderPath: String): String = {
    s"mkdir -p $folderPath"
  }
}
