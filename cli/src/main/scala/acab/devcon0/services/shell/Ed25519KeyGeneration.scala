package acab.devcon0.services.shell

import scala.scalanative.unsafe.Zone

object Ed25519KeyGeneration {
  def apply(): String = {
    Zone { implicit z =>
      val command = s" openssl genpkey -algorithm Ed25519"
      ExecAndGetStdout(command)
        .map(_.tail.head)
        .get
    }
  }
}
