package acab.devcon0.services.shell

import cats.effect.IO
import fansi.{Bold, Color}

object PrintLn {

  def blue(string: String): Unit = {
    println(Color.Blue(string))
  }

  def cyan(string: String): Unit = {
    println(Color.Cyan(string))
  }

  def yellow(string: String): Unit = {
    println(Color.Yellow(string))
  }

  def green(string: String): Unit = {
    println(Color.Green(string))
  }

  def subStepAttemptTap(label: String, either: Either[Throwable, ?], isFinal: Boolean = false): IO[Unit] = {
    either
      .fold(_ => subStepError(label, isFinal), _ => subStepOk(label, isFinal))
  }

  def subStepInfo(label: String, isFinal: Boolean = false): IO[Unit] = {
    val str = {
      if isFinal then s"${Color.Blue("[i]").overlay(Bold.On)} ${Color.Blue(label)}"
      else s"${Color.Blue("[i]").overlay(Bold.On)} $label"
    }
    IO(print(str)).flatMap(_ => IO(println()))
  }

  def subStepOk(label: String, isFinal: Boolean = false): IO[Unit] = {
    val str = {
      if isFinal then s"${Color.Green("[\u2713]").overlay(Bold.On)} ${Color.Green(label)}"
      else s"${Color.Green("[\u2713]").overlay(Bold.On)} $label"
    }
    IO(print(str)).flatMap(_ => IO(println()))
  }

  def subStepError(label: String, isFinal: Boolean = false): IO[Unit] = {
    val str = {
      if isFinal then s"${Color.Red("[x]").overlay(Bold.On)} ${Color.Red(label)}"
      else s"${Color.Red("[x]").overlay(Bold.On)} $label"
    }
    IO(print(str)).flatMap(_ => IO(println()))
  }

  def subStepReplaceOk(label: String, wipingLineLength: Option[Int] = None): IO[Unit] = {
    val padding: String = " " * wipingLineLength.getOrElse(label.length)
    IO(printf(s"\r${Color.Green("[\u2713]").overlay(Bold.On)} $label $padding"))
      .flatMap(_ => IO(println()))
  }

  def subStepReplaceError(label: String, wipingLineLength: Option[Int] = None): IO[Unit] = {
    val padding: String = " " * wipingLineLength.getOrElse(label.length)
    IO(printf(s"\r${Color.Red(s"[x]").overlay(Bold.On)} ${Color.Red(s"$label $padding")}"))
      .flatMap(_ => IO(println()))
  }
}

object Print {
  def cyan(string: String): Unit = {
    println(Color.Cyan(string))
  }

  def yellow(string: String): Unit = {
    println(Color.Yellow(string))
  }

  def green(string: String): Unit = {
    println(Color.Green(string))
  }

}
