package acab.devcon0.services.shell

import cats.effect.IO

object SudoRm {

  private val cmdLabel: String = "Deleting folder"

  def apply(folderPath: String): IO[Unit] = {
    val message     = s"Up to delete $folderPath folder as sudo"
    val cmd: String = getCmd(folderPath, message)
    SubStep.withoutProgress(cmd, s"$cmdLabel: $folderPath", message.length)
  }

  private def getCmd(folderPath: String, message: String): String = {
    s"""
if [ -d "$folderPath" ]; then
    printf \"$message\\n\"
    sudo rm -rf "$folderPath"
fi
    """
  }
}
