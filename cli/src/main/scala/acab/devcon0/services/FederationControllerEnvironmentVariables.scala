package acab.devcon0.services

import acab.devcon0.dtos.FederationControllerInstallInputVariables
import acab.devcon0.dtos.TrileCli.Environment.*
import acab.devcon0.dtos.TrileCli.EnvironmentVariables.*
import acab.devcon0.dtos.TrileCli.{Defaults, Environment}
import acab.devcon0.services.shell.{Ed25519KeyGeneration, GroupId, UserId}

import java.net.InetAddress
import scala.util.Random

object FederationControllerEnvironmentVariables {

  def apply(
      inputVariables: FederationControllerInstallInputVariables
  ): TrileFederationEnvVars = {
    val userHomeAbsolutePath: String          = PathSanitizer.expandHome(Defaults.userHomeAbsolutePath)
    val configurationPath: String             = s"$userHomeAbsolutePath/.config/trile"
    val environment: Environment              = inputVariables.environment
    val federationName: String                = inputVariables.federationName
    val controllerNetworkAddress: InetAddress = inputVariables.networkAddress
    val controllerName: String                = s"$federationName-federation-controller"
    val controllerConfigurationFolder: String = s"$configurationPath/federations/$federationName/controller"

    val certbotContainerName: String = s"trile-$controllerName-certbot"
    val certbotServiceName: String   = certbotContainerName.replace('-', '_')
    val nginxContainerName: String   = s"trile-$controllerName-nginx"
    val nginxServiceName: String     = nginxContainerName.replace('-', '_')
    val backendContainerName         = s"trile-$controllerName-backend"
    val backendServiceName: String   = backendContainerName.replace('-', '_')
    val ipfsSwarmPort: String        = Defaults.ipfsSwarmPort
    val ipfsClusterSwarmPort: String = Defaults.ipfsClusterSwarmPort
    val sharedDiskSpaceGb: Int       = inputVariables.sharedDiskSpaceGb.getOrElse(Defaults.sharedDiskSpaceGb)
    val certbotDirectory =
      inputVariables.certbotFolderAbsolutePathOptional.getOrElse(s"$controllerConfigurationFolder/certbot")
    Map(
      DOLLAR                                             -> "$",
      HOST_UID                                           -> UserId().toString,
      HOST_GID                                           -> GroupId().get,
      TRILE_ENVIRONMENT                                  -> inputVariables.environment.toString,
      TRILE_FEDERATION_NAME                              -> federationName,
      TRILE_CONFIGURATION_FOLDER                         -> configurationPath,
      TRILE_BACKEND_DOCKER_CONTAINER_NAME                -> backendContainerName,
      TRILE_FRONTEND_DOCKER_CONTAINER_NAME               -> s"trile-$controllerName-frontend",
      TRILE_IPFS_DOCKER_CONTAINER_NAME                   -> s"trile-$controllerName-ipfs",
      TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_NAME           -> s"trile-$controllerName-ipfs-cluster",
      TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_DATA_DIRECTORY -> s"$controllerConfigurationFolder/ipfs-cluster",
      TRILE_IPFS_DOCKER_CONTAINER_DATA_DIRECTORY         -> s"$controllerConfigurationFolder/ipfs",
      TRILE_IPFS_DOCKER_CONTAINER_DATA_CERTBOT_DIRECTORY -> certbotDirectory,
      TFC_BACKEND_SERVICE_NAME                           -> backendServiceName,
      TFC_CERTBOT_CONTAINER_NAME                         -> certbotContainerName,
      TFC_CERTBOT_SERVICE_NAME                           -> certbotServiceName,
      TFC_CERTBOT_STAGING                                -> inputVariables.stagingSSL,
      TFC_CONFIGURATION_FOLDER                           -> controllerConfigurationFolder,
      TFC_JOIN_TOKEN                                     -> Random.alphanumeric.take(32).mkString("").toUpperCase,
      TFC_FEDVARS_ABSOLUTE_PATH                          -> s"$controllerConfigurationFolder/.fedvars",
      TFC_IPFS_CLUSTER_ADDRESS -> getIpfsClusterAddress(controllerNetworkAddress, environment, ipfsClusterSwarmPort),
      TFC_IPFS_CLUSTER_REPLICA_FACTOR_MAX -> inputVariables.ipfsClusterReplicaFactorMax.toString,
      TFC_IPFS_MAX_DISK_SPACE_GB          -> sharedDiskSpaceGb.toString,
      TFC_IPFS_ADDRESS                    -> getIpfsAddress(controllerNetworkAddress, environment, ipfsSwarmPort),
      TFC_IPFS_SWARM_PORT                 -> ipfsSwarmPort,
      TFC_IPFS_CLUSTER_SWARM_PORT         -> ipfsClusterSwarmPort,
      TFC_NETWORK_ADDRESS                 -> getNetworkAddress(inputVariables),
      TFC_P2P_PRIVATE_KEY                 -> Ed25519KeyGeneration(),
      TFC_P2P_PORT                        -> "7078",
      TFC_REVERSE_PROXY_CONFIGURATION     -> s"$controllerConfigurationFolder/nginx/nginx.conf",
      TFC_REVERSE_PROXY_CONTAINER_NAME    -> nginxContainerName,
      TFC_REVERSE_PROXY_SERVICE_NAME      -> nginxServiceName
    )
  }

  private def getNetworkAddress(inputVariables: FederationControllerInstallInputVariables): String = {
    inputVariables.environment match
      case Dev        => inputVariables.networkAddress.toString.split('/').last
      case Production => inputVariables.networkAddress.getHostName
  }

  private def getIpfsAddress(networkAddress: InetAddress, environment: Environment, port: String): String = {
    environment match
      case Dev        => s"/ip4/${networkAddress.toString.split('/').last}/tcp/$port"
      case Production => s"/dns4/ipfs.${networkAddress.getHostName}/tcp/$port"
  }

  private def getIpfsClusterAddress(networkAddress: InetAddress, environment: Environment, port: String): String = {
    environment match
      case Dev        => s"/ip4/${networkAddress.toString.split('/').last}/tcp/$port"
      case Production => s"/dns4/ipfs-cluster.${networkAddress.getHostName}/tcp/$port"
  }
}
