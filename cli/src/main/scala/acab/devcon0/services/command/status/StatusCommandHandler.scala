package acab.devcon0.services.command.status

import acab.devcon0.TrileCommand
import acab.devcon0.dtos.TrileCli.EnvironmentVariables.TrileFederationEnvVars
import acab.devcon0.dtos.http.FederationMemberStatus
import acab.devcon0.dtos.{CheckInStatus, ControllerStatus, SyncStatus, TrileCli}
import acab.devcon0.services.shell.{Clear, Print, PrintLn}
import acab.devcon0.services.{FederationMemberLoader, FederationMemberService}
import cats.effect.IO
import cats.effect.unsafe.IORuntime
import fansi.Color
import sttp.client4.SyncBackend
import sttp.client4.curl.CurlBackend

import java.time.Instant
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.math.BigDecimal.RoundingMode

object StatusCommandHandler {

  private val backend: SyncBackend        = CurlBackend()

  def runWatch(federationName: String, nickname: String): Unit = {
    FederationMemberService
      .getStatus(federationName, nickname)
      .map(printFederationMemberStatus(_, federationName))
      .recoverWith(_ => printWaitingForMemberBackendToBeOnline())
      .flatTap(_ => IO.unit.delayBy(Duration(2, TimeUnit.SECONDS)))
      .foreverM
      .unsafeToFuture()(TrileCommand.trileRuntime)
      .wait()
  }

  def run(federationName: String, nickname: String): Unit = {
    FederationMemberService
      .getStatus(federationName, nickname)
      .map(printFederationMemberStatus(_, federationName))
      .recoverWith(_ => printSomethingWentWrong())
      .unsafeToFuture()(TrileCommand.trileRuntime)
      .wait()
  }

  private def printFederationMemberStatus(
      federationMemberStatus: FederationMemberStatus,
      federationName: String
  ): Unit = {
    val nickname: String                          = federationMemberStatus.nickname
    val federationEnvVars: TrileFederationEnvVars = FederationMemberLoader(federationName, nickname)
    Clear()
    println()
    printSectionTitle(s"$nickname@$federationName")
    printConnectionStatus(federationMemberStatus)
    printBulletPoint(s"Peer id: ${federationMemberStatus.p2pPeerId}")
    printBulletPoint(s"Controller: ${federationEnvVars(TrileCli.EnvironmentVariables.TFC_NETWORK_ADDRESS)}")
    printBulletPoint(s"Installation path: ${federationEnvVars(TrileCli.EnvironmentVariables.TFM_CONFIGURATION_FOLDER)}")
    println()
    printSectionTitle("Shared folder")
    printSyncStatus(federationMemberStatus.syncStatus)
    printBulletPoint(s"Path: ${federationEnvVars(TrileCli.EnvironmentVariables.TFM_SHARING_FOLDER)}")
    printBulletPoint(s"Files: ${federationMemberStatus.sharedFolderFileCount}")
    printSharedFolderSize(federationMemberStatus)
    printBulletPoint(s"CID: ${federationMemberStatus.sharedFolderPin}")
    println()
    printIpfsSection(federationMemberStatus)
  }

  private def printIpfsSection(federationMemberStatus: FederationMemberStatus): Unit = {
    printSectionTitle("IPFS")

    val checkInStatus: CheckInStatus       = federationMemberStatus.checkInStatus
    val controllerStatus: ControllerStatus = federationMemberStatus.controllerStatus
    (checkInStatus, controllerStatus) match
      case (CheckInStatus.ACK, ControllerStatus.Online) =>
        printBulletPoint(s"Peers: ${federationMemberStatus.ipfsClusterPeers.peers.size}")
        printBulletPoint(s"Soft copies: ${federationMemberStatus.ipfsClusterPinsCount}")
        printIpfsRepoStats(federationMemberStatus)
      case (_, _) =>
        PrintLn.cyan("    Waiting for full online status from the controller...")

  }

  private def printSectionTitle(message: String): Unit = {
    println(s"${Color.Blue("===")} $message")
  }

  private def printIpfsRepoStats(federationMemberStatus: FederationMemberStatus): Unit = {
    val ipfsRepoSizeRaw             = federationMemberStatus.ipfsRepoSize
    val ipfsStorageMaxRaw           = federationMemberStatus.ipfsStorageMax
    val ipfsRepoSize: String        = humanReadableByteCount(ipfsRepoSizeRaw)
    val ipfsStorageMax: String      = humanReadableByteCount(ipfsStorageMaxRaw)
    val percentageDouble: Double    = (ipfsRepoSizeRaw.toDouble / ipfsStorageMaxRaw.toDouble) * 100
    val usagePercentage: BigDecimal = BigDecimal(percentageDouble).setScale(2, RoundingMode.HALF_UP)
    printBulletPoint(s"Disk space usage: $ipfsRepoSize out of $ipfsStorageMax (${usagePercentage}%)")
  }
  private def printSharedFolderSize(federationMemberStatus: FederationMemberStatus): Unit = {
    val sizeHumanReadable: String = humanReadableByteCount(federationMemberStatus.sharedFolderSizeBytes)
    printBulletPoint(s"Size: $sizeHumanReadable")
  }

  private def printConnectionStatus(federationMemberStatus: FederationMemberStatus): Unit = {
    val checkInStatus: CheckInStatus       = federationMemberStatus.checkInStatus
    val controllerStatus: ControllerStatus = federationMemberStatus.controllerStatus
    val connection                         = "Status: "
    val lastSeen: String = federationMemberStatus.controllerLastSeen
      .map(timestamp => {
        val duration = Duration.fromNanos(java.time.Duration.between(timestamp, Instant.now()).toNanos)
        formatScalaDuration(duration)
      })
      .map(lastSeenReadable => s"(last seen $lastSeenReadable)")
      .getOrElse("")

    (checkInStatus, controllerStatus) match
      case (_, ControllerStatus.Offline) => printBulletPoint(s"$connection${Color.Yellow("Offline")} $lastSeen")
      case (CheckInStatus.ACK, ControllerStatus.Online) =>
        printBulletPoint(s"$connection${Color.Green("Online")} $lastSeen")
      case (CheckInStatus.NACK, _)    => printBulletPoint(s"$connection${Color.Yellow("Not checked in")} $lastSeen")
      case (CheckInStatus.Unknown, _) => printBulletPoint(s"$connection${Color.Yellow(s"$checkInStatus")} $lastSeen")
      case (CheckInStatus.ACK, ControllerStatus.Unknown) =>
        printBulletPoint(s"$connection${Color.Yellow("Checked in, waiting for 1st hearbeat")}")
  }

  private def formatScalaDuration(duration: FiniteDuration): String = {
    val parts = Seq(
      (duration.toDays, "day"),
      (duration.toHours   % 24, "hour"),
      (duration.toMinutes % 60, "minute"),
      (duration.toSeconds % 60, "second")
    ).collect {
      case (value, unit) if value > 0 => s"$value $unit${if (value == 1) "" else "s"}"
    }
    if (parts.isEmpty) "just now" else parts.mkString(", ") + " ago"
  }

  private def printSyncStatus(syncStatus: SyncStatus): Unit = {
    print(s"  ${Color.Blue("*")} Status: ")
    if syncStatus.eq(SyncStatus.Sync) then Print.green(s"Synced")
    else if syncStatus.eq(SyncStatus.Syncing) then Print.cyan(s"Syncing...")
    else Print.yellow(s"$syncStatus")
  }

  private def printWaitingForMemberBackendToBeOnline(): IO[Unit] = {
    IO(Clear()) >>
      IO(println(s"${Color.LightMagenta("Waiting for trile to be up & running in the machine...")}"))
  }

  private def printSomethingWentWrong(): IO[Unit] = {
    IO(Clear()) >>
      IO(println(s"${Color.LightMagenta("Oh-oh. It was not possible to communicate with trile. Is it running?")}"))
  }

  private def humanReadableByteCount(bytes: Long, si: Boolean = true): String = {
    val unit = if (si) 1000 else 1024
    if (bytes < unit) return bytes + " B"
    val exp = (Math.log(bytes.toDouble) / Math.log(unit.toDouble)).toInt
    val pre = (if (si) "kMGTPE" else "KMGTPE").charAt(exp - 1) + (if (si) "" else "i")
    String.format("%.1f %sB", bytes.toDouble / Math.pow(unit.toDouble, exp.toDouble), pre)
  }

  private def printBulletPoint(message: String): Unit = {
    println(s"  ${Color.Blue("*")} $message")
  }
}
