package acab.devcon0.services.command.update

import acab.devcon0.dtos.*
import acab.devcon0.services.FederationMemberNicknameLister
import acab.devcon0.services.shell.PrintLn
import cats.effect.IO
import cats.implicits._

object UpdateFederationCommandHandler {

  def apply(inputVariables: FederationUpdateInputVariables): IO[Unit] = {

    val federationName: String                  = inputVariables.federationName
    val federationMemberNicknames: List[String] = FederationMemberNicknameLister(federationName)
    val controllerInputVariables: FederationControllerUpdateInputVariables = FederationControllerUpdateInputVariables(
      federationName = federationName
    )

    for
      _ <- updateController(inputVariables)
      _ <- federationMemberNicknames
        .map(FederationMemberUpdateInputVariables(federationName, _))
        .traverse(updateMember)
    yield ()
  }

  private def updateController(inputVariables: FederationUpdateInputVariables): IO[Unit] = {
    val controllerInputVariables: FederationControllerUpdateInputVariables = FederationControllerUpdateInputVariables(
      federationName = inputVariables.federationName
    )
    (for
      _ <- IO(PrintLn.blue("=== Controller update"))
      _ <- UpdateControllerCommandHandler(controllerInputVariables)
    yield ())
      .attemptTap(PrintLn.subStepAttemptTap("Controller update", _, true)) >>
      IO(println())
  }

  private def updateMember(inputVariables: FederationMemberUpdateInputVariables): IO[Unit] = {
    (for
      _ <- IO(PrintLn.blue(s"=== Member '${inputVariables.nickname}' update"))
      _ <- UpdateMemberCommandHandler(inputVariables)
    yield ())
      .attemptTap(PrintLn.subStepAttemptTap(s"Member '${inputVariables.nickname}' update", _, true)) >>
      IO(println())
  }
}
