package acab.devcon0.services.command.update

import acab.devcon0.dtos.*
import acab.devcon0.dtos.TrileCli.Component.*
import acab.devcon0.dtos.TrileCli.EnvironmentVariables.*
import acab.devcon0.dtos.TrileCli.File.*
import acab.devcon0.dtos.TrileCli.{Component, Defaults, Environment, File}
import acab.devcon0.services.{EnvVarsFileLoader, PathSanitizer}
import acab.devcon0.services.shell.{DockerComposeUp, EnvSust}
import acab.devcon0.{files, services}
import cats.effect.IO
import ujson.Value.Value

object UpdateMemberCommandHandler {

  def apply(inputVariables: FederationMemberUpdateInputVariables): IO[Unit] = {
    val userHomeAbsolutePath: String = PathSanitizer.expandHome(Defaults.userHomeAbsolutePath)
    val configurationPath: String    = s"$userHomeAbsolutePath/.config/trile"
    val federationName: String       = inputVariables.federationName
    val nickname: String             = inputVariables.nickname
    val installationPath: String     = s"$configurationPath/federations/$federationName/members/$nickname"
    val environmentVariables: TrileFederationEnvVars = EnvVarsFileLoader(installationPath + "/.installationEnvVars")
    val dockerComposeAbsolutePath: String            = getDockerComposeAbsolutePath(installationPath)

    updateConfigurationFiles(environmentVariables, installationPath) >>
      DockerComposeUp(dockerComposeAbsolutePath)
  }

  private def updateConfigurationFiles(envVars: TrileFederationEnvVars, installationPath: String): IO[Unit] = {
    val environment                  = Environment.valueOf(envVars(TRILE_ENVIRONMENT))
    val dockerComposeAbsolutePath    = getDockerComposeAbsolutePath(installationPath)
    val dockerComposeTemplateContent = files.Loader.get(DockerCompose, FederationMember, environment)

    EnvSust(envVars, dockerComposeTemplateContent, dockerComposeAbsolutePath)
  }

  private def getDockerComposeAbsolutePath(configurationFolderAbsolutePath: String): String = {
    configurationFolderAbsolutePath + "/docker-compose.yaml"
  }
}
