package acab.devcon0.services

import acab.devcon0.dtos.TrileCli.Defaults
import acab.devcon0.dtos.TrileCli.EnvironmentVariables.TrileFederationEnvVars

import java.nio.file.{Files, Path, Paths}
import java.util.stream.Collectors
import scala.jdk.CollectionConverters.ListHasAsScala
import scala.util.Try

object FederationMemberNicknameLister {

  def apply(federationName: String): List[String] = {
    val userHomeAbsolutePath: String = PathSanitizer.expandHome(Defaults.userHomeAbsolutePath)
    val configurationPath: String    = s"$userHomeAbsolutePath/.config/trile"
    val membersPath: String          = s"$configurationPath/federations/$federationName/members"
    val stream                       = Files.walk(Paths.get(membersPath), 1)
    Try(stream.filter(Files.isDirectory(_)).collect(Collectors.toList()))
      .fold(
        _ => List(),
        javaList => {
          stream.close()
          javaList.asScala.map(_.getFileName.toString).toList.tail
        }
      )
  }
}

object FederationMemberLoader {

  def apply(federationName: String, nickname: String): TrileFederationEnvVars = {
    val userHomeAbsolutePath: String = PathSanitizer.expandHome(Defaults.userHomeAbsolutePath)
    val configurationPath: String    = s"$userHomeAbsolutePath/.config/trile"
    val installationPath: String     = s"$configurationPath/federations/${federationName}/members/${nickname}"
    EnvVarsFileLoader(installationPath + "/.installationEnvVars")
  }
}
