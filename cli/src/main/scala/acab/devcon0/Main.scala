package acab.devcon0

import acab.devcon0.program.commands.{InstallCommand, StatusCommand, UpdateCommand}
import cats.effect.unsafe.IORuntime
import com.monovore.decline.*

object TrileCommandApp
    extends CommandApp(
      name = "trile",
      header = "Trile CLI. Complete docs version can be found at https://trile.link",
      main = TrileCommand.main()
    )

object TrileCommand {

  implicit val trileRuntime: IORuntime = cats.effect.unsafe.IORuntime.global

  def main(): Opts[Unit] = {
    Opts.subcommands(InstallCommand.command, UpdateCommand.command, StatusCommand.command)
  }
}
