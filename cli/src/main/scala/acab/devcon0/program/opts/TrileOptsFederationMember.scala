package acab.devcon0.program.opts

import acab.devcon0.dtos.TrileCli.Defaults
import acab.devcon0.services.PathSanitizer
import com.monovore.decline.*

object TrileOptsFederationMember {

  val nicknameOpt: Opts[String] = Opts
    .option[String](
      long = "nickname",
      short = "n",
      help = s"The name you want to use in the federation (default=${Defaults.nickname})"
    )
    .withDefault(Defaults.nickname)

  val sharedDiskSpaceGBMandatoryOpt: Opts[Option[Int]] = Opts
    .option[Int](
      "shared-disk-space-gb",
      help = "Amount of GBs the federation will use from your machine for replication purposes"
    )
    .orNone

  val sharingFolderMandatoryOpt: Opts[Option[String]] =
    Opts.option[String]("sharing-folder", help = "Location of your sharing folder").orNone

  val joinToken: Opts[String] = Opts
    .option[String](long = "join-token", short = "j", help = s"The join token the federation controller shall provide")

  val federationUrl: Opts[String] = Opts
    .option[String](
      "federation-url",
      help = s"URL of the federation. For instance: demo.trile.link"
    )
    .map(PathSanitizer.expandHome)
}
