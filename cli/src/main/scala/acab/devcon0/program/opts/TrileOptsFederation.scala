package acab.devcon0.program.opts

import acab.devcon0.dtos.TrileCli.Defaults
import acab.devcon0.services.PathSanitizer
import com.monovore.decline.*

object TrileOptsFederation {

  val environmentOpt: Opts[String] = Opts
    .option[String](
      "environment",
      help = "Generate for testing or real-world usage purposes (default=" + Defaults.environment + ")"
    )
    .withDefault(Defaults.environment)

  val federationNameOpt: Opts[String] = Opts
    .option[String](long = "federation-name", short = "f", help = s"The name you want to use in the federation")

  val assumeYesFlag: Opts[Boolean] = Opts
    .flag(
      long = "assume-yes",
      help = s"If set, installation proceeds without confirmation.",
      short = "y"
    )
    .orFalse

  val watchFlag: Opts[Boolean] = Opts
    .flag(
      long = "watch",
      short = "w",
      help = s"If set, runs the action every second"
    )
    .orFalse
}
