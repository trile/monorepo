package acab.devcon0.program.commands

import acab.devcon0.TrileCommand
import acab.devcon0.dtos.FederationMemberInstallInputVariables
import acab.devcon0.dtos.TrileCli.Environment
import acab.devcon0.dtos.TrileCli.Environment.*
import acab.devcon0.dtos.TrileCli.EnvironmentVariables.*
import acab.devcon0.program.opts.{TrileOptsFederation, TrileOptsFederationMember}
import acab.devcon0.services.FederationControllerService
import acab.devcon0.services.command.install.InstallMemberCommandHandler
import acab.devcon0.services.shell.{Clear, ConfirmAndRun, Greeter, PrintLn}
import cats.effect.IO
import cats.implicits.*
import com.monovore.decline.*

import scala.io.StdIn
import scala.util.Try

object InstallFederationMemberCommand {

  def apply(): Command[Unit] =
    Command(name = "member", header = "Run the installation for a member.") {
      (
        TrileOptsFederation.environmentOpt.map(environmentString => {
          if environmentString == "dev" then Dev else Production
        }),
        TrileOptsFederationMember.nicknameOpt,
        TrileOptsFederationMember.sharedDiskSpaceGBMandatoryOpt,
        TrileOptsFederationMember.sharingFolderMandatoryOpt,
        TrileOptsFederationMember.federationUrl,
        TrileOptsFederationMember.joinToken,
        TrileOptsFederation.assumeYesFlag
      ).mapN {
        (
            environment,
            nickname,
            sharedDiskSpaceGBMandatory,
            sharingFolderMandatory,
            federationUrl,
            joinToken,
            assumeYes
        ) =>
          {
            for fedvarsMap <- FederationControllerService.getFedvars(environment, federationUrl, joinToken)
            yield {
              Greeter(action = s"join the '${fedvarsMap(TRILE_FEDERATION_NAME)}'")

              val sharedDiskSpaceGB: Int = loadOrReadSharedDiskSpaceGB(sharedDiskSpaceGBMandatory)
              val sharingFolder          = loadOrReadSharingFolder(sharingFolderMandatory)
              val federationMemberInstallInputVariables: FederationMemberInstallInputVariables =
                FederationMemberInstallInputVariables(
                  environment,
                  nickname,
                  fedvarsMap,
                  sharedDiskSpaceGB,
                  sharingFolder
                )
              printInputVariables(federationMemberInstallInputVariables)

              ConfirmAndRun(
                skipConfirmation = assumeYes,
                action = () => triggerInstallation(fedvarsMap, federationMemberInstallInputVariables)
              )
            }
          }
            .attemptTap(_.fold(throwable => IO(println(throwable)), _ => IO(println())))
            .unsafeToFuture()(TrileCommand.trileRuntime)
            .wait()
      }
    }

  private def triggerInstallation(
      fedvarsMap: TrileFederationEnvVars,
      federationMemberInstallInputVariables: FederationMemberInstallInputVariables
  ): Unit = {
    InstallMemberCommandHandler(federationMemberInstallInputVariables, fedvarsMap)
      .attemptTap(_.fold(_ => IO(println()), _ => IO(println())))
      .attemptTap(PrintLn.subStepAttemptTap("Member installation", _, true))
      .unsafeToFuture()(TrileCommand.trileRuntime)
      .wait()
  }

  private def printInputVariables(inputVariables: FederationMemberInstallInputVariables): Unit = {
    println()
    println(s"These are you input values. Please review them: ")
    println(s"- Environment: ${inputVariables.environment}")
    println(s"- Nickname: ${inputVariables.nickname}")
    println(s"- Shared disk space (GBs): ${inputVariables.sharedDiskSpaceGB}")
    println(s"- Shared folder: ${inputVariables.sharingFolder}")
    println(s"- Backend port: ${inputVariables.sharingFolder}")
    println(s"- .fedvars content: ${inputVariables.fedvarsMap}")
    println()
  }

  private def greet(fedvarsMaps: TrileFederationEnvVars): Unit = {
    Clear()
    PrintLn.cyan(s"Welcome to trile! You are up to join the '${fedvarsMaps(TRILE_FEDERATION_NAME)}' federation.")
    PrintLn.cyan(
      s"You can pass your values as arguments. use 'install member --help' to see the list. " +
        s"In order to complete the installation, all values need to be provided."
    )
  }

  private def loadOrReadSharingFolder(sharingFolderMandatory: Option[String]): String = {
    sharingFolderMandatory match
      case Some(sharingFolder) => sharingFolder
      case None                => readSharingFolder
  }

  private def readSharingFolder: String = {
    println()
    val prompt: String             = "Sharing directory (absolute path): "
    var sharingFolder: Try[String] = readSharingFolder(prompt)
    while sharingFolder.isFailure
    do {
      PrintLn.yellow(s"Please provide an absolute path to your sharing folder: ")
      sharingFolder = readSharingFolder(prompt)
    }
    sharingFolder.get
  }

  private def loadOrReadSharedDiskSpaceGB(sharedDiskSpaceGBMandatory: Option[Int]): Int = {
    sharedDiskSpaceGBMandatory match
      case Some(sharedDiskSpaceGB) => sharedDiskSpaceGB
      case None                    => readSharedDiskSpaceGB
  }

  private def readSharedDiskSpaceGB: Int = {
    val prompt: String = "How much disk space do you want to let the cluster use? (GBs): "
    println()
    var sharedDiskSpaceGBTry: Try[Int] = readIntLine(prompt)
    while sharedDiskSpaceGBTry.isFailure
    do {
      PrintLn.yellow(s"Please provide a valid value (GBs): ")
      sharedDiskSpaceGBTry = readIntLine(prompt)
    }
    sharedDiskSpaceGBTry.get
  }

  private def readSharingFolder(prompt: String): Try[String] = {
    Try(StdIn.readLine(prompt)).filter(!_.isBlank)
  }

  private def readIntLine(prompt: String): Try[Int] = {
    Try(StdIn.readLine(prompt)).map(_.toInt)
  }
}
