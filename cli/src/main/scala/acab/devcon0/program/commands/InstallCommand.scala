package acab.devcon0.program.commands

import com.monovore.decline.*

object InstallCommand {

  val command: Command[Unit] =
    Command(name = "install", header = "Run the installation.") {
      Opts.subcommands(InstallFederationMemberCommand(), InstallFederationControllerCommand())
    }
}
