package acab.devcon0.codecs

import acab.devcon0.dtos
import acab.devcon0.dtos.http.{FederationMemberStatus, IpfsClusterPeers}
import acab.devcon0.dtos.{CheckInStatus, ControllerStatus, SyncStatus}
import cats.effect.IO
import io.circe.jawn.decode
import io.circe.{Decoder, HCursor}

import java.time.Instant

object CirceCodecs {

  object Decoders {
    import acab.devcon0.codecs.IpfsClusterCodecs.Decoders.*
    implicit val federationMemberStatus: Decoder[FederationMemberStatus] = (c: HCursor) => {
      for
        nickname              <- c.downField("nickname").as[String]
        p2pPeerId             <- c.downField("p2pPeerId").as[String]
        sharedFolderPin       <- c.downField("sharedFolderPin").as[String]
        sharedFolderFileCount <- c.downField("sharedFolderFileCount").as[Int]
        sharedFolderSizeBytes <- c.downField("sharedFolderSizeBytes").as[Long]
        checkInStatusStr      <- c.downField("checkInStatus").as[String]
        checkInStatus = CheckInStatus.valueOf(checkInStatusStr)
        controllerStatusStr <- c.downField("controllerStatus").as[String]
        controllerLastSeen  <- c.downField("controllerLastSeen").as[Option[Instant]]
        controllerStatus = ControllerStatus.valueOf(controllerStatusStr)
        syncStatusStr <- c.downField("syncStatus").as[String]
        syncStatus = SyncStatus.valueOf(syncStatusStr)
        ipfsClusterPeers     <- c.downField("ipfsClusterPeers").as[IpfsClusterPeers]
        ipfsClusterPinsCount <- c.downField("ipfsClusterPinsCount").as[Int]
        ipfsRepoSize         <- c.downField("ipfsRepoSize").as[Long]
        ipfsStorageMax       <- c.downField("ipfsStorageMax").as[Long]
      yield dtos.http.FederationMemberStatus(
        nickname = nickname,
        p2pPeerId = p2pPeerId,
        sharedFolderPin = sharedFolderPin,
        sharedFolderFileCount = sharedFolderFileCount,
        sharedFolderSizeBytes = sharedFolderSizeBytes,
        checkInStatus = checkInStatus,
        syncStatus = syncStatus,
        ipfsClusterPeers = ipfsClusterPeers,
        ipfsClusterPinsCount = ipfsClusterPinsCount,
        ipfsRepoSize = ipfsRepoSize,
        ipfsStorageMax = ipfsStorageMax,
        controllerStatus = controllerStatus,
        controllerLastSeen = controllerLastSeen
      )
    }

    object FederationMemberStatus {
      def apply(rawJson: String): IO[FederationMemberStatus] = IO.fromEither(decode[FederationMemberStatus](rawJson))
    }

  }
}
