package acab.devcon0.codecs

import acab.devcon0.dtos
import acab.devcon0.dtos.http.*
import io.circe.generic.semiauto.deriveDecoder
import io.circe.*

object IpfsClusterCodecs {

  object Decoders {
    implicit val ipfsClusterPins: Decoder[IpfsClusterPins]      = deriveDecoder
    implicit val ipfsClusterPin: Decoder[IpfsClusterPin]        = deriveDecoder
    implicit val peerIpfsInfo: Decoder[IpfsClusterPeerIpfsInfo] = deriveDecoder
    implicit val peers: Decoder[IpfsClusterPeers]               = deriveDecoder
    implicit val peer: Decoder[IpfsClusterPeer]                 = deriveDecoder
  }
}
