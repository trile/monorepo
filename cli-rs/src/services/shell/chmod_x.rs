use std::process::Command;
use super::sub_step;

const CMD_LABEL: &str = "Granting permissions";

pub fn chmod_x(path: &str) -> Result<(), Box<dyn std::error::Error>> {
   let label = format!("{}: {}", CMD_LABEL, path.split('/').last().unwrap_or(path));

    let mut binding = Command::new("sh");
    let cmd: &mut Command = binding
        .arg("-c")
        .arg(format!("chmod +x {}", path));

    sub_step::without_progress(cmd, &label, label.len())
}
