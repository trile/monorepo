use super::println::PrintLn;
use std::io::Write;

pub fn greeter(action: &str) {
    clear_screen();
    PrintLn::cyan(&format!("You are up to {}.", action));
    PrintLn::cyan("Run this command with --help to see the list of options. Pay attention to the mandatory ones.");
    println!();
}

fn clear_screen() {
    print!("\x1B[2J\x1B[1;1H"); // ANSI escape code to clear the screen and move the cursor to the top
    std::io::stdout().flush().unwrap();
}