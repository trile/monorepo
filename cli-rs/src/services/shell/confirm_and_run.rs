use std::io::{self, Write};

pub fn confirm_and_run<F>(skip_confirmation: bool, action: F)
where
    F: FnOnce(),
{
    if skip_confirmation || user_confirms() {
        action();
    } else {
        abort();
    }
}

fn user_confirms() -> bool {
    print!("Continue? [y/N]: ");
    io::stdout().flush().expect("Failed to flush stdout");

    let mut confirmation = String::new();
    io::stdin().read_line(&mut confirmation).expect("Failed to read line");

    confirmation.trim().eq_ignore_ascii_case("y")
}

fn abort() {
    println!("Aborting...See you soon");
    std::process::exit(0);
}