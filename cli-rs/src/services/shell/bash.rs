use std::process::Command;
use super::sub_step;

const CMD_LABEL: &str = "Executing shell command";

pub fn bash(command_input: &str) {
    let label = format!("{}: {}", CMD_LABEL, command_input);

    let mut binding = Command::new("bash");
    let cmd: &mut Command = binding
        .arg("-c")
        .arg(command_input);

    sub_step::without_progress(cmd, &label, command_input.len()).expect("Failed to execute shell command");
}
