use super::sub_step;
use std::path::Path;
use std::process::Command;

const CMD_LABEL: &str = "Ensuring folder exists";

pub fn mkdir_p(folder_path: &str)  -> Result<(), Box<dyn std::error::Error>> {
    let mut binding = Command::new("sh");
    let cmd: &mut Command = binding
        .arg("-c")
        .arg(format!("mkdir -p {}", folder_path));
    let label = format!(
        "{}: {}",
        CMD_LABEL,
        Path::new(folder_path).file_name().unwrap_or_else(|| Path::new(folder_path).as_os_str()).to_string_lossy());
    sub_step::with_progress(cmd, &label)
}
