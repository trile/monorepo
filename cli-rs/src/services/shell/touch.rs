use super::sub_step;
use std::path::Path;
use std::process::Command;

const CMD_LABEL: &str = "Ensuring file exists";

pub fn touch(file_path: &str) -> Result<(), Box<dyn std::error::Error>> {
    let label = format!("{}: {}", CMD_LABEL, Path::new(file_path).file_name().unwrap_or_else(|| Path::new(file_path).as_os_str()).to_string_lossy());

    let mut binding = Command::new("sh");
    let cmd: &mut Command = binding.arg("-c").arg(format!("touch {}", file_path));

    sub_step::with_progress(cmd, &label)
}
