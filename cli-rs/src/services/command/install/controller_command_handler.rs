use crate::dtos::input::federation_controller_install::FederationControllerInstallInputVariables;
use crate::dtos::trile_cli::{
    defaults, environment_variables::*, Component, File,
};
use crate::files::loader;
use crate::services::federation_controller_environment_variables::federation_controller_environment_variables;
use crate::services::federation_controller_service::get_peer_id;
use crate::services::shell::bash::bash;
use crate::services::shell::chmod_x::chmod_x;
use crate::services::shell::docker_compose_up::docker_compose_up;
use crate::services::shell::env_sust::env_sust;
use crate::services::shell::ipfs_generate_swarm_key::ipfs_generate_swarm_key;
use crate::services::shell::mkdir_p::mkdir_p;
use crate::services::shell::println::PrintLn;
use crate::services::shell::sudo_rm::sudo_rm;
use crate::services::shell::touch::touch;
use serde_json::Value;
use std::collections::HashMap;
use std::fs;

pub fn install_controller_command_handler(
    input_variables: FederationControllerInstallInputVariables,
) -> Result<(), Box<dyn std::error::Error>> {
    let environment_vars = federation_controller_environment_variables(input_variables.clone());
    let configuration_folder_absolute_path = get_configuration_folder_absolute_path(&environment_vars);
    let docker_compose_absolute_path = get_docker_compose_absolute_path(&configuration_folder_absolute_path);

    generate_configuration_files(input_variables.clone(), &environment_vars)?;
    docker_compose_up(&docker_compose_absolute_path)?;

    if input_variables.generate_ssl {
        generate_ssl_certificate(&environment_vars)?;
    }

    generate_configuration_files_post_ssl(input_variables.clone(), &environment_vars)?;
    docker_compose_up(&docker_compose_absolute_path)?;
    generate_fedvars(input_variables.clone(), &environment_vars)?;
    generate_installation_env_vars(&environment_vars)?;
    print_join_token(&environment_vars)?;

    Ok(())
}

fn print_join_token(environment_vars: &TrileFederationEnvVars) -> Result<(), Box<dyn std::error::Error>> {
    PrintLn::sub_step_info(&format!("Federation join token: {}", environment_vars[TFC_JOIN_TOKEN]), true);
    Ok(())
}

fn generate_fedvars(
    input_variables: FederationControllerInstallInputVariables,
    environment_vars: &TrileFederationEnvVars,
) -> Result<(), Box<dyn std::error::Error>> {
    let network_address = &environment_vars[TFC_NETWORK_ADDRESS];
    let environment = input_variables.environment;
    let peer_id = get_peer_id(environment, network_address)?;

    let fedvars_absolute_path = &environment_vars[TFC_FEDVARS_ABSOLUTE_PATH];
    let map = HashMap::from([
        (TRILE_FEDERATION_IPFS_SWARM_KEY_VALUE.to_string(), get_ipfs_swarm_key_value(environment_vars)),
        (TFC_NETWORK_ADDRESS.to_string(), network_address.to_string()),
        (
            TRILE_FEDERATION_NAME.to_string(),
            environment_vars[TRILE_FEDERATION_NAME].to_string(),
        ),
        (TFC_IPFS_PEER_ID.to_string(), get_ipfs_peer_id(environment_vars)),
        (TFC_IPFS_CLUSTER_PEER_ID.to_string(), get_ipfs_cluster_peer_id(environment_vars)),
        (TFC_IPFS_CLUSTER_SECRET.to_string(), get_ipfs_cluster_secret(environment_vars)),
        (TFC_IPFS_CLUSTER_SWARM_PORT.to_string(), defaults::IPFS_CLUSTER_SWARM_PORT.to_string()),
        (TFC_IPFS_SWARM_PORT.to_string(), defaults::IPFS_SWARM_PORT.to_string()),
        (TFC_P2P_PEER_ID.to_string(), peer_id),
        (TFC_P2P_PORT.to_string(), "7078".to_string()),
    ]);

    let fedvars_content: String = map.iter().map(|(k, v)| format!("{}={}", k, v)).collect::<Vec<_>>().join("\n");
    fs::write(fedvars_absolute_path, fedvars_content.as_bytes())?;
    Ok(())
}

fn generate_installation_env_vars(env_vars: &TrileFederationEnvVars) -> Result<(), Box<dyn std::error::Error>> {
    let configuration_folder_absolute_path = get_configuration_folder_absolute_path(env_vars);
    let installation_env_vars_absolute_path = format!("{}/.installationEnvVars", configuration_folder_absolute_path);
    let env_vars_content: String = env_vars.iter()
        .map(|(k, v)| format!("{}={}", k, v))
        .collect::<Vec<_>>()
        .join("\n");
    fs::write(installation_env_vars_absolute_path, env_vars_content.as_bytes())?;
    Ok(())
}

fn get_ipfs_peer_id(env_vars: &TrileFederationEnvVars) -> String {
    let ipfs_configuration_folder_absolute_path = get_ipfs_configuration_folder_absolute_path(env_vars);
    let path = format!("{}/config", ipfs_configuration_folder_absolute_path);
    let contents = fs::read_to_string(path).expect("Failed to read IPFS config file");
    let json: Value = serde_json::from_str(&contents).expect("Failed to parse JSON");
    json["Identity"]["PeerID"].as_str().unwrap().to_string()
}

fn get_ipfs_cluster_secret(env_vars: &TrileFederationEnvVars) -> String {
    let ipfs_cluster_configuration_folder_absolute_path = get_ipfs_cluster_configuration_folder_absolute_path(env_vars);
    let path = format!("{}/service.json", ipfs_cluster_configuration_folder_absolute_path);
    let contents = fs::read_to_string(path).expect("Failed to read IPFS Cluster service JSON file");
    let json: Value = serde_json::from_str(&contents).expect("Failed to parse JSON");
    json["cluster"]["secret"].as_str().unwrap().to_string()
}

fn get_ipfs_cluster_peer_id(env_vars: &TrileFederationEnvVars) -> String {
    let ipfs_cluster_configuration_folder_absolute_path = get_ipfs_cluster_configuration_folder_absolute_path(env_vars);
    let path = format!("{}/identity.json", ipfs_cluster_configuration_folder_absolute_path);
    let contents = fs::read_to_string(path).expect("Failed to read IPFS Cluster identity JSON file");
    let json: Value = serde_json::from_str(&contents).expect("Failed to parse JSON");
    json["id"].as_str().unwrap().to_string()
}

fn get_ipfs_swarm_key_value(env_vars: &TrileFederationEnvVars) -> String {
    let ipfs_configuration_folder_absolute_path = get_ipfs_configuration_folder_absolute_path(env_vars);
    let path = format!("{}/swarm.key", ipfs_configuration_folder_absolute_path);
    let contents = fs::read_to_string(path).expect("Failed to read swarm key file");
    contents.lines().last().unwrap().to_string()
}

pub fn generate_configuration_files(
    input_variables: FederationControllerInstallInputVariables,
    env_vars: &TrileFederationEnvVars,
) -> Result<(), Box<dyn std::error::Error>> {
    let environment = input_variables.environment;
    let config_folder_absolute_path = get_configuration_folder_absolute_path(env_vars);
    let ipfs_configuration_folder_absolute_path = get_ipfs_configuration_folder_absolute_path(env_vars);
    let ipfs_cluster_configuration_folder_absolute_path = get_ipfs_cluster_configuration_folder_absolute_path(env_vars);
    let nginx_folder_absolute_path = get_nginx_configuration_folder_absolute_path(&config_folder_absolute_path);
    let certbot_configuration_folder_absolute_path = get_certbot_configuration_folder_absolute_path(&config_folder_absolute_path);
    let ssl_certificate_generator_file_absolute_path = get_ssl_certificate_absolute_path(&config_folder_absolute_path);
    let nginx_configuration_file_absolute_path = get_nginx_configuration_file_absolute_path(&nginx_folder_absolute_path);
    let nginx_pre_ssl_configuration_file_absolute_path = get_nginx_pre_ssl_configuration_file_absolute_path(&nginx_folder_absolute_path);
    let swarm_key_absolute_path = format!("{}/swarm.key", ipfs_configuration_folder_absolute_path);
    let docker_compose_absolute_path = get_docker_compose_absolute_path(&config_folder_absolute_path);
    let fedvars_absolute_path = ge_fedvars_absolute_path(env_vars);

    let docker_compose_template_content = loader::get(File::DockerCompose, Component::FederationController, environment.clone());
    let nginx_conf_template_content = loader::get(File::NginxConf, Component::FederationController, environment.clone());
    let nginx_pre_ssl_conf_template_content = loader::get(File::NginxPreSslConf, Component::FederationController, environment.clone());
    let ssl_certificate_generator_template_content = loader::get(File::SslCertificate, Component::FederationController, environment.clone());

    sudo_rm(&config_folder_absolute_path)?;
    mkdir_p(&config_folder_absolute_path)?;
    touch(&fedvars_absolute_path)?;
    mkdir_p(&certbot_configuration_folder_absolute_path)?;
    mkdir_p(&nginx_folder_absolute_path)?;
    mkdir_p(&ipfs_configuration_folder_absolute_path)?;
    mkdir_p(&ipfs_cluster_configuration_folder_absolute_path)?;
    ipfs_generate_swarm_key(&swarm_key_absolute_path)?;
    let mut env_vars_pre_boot: HashMap<TrileEnvironmentVariable, String> = env_vars.clone();
    env_vars_pre_boot.insert(TFC_REVERSE_PROXY_CONFIGURATION.to_string(), nginx_pre_ssl_configuration_file_absolute_path.clone());
    env_vars_pre_boot.insert(TRILE_FEDERATION_IPFS_SWARM_KEY_VALUE.to_string(), get_ipfs_swarm_key_value(env_vars));
    env_sust(env_vars.clone(), &docker_compose_template_content, &docker_compose_absolute_path)?;
    env_sust(env_vars.clone(), &nginx_conf_template_content, &nginx_configuration_file_absolute_path)?;
    env_sust(env_vars.clone(), &nginx_pre_ssl_conf_template_content, &nginx_pre_ssl_configuration_file_absolute_path)?;
    env_sust(env_vars_pre_boot.clone(), &docker_compose_template_content, &docker_compose_absolute_path)?;
    env_sust(env_vars.clone(), &ssl_certificate_generator_template_content, &ssl_certificate_generator_file_absolute_path)?;
    chmod_x(&ssl_certificate_generator_file_absolute_path)?;
    Ok(())
}

fn generate_configuration_files_post_ssl(
    input_variables: FederationControllerInstallInputVariables,
    env_vars: &TrileFederationEnvVars,
) -> Result<(), Box<dyn std::error::Error>> {
    let environment = input_variables.environment;
    let configuration_folder_absolute_path = get_configuration_folder_absolute_path(env_vars);
    let docker_compose_absolute_path = get_docker_compose_absolute_path(&configuration_folder_absolute_path);
    let docker_compose_template_content = loader::get(File::DockerCompose, Component::FederationController, environment);
    let mut env_vars_post_ssl: HashMap<TrileEnvironmentVariable, String> = env_vars.clone();
    env_vars_post_ssl.insert(TRILE_FEDERATION_IPFS_SWARM_KEY_VALUE.to_string(), get_ipfs_swarm_key_value(env_vars));
    env_sust(env_vars_post_ssl, &docker_compose_template_content, &docker_compose_absolute_path)?;
    Ok(())
}

pub fn generate_ssl_certificate(env_vars: &TrileFederationEnvVars) -> Result<(), Box<dyn std::error::Error>> {
    let configuration_folder_absolute_path = get_configuration_folder_absolute_path(env_vars);
    let ssl_certificate_generator_file_absolute_path = get_ssl_certificate_absolute_path(&configuration_folder_absolute_path);
    bash(&ssl_certificate_generator_file_absolute_path);
    Ok(())
}

fn get_docker_compose_absolute_path(configuration_folder_absolute_path: &str) -> String {
    format!("{}/docker-compose.yaml", configuration_folder_absolute_path)
}

fn get_ssl_certificate_absolute_path(configuration_folder_absolute_path: &str) -> String {
    format!("{}/generate-lets-encrypt-ssl-certificates", configuration_folder_absolute_path)
}

fn get_certbot_configuration_folder_absolute_path(configuration_folder_absolute_path: &str) -> String {
    format!("{}/certbot/conf", configuration_folder_absolute_path)
}

fn get_nginx_configuration_folder_absolute_path(configuration_folder_absolute_path: &str) -> String {
    format!("{}/nginx", configuration_folder_absolute_path)
}

fn get_nginx_configuration_file_absolute_path(nginx_configuration_folder_absolute_path: &str) -> String {
    format!("{}/nginx.conf", nginx_configuration_folder_absolute_path)
}

fn get_nginx_pre_ssl_configuration_file_absolute_path(nginx_configuration_folder_absolute_path: &str) -> String {
    format!("{}/nginx-pre-ssl.conf", nginx_configuration_folder_absolute_path)
}

fn ge_fedvars_absolute_path(env_vars: &TrileFederationEnvVars) -> String {
    env_vars[TFC_FEDVARS_ABSOLUTE_PATH].clone()
}

fn get_configuration_folder_absolute_path(env_vars: &TrileFederationEnvVars) -> String {
    env_vars[TFC_CONFIGURATION_FOLDER].clone()
}

fn get_ipfs_configuration_folder_absolute_path(env_vars: &TrileFederationEnvVars) -> String {
    env_vars[TRILE_IPFS_DOCKER_CONTAINER_DATA_DIRECTORY].clone()
}

fn get_ipfs_cluster_configuration_folder_absolute_path(env_vars: &TrileFederationEnvVars) -> String {
    env_vars[TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_DATA_DIRECTORY].clone()
}