use crate::dtos::input::federation_controller_update::FederationControllerUpdateInputVariables;
use crate::dtos::input::federation_member_update::FederationMemberUpdateInputVariables;
use crate::dtos::input::federation_update::FederationUpdateInputVariables;
use crate::services::command::update::controller_command_handler::update_controller_command_handler;
use crate::services::command::update::member_command_handler::update_member_command_handler;
use crate::services::federation_member_nickname_listener::federation_member_nickname_lister;
use crate::services::shell::println::PrintLn;
use std::error::Error;

pub fn update_federation_command_handler(input_variables: FederationUpdateInputVariables) -> Result<(), Box<dyn Error>> {
    let federation_name = input_variables.federation_name.clone();
    let federation_member_nicknames = federation_member_nickname_lister(&federation_name);

    PrintLn::blue("=== Updating federation ===");

    update_controller(input_variables.clone())?;
    for nickname in federation_member_nicknames {
        let member_input_variables = FederationMemberUpdateInputVariables {
            federation_name: federation_name.clone(),
            nickname: nickname.clone(),
        };
        update_member(member_input_variables)?;
    }

    Ok(())
}

fn update_controller(input_variables: FederationUpdateInputVariables) -> Result<(), Box<dyn Error>> {
    let controller_input_variables = FederationControllerUpdateInputVariables {
        federation_name: input_variables.federation_name.clone(),
    };

    PrintLn::blue("=== Controller update");
    let result = update_controller_command_handler(controller_input_variables);
    PrintLn::sub_step_attempt_tap("Controller update", result,true);
    println!();
    Ok(())
}

fn update_member(input_variables: FederationMemberUpdateInputVariables) -> Result<(), Box<dyn Error>> {
    PrintLn::blue(&format!("=== Member '{}' update", input_variables.clone().nickname));
    let result = update_member_command_handler(input_variables.clone());
    PrintLn::sub_step_attempt_tap(&format!("Member '{}' update", input_variables.nickname), result,true);
    println!();
    Ok(())
}