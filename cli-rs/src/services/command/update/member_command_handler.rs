use crate::dtos::input::federation_member_update::FederationMemberUpdateInputVariables;
use crate::dtos::trile_cli::environment_variables::{TrileFederationEnvVars, TRILE_ENVIRONMENT};
use crate::dtos::trile_cli::{defaults, Component, Environment, File};
use crate::files::loader;
use crate::services::env_vars_file_loader::env_vars_file_loader;
use crate::services::path_sanitizer;
use crate::services::shell::docker_compose_up::docker_compose_up;
use crate::services::shell::env_sust::env_sust;
use std::str::FromStr;

pub fn update_member_command_handler(input_variables: FederationMemberUpdateInputVariables) -> Result<(), Box<dyn std::error::Error>> {
    let user_home_absolute_path = path_sanitizer::expand_home(&defaults::user_home_absolute_path());
    let configuration_path = format!("{}/.config/trile", user_home_absolute_path);
    let federation_name = &input_variables.federation_name;
    let nickname = &input_variables.nickname;
    let installation_path = format!("{}/federations/{}/members/{}", configuration_path, federation_name, nickname);
    let environment_variables = env_vars_file_loader(&(installation_path.clone() + "/.installationEnvVars"));
    let docker_compose_absolute_path = get_docker_compose_absolute_path(&installation_path);

    update_configuration_files(&environment_variables, &installation_path)?;
    docker_compose_up(&docker_compose_absolute_path)?;

    Ok(())
}

fn update_configuration_files(env_vars: &TrileFederationEnvVars, installation_path: &str) -> Result<(), Box<dyn std::error::Error>> {
    let env = Environment::from_str(&env_vars[TRILE_ENVIRONMENT])?;
    let docker_compose_absolute_path = get_docker_compose_absolute_path(installation_path);
    let docker_compose_template_content = loader::get(File::DockerCompose, Component::FederationMember, env);

    env_sust(env_vars.clone(), &docker_compose_template_content, &docker_compose_absolute_path)?;
    Ok(())
}

fn get_docker_compose_absolute_path(configuration_folder_absolute_path: &str) -> String {
    format!("{}/docker-compose.yaml", configuration_folder_absolute_path)
}