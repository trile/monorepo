use crate::dtos::http::federation_member_status::FederationMemberStatus;
use crate::dtos::p2p_status::{CheckInStatus, ControllerStatus, SyncStatus};
use crate::services::federation_member_nickname_listener::federation_member_loader;
use crate::services::federation_member_service;
use crate::services::shell::clear::clear;
use crate::services::shell::println::PrintLn;
use chrono::Utc;
use std::error::Error;
use std::thread;
use std::time::Duration;
use colored::Colorize;
use crate::dtos::trile_cli::environment_variables::{TFC_NETWORK_ADDRESS, TFM_CONFIGURATION_FOLDER, TFM_SHARING_FOLDER};

pub fn run_watch(federation_name: &str, nickname: &str) -> Result<(), Box<dyn Error>> {
    loop {
        match federation_member_service::get_status(federation_name, nickname) {
            Ok(status) => print_federation_member_status(&status, federation_name)?,
            Err(_) => print_waiting_for_member_backend_to_be_online()?,
        }
        thread::sleep(Duration::from_secs(2));
    }
}

pub fn run(federation_name: &str, nickname: &str) -> Result<(), Box<dyn Error>> {
    match federation_member_service::get_status(federation_name, nickname) {
        Ok(status) => print_federation_member_status(&status, federation_name)?,
        Err(error) => {
            eprintln!("{}", error);
            print_something_went_wrong()?
        },
    }
    Ok(())
}

fn print_federation_member_status(
    status: &FederationMemberStatus,
    federation_name: &str,
) -> Result<(), Box<dyn Error>> {
    let nickname = &status.nickname;
    let federation_env_vars = federation_member_loader(federation_name, nickname);
    clear();
    println!();
    print_section_title(&format!("{}@{}", nickname, federation_name));
    print_connection_status(status);
    print_bullet_point(&format!("Peer id: {}", &status.p2p_peer_id));
    print_bullet_point(&format!("Controller: {}", &federation_env_vars[TFC_NETWORK_ADDRESS]));
    print_bullet_point(&format!("Installation path: {}", &federation_env_vars[TFM_CONFIGURATION_FOLDER]));
    println!();
    print_section_title("Shared folder");
    print_sync_status(&status.sync_status);
    print_bullet_point(&format!("Path: {}", &federation_env_vars[TFM_SHARING_FOLDER]));
    print_bullet_point(&format!("Files: {}", status.shared_folder_file_count));
    print_shared_folder_size(status);
    print_bullet_point(&format!("CID: {}", &status.shared_folder_pin));
    println!();
    print_ipfs_section(status);
    Ok(())
}

fn print_ipfs_section(status: &FederationMemberStatus) {
    print_section_title("IPFS");

    let check_in_status: &CheckInStatus = &status.check_in_status;
    let controller_status = &status.controller_status;

    if check_in_status == &CheckInStatus::Ack && controller_status == &ControllerStatus::Online {
        print_bullet_point(&format!("Peers: {}", status.ipfs_cluster_peers.peers.len()));
        print_bullet_point(&format!("Soft copies: {}", status.ipfs_cluster_pins_count));
        print_ipfs_repo_stats(status);
    } else {
        PrintLn::cyan("    Waiting for full online status from the controller...");
    }
}

fn print_section_title(message: &str) {
    println!("=== {} ===", message);
}

fn print_ipfs_repo_stats(status: &FederationMemberStatus) {
    let repo_size = human_readable_byte_count(status.ipfs_repo_size, true);
    let storage_max = human_readable_byte_count(status.ipfs_storage_max, true);
    let usage_percentage = (status.ipfs_repo_size as f64 / status.ipfs_storage_max as f64) * 100.0;
    print_bullet_point(&format!(
        "Disk space usage: {} out of {} ({:.2}%)",
        repo_size, storage_max, usage_percentage
    ));
}

fn print_shared_folder_size(status: &FederationMemberStatus) {
    let size_human_readable = human_readable_byte_count(status.shared_folder_size_bytes, true);
    print_bullet_point(&format!("Size: {}", size_human_readable));
}


fn print_connection_status(status: &FederationMemberStatus) {
    let check_in_status = &status.check_in_status;
    let controller_status = &status.controller_status;
    let connection = "Status: ";
    let last_seen = status
        .controller_last_seen
        .map(|timestamp| {
            let now = Utc::now();
            let duration_since_timestamp = now.signed_duration_since(timestamp);
            format_chrono_duration(duration_since_timestamp)
        })
        .unwrap_or_else(|| "".to_string());

    match (check_in_status, controller_status) {
        (_, ControllerStatus::Offline) => print_bullet_point(&format!("{}{} {}", connection, "Offline".yellow(), last_seen)),
        (CheckInStatus::Ack, ControllerStatus::Online) => print_bullet_point(&format!("{}{} {}", connection, "Online".green(), last_seen)),
        (CheckInStatus::Nack, _) => print_bullet_point(&format!("{}{} {}", connection, "Not checked in".yellow(), last_seen)),
        (CheckInStatus::Unknown, _) => print_bullet_point(&format!("{}{} {}", connection, "Unknown".green(), last_seen)),
        (CheckInStatus::Ack, ControllerStatus::Unknown) => print_bullet_point(&format!("{}{}", connection, "Checked in, waiting for 1st hearbeat".yellow())),
    }
}

fn format_chrono_duration(duration: chrono::Duration) -> String {
    let parts = [
        (duration.num_days(), "day"),
        (duration.num_hours() % 24, "hour"),
        (duration.num_minutes() % 60, "minute"),
        (duration.num_seconds() % 60, "second"),
    ]
        .iter()
        .filter_map(|&(value, unit)| {
            if value > 0 {
                Some(format!("{} {}{}", value, unit, if value == 1 { "" } else { "s" }))
            } else {
                None
            }
        })
        .collect::<Vec<_>>()
        .join(", ");

    if parts.is_empty() {
        "just now".to_string()
    } else {
        format!("{} ago", parts)
    }
}

// fn format_duration(duration: Duration) -> String {
//     let parts = [
//         (duration.as_secs() / 86400, "day"),
//         (duration.as_secs() % 86400 / 3600, "hour"),
//         (duration.as_secs() % 3600 / 60, "minute"),
//         (duration.as_secs() % 60, "second"),
//     ]
//         .iter()
//         .filter_map(|&(value, unit)| {
//             if value > 0 {
//                 Some(format!("{} {}{}", value, unit, if value == 1 { "" } else { "s" }))
//             } else {
//                 None
//             }
//         })
//         .collect::<Vec<_>>()
//         .join(", ");
//
//     if parts.is_empty() {
//         "just now".to_string()
//     } else {
//         format!("{} ago", parts)
//     }
// }

fn print_sync_status(sync_status: &SyncStatus) {
    print!("  * Status: ");
    match sync_status {
        SyncStatus::Sync => PrintLn::green("Synced"),
        SyncStatus::Syncing => PrintLn::cyan("Syncing..."),
        _ => println!("{}", sync_status),
    }
}

fn print_waiting_for_member_backend_to_be_online() -> Result<(), Box<dyn Error>> {
    clear();
    println!("Waiting for trile to be up & running in the machine...");
    Ok(())
}

fn print_something_went_wrong() -> Result<(), Box<dyn Error>> {
    clear();
    println!("Oh-oh. It was not possible to communicate with trile. Is it running?");
    Ok(())
}

fn human_readable_byte_count(bytes: i64, si: bool) -> String {
    let unit = if si { 1000 } else { 1024 };
    if bytes < unit {
        return format!("{} B", bytes);
    }
    let exp = (bytes as f64).log(unit as f64) as i32;
    let pre = (if si { "kMGTPE" } else { "KMGTPE" })
        .chars()
        .nth((exp - 1) as usize)
        .unwrap();
    let format = if si { "" } else { "i" };
    format!("{:.1} {}{}B", bytes as f64 / unit.pow(exp as u32) as f64, pre, format)
}

fn print_bullet_point(message: &str) {
    println!("  * {}", message);
}
