use crate::dtos::trile_cli::defaults;
use crate::dtos::trile_cli::environment_variables::TrileFederationEnvVars;
use crate::services::env_vars_file_loader::env_vars_file_loader;
use std::path::Path;
use walkdir::WalkDir;

pub fn federation_member_nickname_lister(federation_name: &str) -> Vec<String> {
    let user_home_absolute_path = expand_home(&defaults::user_home_absolute_path());
    let configuration_path = format!("{}/.config/trile", user_home_absolute_path);
    let members_path = format!("{}/federations/{}/members", configuration_path, federation_name);

    match list_directories(Path::new(&members_path)) {
        Ok(directories) => directories,
        Err(_) => vec![],
    }
}

fn list_directories(path: &Path) -> Result<Vec<String>, std::io::Error> {
    let mut directories = vec![];

    for entry in WalkDir::new(path).min_depth(1).max_depth(1).into_iter().filter_map(|e| e.ok()) {
        if entry.file_type().is_dir() {
            if let Some(file_name) = entry.path().file_name() {
                directories.push(file_name.to_string_lossy().to_string());
            }
        }
    }

    Ok(directories)
}

pub fn federation_member_loader(federation_name: &str, nickname: &str) -> TrileFederationEnvVars {
    let user_home_absolute_path = expand_home(&defaults::user_home_absolute_path());
    let configuration_path = format!("{}/.config/trile", user_home_absolute_path);
    let installation_path = format!("{}/federations/{}/members/{}", configuration_path, federation_name, nickname);

    env_vars_file_loader(&(installation_path + "/.installationEnvVars"))
}

fn expand_home(input_path: &str) -> String {
    if input_path.starts_with("~") {
        let user_home = std::env::var("HOME").unwrap_or_else(|_| ".".to_string());
        format!("{}{}", user_home, &input_path[1..])
    } else {
        input_path.to_string()
    }
}