use crate::dtos::http::federation_member_status::FederationMemberStatus;
use crate::dtos::trile_cli;
use crate::services::federation_member_nickname_listener::federation_member_loader;
use crate::services::shell::docker_compose_port::docker_compose_port;
use std::collections::HashMap;

pub fn get_status(federation_name: &str, nickname: &str) -> Result<FederationMemberStatus, Box<dyn std::error::Error>> {
    let uri = get_uri(federation_name, nickname)?;
    trigger_request(&uri)
}

fn trigger_request(uri: &str) -> Result<FederationMemberStatus, Box<dyn std::error::Error>> {
    let response = reqwest::blocking::get(uri)?.text()?;
    let status: FederationMemberStatus = serde_json::from_str(&response).map_err(|error| {
        println!("{}", response);
        println!("{}", error);
        error
    })?;
    Ok(status)
}

fn get_uri(federation_name: &str, nickname: &str) -> Result<String, Box<dyn std::error::Error>> {
    let federation_env_vars: HashMap<String, String> = federation_member_loader(federation_name, nickname);
    let configuration_folder = federation_env_vars.get(trile_cli::environment_variables::TFM_CONFIGURATION_FOLDER)
        .ok_or("Configuration folder not specified")?;
    let docker_compose_file = format!("{}/docker-compose.yaml", configuration_folder);
    let service = federation_env_vars.get(trile_cli::environment_variables::TFM_BACKEND_SERVICE_NAME)
        .ok_or("Backend service name not specified")?;
    let private_port: i32 = trile_cli::defaults::FEDERATION_MEMBER_BACKEND_PRIVATE_PORT;
    let backend_port = docker_compose_port(&docker_compose_file, service, private_port)?;

    Ok(format!("http://localhost:{}/api/status", backend_port))
}
