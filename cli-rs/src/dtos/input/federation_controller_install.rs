use crate::dtos::trile_cli::Environment;
use serde::Deserialize;
use std::net::IpAddr;

#[derive(Deserialize, Debug, Clone)]
pub struct FederationControllerInstallInputVariables {
    pub environment: Environment,
    pub federation_name: String,
    pub network_address: IpAddr,
    pub generate_ssl: bool,
    pub staging_ssl: String,
    pub certbot_folder_absolute_path_optional: Option<String>,
    pub ipfs_cluster_replica_factor_max: i32,
    pub shared_disk_space_gb: Option<i32>,
}
