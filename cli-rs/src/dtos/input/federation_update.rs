use serde::Deserialize;

#[derive(Deserialize, Debug, Clone)]
pub struct FederationUpdateInputVariables {
    pub federation_name: String,
}