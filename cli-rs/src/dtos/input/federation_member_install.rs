use crate::dtos::trile_cli::environment_variables::TrileFederationEnvVars;
use crate::dtos::trile_cli::Environment;
use serde::Deserialize;

#[derive(Deserialize, Debug, Clone)]
pub struct FederationMemberInstallInputVariables {
    pub environment: Environment,
    pub nickname: String,
    pub fedvars_map: TrileFederationEnvVars,
    pub shared_disk_space_gb: i32,
    pub sharing_folder: String,
}