use crate::dtos::input::federation_member_update::FederationMemberUpdateInputVariables;
use crate::program::opts::{trile_opts_federation, trile_opts_federation_member};
use crate::services::command::update::member_command_handler::update_member_command_handler;
use crate::services::shell::confirm_and_run::confirm_and_run;
use crate::services::shell::greeter::greeter;
use clap::{ArgMatches, Command};

pub fn update_federation_member_command() -> Command {
    Command::new("member")
        .about("Updates a member installation.")
        .arg(trile_opts_federation::federation_name_opt())
        .arg(trile_opts_federation::assume_yes_flag())
        .arg(trile_opts_federation_member::nickname_opt())
}

pub fn handle_update_federation_member_command(matches: &ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
    let federation_name: String = matches.get_one::<String>("federation-name").unwrap().to_string();
    let nickname: String = matches.get_one::<String>("nickname").unwrap().to_string();
    let assume_yes: bool = matches.contains_id("assume-yes");

    greeter("update a member");

    let input_variables = FederationMemberUpdateInputVariables {
        federation_name: federation_name.clone(),
        nickname: nickname.clone(),
    };
    print_input_variables(&input_variables);

    confirm_and_run(assume_yes, || { let _ = trigger_update(input_variables); });
    Ok(())
}

fn trigger_update(input_variables: FederationMemberUpdateInputVariables) -> Result<(), Box<dyn std::error::Error>> {
    update_member_command_handler(input_variables)?;
    Ok(())
}

fn print_input_variables(input_variables: &FederationMemberUpdateInputVariables) {
    println!();
    println!("These are your input values:");
    println!("- Federation name: {}", input_variables.federation_name);
    println!("- Nickname: {}", input_variables.nickname);
    println!();
}