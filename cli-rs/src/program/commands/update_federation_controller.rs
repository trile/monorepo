use crate::dtos::input::federation_controller_update::FederationControllerUpdateInputVariables;
use crate::program::opts::trile_opts_federation;
use crate::services::command::update::controller_command_handler::update_controller_command_handler;
use crate::services::shell::confirm_and_run::confirm_and_run;
use crate::services::shell::greeter::greeter;
use clap::{ArgMatches, Command};

pub fn update_federation_controller_command() -> Command {
    Command::new("controller")
        .about("Updates a controller installation.")
        .arg(trile_opts_federation::federation_name_opt())
        .arg(trile_opts_federation::assume_yes_flag())
}

pub fn handle_update_federation_controller_command(matches: &ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
    let federation_name: String = matches.get_one::<String>("federation-name").unwrap().to_string();
    let assume_yes = matches.contains_id("assume-yes");

    greeter("update a controller");

    let input_variables = FederationControllerUpdateInputVariables {
        federation_name: federation_name.clone(),
    };
    print_input_variables(&input_variables);

    Ok(confirm_and_run(assume_yes, || {
        let _ = trigger_update(input_variables);
    }))
}

fn trigger_update(input_variables: FederationControllerUpdateInputVariables) -> Result<(), Box<dyn std::error::Error>> {
    update_controller_command_handler(input_variables)?;
    Ok(())
}

fn print_input_variables(input_variables: &FederationControllerUpdateInputVariables) {
    println!();
    println!("These are your input values. Please review them:");
    println!("- Federation name: {}", input_variables.federation_name);
    println!();
}