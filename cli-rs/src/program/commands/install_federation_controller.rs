use crate::dtos::input::federation_controller_install::FederationControllerInstallInputVariables;
use crate::dtos::trile_cli::Environment;
use crate::program::opts::{trile_opts_federation, trile_opts_federation_controller};
use crate::services::command::install::controller_command_handler::install_controller_command_handler;
use crate::services::shell::confirm_and_run::confirm_and_run;
use crate::services::shell::greeter::greeter;
use clap::Command;
use std::net::IpAddr;
use crate::services::shell::println::PrintLn;

pub fn install_federation_controller_command() -> Command {
    Command::new("controller")
        .about("Run the installation for a controller.")
        .arg(trile_opts_federation::environment_opt())
        .arg(trile_opts_federation::federation_name_opt())
        .arg(trile_opts_federation::assume_yes_flag())
        .arg(trile_opts_federation_controller::network_address_opt())
        .arg(trile_opts_federation_controller::ssl_staging_flag())
        .arg(trile_opts_federation_controller::certbot_folder_absolute_path())
        .arg(trile_opts_federation_controller::ipfs_cluster_replica_factor_max_opt())
        .arg(trile_opts_federation_controller::shared_disk_space_gb_opt())
        .after_help("Run the installation for a Federation Controller.")
}

pub fn handle_install_federation_controller_command(matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
    let environment: Environment = matches.get_one::<Environment>("environment").unwrap().clone();
    let federation_name: String = matches.get_one::<String>("federation-name").unwrap().to_string();
    let network_address = matches.get_one::<IpAddr>("network-address").unwrap();
        // .unwrap_or(IpAddr::V4("127.0.0.1".parse().unwrap()));
    let assume_yes: bool = matches.contains_id("assume-yes");
    let ssl_staging: bool = matches.contains_id("ssl-staging");
    let certbot_folder_absolute_path: Option<String> = matches.get_one::<String>("certbot-folder").map(|s| s.to_string());
    let ipfs_cluster_replica_factor_max: i32 = *matches.get_one::<i32>("ipfs-cluster-replica-factor-max").unwrap();
    let shared_disk_space_gb: i32 = *matches.get_one::<i32>("shared-disk-space-gb").unwrap();

    greeter(&format!("create the '{}' federation", &federation_name));

    let input_variables = FederationControllerInstallInputVariables {
        environment: environment.clone(),
        federation_name,
        network_address: *network_address,
        generate_ssl: matches!(environment.clone(), Environment::Production) && certbot_folder_absolute_path.is_none(),
        staging_ssl: if ssl_staging { "1".to_string() } else { "0".to_string() },
        certbot_folder_absolute_path_optional: certbot_folder_absolute_path,
        ipfs_cluster_replica_factor_max,
        shared_disk_space_gb: Some(shared_disk_space_gb),
    };

    print_input_variables(&input_variables);

    Ok(confirm_and_run(assume_yes, || {
        let _ = install_controller_command_handler(input_variables);
    }))
        .inspect(|_| {
            println!();
            PrintLn::sub_step_ok("Member installation", true)
        })
        .inspect_err(|_| PrintLn::sub_step_error("Member installation", true))
}

fn print_input_variables(input_variables: &FederationControllerInstallInputVariables) {
    println!();
    println!("These are your input values. Please review them:");
    println!("- Environment: {:?}", input_variables.environment);
    println!("- Federation name: {}", input_variables.federation_name);
    println!("- Network address: {}", input_variables.network_address);
    println!("- Ipfs Cluster replica factor maximum: {}", input_variables.ipfs_cluster_replica_factor_max);
    if let Some(ref certbot_path) = input_variables.certbot_folder_absolute_path_optional {
        println!("- Certbot folder absolute path: {}", certbot_path);
    }
    println!("- Shared disk space (GB): {:?}", input_variables.shared_disk_space_gb);
    println!();
}