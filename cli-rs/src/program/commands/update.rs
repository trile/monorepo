use crate::program::commands::update_federation::{handle_update_federation_command, update_federation_command};
use crate::program::commands::update_federation_controller::{handle_update_federation_controller_command, update_federation_controller_command};
use crate::program::commands::update_federation_member::{handle_update_federation_member_command, update_federation_member_command};
use clap::{ArgMatches, Command};

pub fn update_command() -> Command {
    Command::new("update")
        .about("Updates a pre-existing installation")
        .subcommand(update_federation_controller_command())
        .subcommand(update_federation_member_command())
        .subcommand(update_federation_command())
}

pub fn handle_update_command(matches: &ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
    match matches.subcommand() {
        Some(("controller", sub_matches)) => {
            handle_update_federation_controller_command(sub_matches)?;
        },
        Some(("member", sub_matches)) => {
            handle_update_federation_member_command(sub_matches)?;
        },
        Some(("federation", sub_matches)) => {
            handle_update_federation_command(sub_matches)?;
        },
        _ => {}
    }
    Ok(())
}