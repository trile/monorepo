use clap::builder::OsStr;
use clap::Arg;
use crate::dtos::trile_cli::{defaults, Environment};

pub fn environment_opt() -> Arg {
    Arg::new("environment")
        .long("environment")
        .help("Generate for testing or real-world usage purposes (default=production)")
        .value_parser(clap::value_parser!(Environment))
        .default_value(OsStr::from(defaults::ENVIRONMENT))
}

pub fn federation_name_opt() -> Arg {
    Arg::new("federation-name")
        .long("federation-name")
        .short('f')
        .required(true)
        .help("The name you want to use in the federation")
}

pub fn assume_yes_flag() -> Arg {
    Arg::new("assume-yes")
        .long("assume-yes")
        .short('y')
        .help("If set, installation proceeds without confirmation")
        .action(clap::ArgAction::SetTrue)
}

pub fn watch_flag() -> Arg {
    Arg::new("watch")
        .long("watch")
        .short('w')
        .help("If set, runs the action every second")
        .num_args(0)
}