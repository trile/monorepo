#!/usr/bin/env bash

set -o nounset \
    -o errexit \
    -o verbose \
    -o xtrace

if [ $# -ne 0 ]; then
  for var in "$@"
  do
    export "$var"
  done
fi
env | sort
id
yarn build --production
exec serve -s build -l 3000
