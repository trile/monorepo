import React, { useEffect, useRef } from 'react';

function WebSocketComponent() {
    const webSocketRef = useRef(null);

    useEffect(() => {
        // Define the WebSocket URL
        const webSocketUrl = 'ws://localhost:8076/websocket';

        // Create a new WebSocket instance
        webSocketRef.current = new WebSocket(webSocketUrl);

        // Event handler for WebSocket connection opened
        webSocketRef.current.onopen = () => {
            console.log('WebSocket connection opened');
            sendMessage("Pong")
        };

        // Event handler for WebSocket connection closed
        webSocketRef.current.onclose = () => {
            console.log('WebSocket connection closed');
        };

        // Event handler for receiving messages from the WebSocket server
        webSocketRef.current.onmessage = (event) => {
            const message = event.data;
            console.log('Received message from server:', message);
            // Handle the received message as needed
        };

        // Event handler for WebSocket errors
        webSocketRef.current.onerror = (error) => {
            console.error('WebSocket error:', error);
        };

        // Clean up WebSocket connection when component unmounts
        return () => {
            if (webSocketRef.current) {
                webSocketRef.current.close();
            }
        };
    }, []); // Empty dependency array to run the effect only once on component mount

    // Example of sending a message to the WebSocket server
    const sendMessage = (message) => {
        if (webSocketRef.current && webSocketRef.current.readyState === WebSocket.OPEN) {
            webSocketRef.current.send(message);
            console.log('Message sent to server:', message);
        } else {
            console.error('WebSocket connection is not open');
        }
    };

    // Example usage: sending a message
    sendMessage('Hello, WebSocket server!');

    return (
        <div>
            {/* JSX content */}
        </div>
    );
}

export default WebSocketComponent;
