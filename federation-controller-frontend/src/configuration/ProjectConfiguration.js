const ProjectConfig = {
    backendUrl: process.env.REACT_APP_TRILE_BACKEND_URL || 'https://api.demo.trile.link',
    ipfsGatewayUrl: process.env.REACT_APP_TRILE_IPFS_GATEWAY_URL || 'https://ipfs.demo.trile.link',
    federationName: process.env.REACT_APP_FEDERATION_NAME || 'Dev'
};

export default ProjectConfig;
