import axios from "axios";
import React, {useEffect, useState} from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import Grid from "@mui/material/Grid";
import {ListItem, ListItemAvatar, ListItemText} from "@mui/material";
import {Avatar, CardActions} from "@mui/material";
import FingerprintIcon from '@mui/icons-material/Fingerprint';
import PushPinIcon from '@mui/icons-material/PushPin';
import FilePresentIcon from '@mui/icons-material/FilePresent';
import {useNavigate} from 'react-router-dom';
import LoadingErrorComponent from "../components/LoadingErrorComponent";
import Button from "@mui/material/Button";
import ExploreOutlinedIcon from '@mui/icons-material/ExploreOutlined';
import BackendApiUrls from "../utils/BackendApiUrls";
import LeakAddIcon from '@mui/icons-material/LeakAdd';
import LeakRemoveIcon from '@mui/icons-material/LeakRemove';

function FederationMembersView() {
    const [members, setMembers] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);
    const navigate = useNavigate();

    async function getFileTreeNodesByIpfsCid(ipfsCid) {
        return ipfsCid ?
            axios.get(BackendApiUrls.IpfsCid.nodeTree(ipfsCid))
                .then((fileTreeNodesResponse) => fileTreeNodesResponse.data) :
            Promise.resolve({'fileCount': 0});
    }

    async function countFederationMemberFiles(ipfsCid) {
        return getFileTreeNodesByIpfsCid(ipfsCid)
            .then((fileTreeNodes) => fileTreeNodes.fileCount);
    }

    const isOnline = (lastSeenTimestamp) => {
        const now = new Date();
        const lastSeen = new Date(lastSeenTimestamp);
        const fiveMinutes = 2 * 60 * 1000;
        return (now - lastSeen) < fiveMinutes;
    }

    const lastSeenHumanReadable = (lastSeenTimestamp) => {
        const moment = require('moment'); // If you're using modules
        const lastSeen = moment(lastSeenTimestamp);
        const now = moment();
        const duration = moment.duration(now.diff(lastSeen));
        
        const parts = [
          { value: duration.days(), unit: 'day' },
          { value: duration.hours(), unit: 'hour' },
          { value: duration.minutes(), unit: 'minute' },
          { value: duration.seconds(), unit: 'second' },
        ].filter(part => part.value > 0)
          .map(({ value, unit }) => `${value} ${unit}${value === 1 ? '' : 's'}`);
        
        return parts.length === 0 ? 'just now' : parts.join(', ') + ' ago';
    }

    useEffect(() => {
        const fetchFederationMembersInformation = async () => {
            setIsLoading(true);
            setError(null);

            try {
                const response = await axios.get(BackendApiUrls.Federation.Members());
                for (const member of response.data) {
                    if (member.sharedFolderCid) {
                        member.fileCount = await countFederationMemberFiles(member.sharedFolderCid);
                    } else {
                        member.fileCount = '-';
                    }
                }
                setMembers(response.data);
            } catch (error) {
                console.error("Error fetching data:", error);
                setError(error);
            } finally {
                setIsLoading(false);
            }
        };

        fetchFederationMembersInformation();
    }, []);

    return (
        <div>
            <LoadingErrorComponent isLoading={isLoading} error={error}/>
            {members.length > 0 && (
                <Grid container spacing={2}>
                    {members.map((member) => (
                        <Grid item xs={12} sm={12} md={12} lg={6} xl={6}>
                            <Card spacing={2}>
                                <CardContent>
                                    <Typography variant="h5" gutterBottom>
                                        {member.nickname ? member.nickname : member.ipfsClusterNodeName}
                                    </Typography>
                                    <Divider/>
                                    <Typography variant="body1">
                                        <ListItem alignItems="flex-start">
                                            <ListItemAvatar>
                                                <Avatar sx={{ bgcolor: 'transparent' }}>
                                                    <FingerprintIcon style={{ color: 'black' }} />
                                                </Avatar>
                                            </ListItemAvatar>
                                            <ListItemText
                                                primary="P2P peer id"
                                                secondary={member.p2pPeerId}
                                            />
                                        </ListItem>
                                        <ListItem alignItems="flex-start">
                                            <ListItemAvatar>
                                                <Avatar sx={{ bgcolor: 'transparent' }}>
                                                    <PushPinIcon style={{ color: 'black' }} />
                                                </Avatar>
                                            </ListItemAvatar>
                                            <ListItemText
                                                primary="Shared folder CID"
                                                secondary={member.sharedFolderCid || '-'}
                                            />
                                        </ListItem>
                                        <ListItem alignItems="flex-start">
                                            <ListItemAvatar>
                                                <Avatar sx={{ bgcolor: 'transparent' }}>
                                                    <FilePresentIcon style={{ color: 'black' }} />
                                                </Avatar>
                                            </ListItemAvatar>
                                            <ListItemText
                                                primary="Files"
                                                secondary={member.fileCount}
                                            />
                                        </ListItem>
                                        <ListItem alignItems="flex-start">
                                            <ListItemAvatar>
                                               {(!isOnline(member.lastSeen)) && (
                                                <Avatar sx={{ bgcolor: 'transparent' }}>
                                                    <LeakRemoveIcon style={{ color: 'orange' }} />
                                                  </Avatar>
                                               )}
                                               {(isOnline(member.lastSeen)) && (
                                                <Avatar sx={{ bgcolor: 'transparent' }}>
                                                    <LeakAddIcon style={{ color: 'green' }} />
                                                  </Avatar>
                                               )}

                                            </ListItemAvatar>
                                            <ListItemText
                                                primary="Status"
                                                secondary={(isOnline(member.lastSeen)) ? 'Online' : ('Offline (last seen ' + lastSeenHumanReadable(member.lastSeen) + ')') }
                                            />
                                        </ListItem>
                                    </Typography>
                                </CardContent>
                                <CardActions style={{justifyContent: 'flex-end'}}>
                                    {member && member.sharedFolderCid && (
                                        <Button
                                            variant="outlined"
                                            color="primary"
                                            startIcon={<ExploreOutlinedIcon/>}
                                            onClick={(event) => {
                                                event.preventDefault();
                                                navigate(`/ipfs/cid/meta/${member.sharedFolderCid}`)
                                            }}
                                            href={`/ipfs/cid/meta/${member.sharedFolderCid}`}
                                        >
                                            Browse files
                                        </Button>
                                    )}
                                </CardActions>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
            )}
        </div>
    );
}

export default FederationMembersView;
