import sbt.*

// Dependencies
// Alphabetical
object Dependencies {
  val circeFs2          = "io.circe"                      %% "circe-fs2"           % DependencyVersions.circeFs2
  val http4sEmber       = "org.http4s"                    %% "http4s-ember-server" % DependencyVersions.http4s
  val http4sDsl         = "org.http4s"                    %% "http4s-dsl"          % DependencyVersions.http4s
  val http4sCirce       = "org.http4s"                    %% "http4s-circe"        % DependencyVersions.http4s
  val typeSafeConfig    = "com.typesafe"                   % "config"              % DependencyVersions.typeSafeConfig
  val logback           = "ch.qos.logback"                 % "logback-classic"     % DependencyVersions.logback
  val redis4CatsLogs    = "dev.profunktor"                %% "redis4cats-log4cats" % DependencyVersions.redis4Cats
  val redis4CatsStreams = "dev.profunktor"                %% "redis4cats-streams"  % DependencyVersions.redis4Cats
  val redis4CatsEffects = "dev.profunktor"                %% "redis4cats-effects"  % DependencyVersions.redis4Cats
  val sttp              = "com.softwaremill.sttp.client4" %% "core"                % DependencyVersions.sttp
  val sttpFs2Backend      = "com.softwaremill.sttp.client4" %% "async-http-client-backend-fs2" % DependencyVersions.sttp
  val trileBackendCommons = "acab.devcon0"                  %% "trile-backend-commons"         % "0.1.0-SNAPSHOT"
  val typeSafeScalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % DependencyVersions.typeSafeScalaLogging
}
