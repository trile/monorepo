package acab.devcon0.output.publisher

import acab.devcon0.domain.codecs.FederationCodecs
import acab.devcon0.domain.codecs.FederationMemberCodecs
import acab.devcon0.domain.codecs.FederationMemberCodecs.Encoders.RedisSharingFolderUpdateMessage
import acab.devcon0.domain.dtos.pubsub.Redis.FederationIpfsCidsDeltaUpdateMessage
import acab.devcon0.domain.dtos.pubsub.Redis.FederationMemberChangelogUpdateMessage
import acab.devcon0.domain.dtos.pubsub.Redis.FederationMemberIpfsCidsDeltaUpdateMessage
import acab.devcon0.domain.dtos.pubsub.Redis.FederationMemberSharingFolderUpdateMessage
import acab.devcon0.domain.ports.output.publisher.RedisPublisher
import acab.devcon0.output.repository.redisutils.Redis
import cats.effect.IO
import cats.effect.kernel.Resource
import dev.profunktor.redis4cats.connection.RedisClient
import dev.profunktor.redis4cats.data.RedisChannel
import dev.profunktor.redis4cats.data.RedisCodec
import dev.profunktor.redis4cats.log4cats._
import dev.profunktor.redis4cats.pubsub.PubSub
import fs2.Stream
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class RedisPublisherImpl(redisClient: Resource[IO, RedisClient]) extends RedisPublisher[IO] {
  private implicit val logger: Logger[IO]             = Slf4jLogger.getLogger[IO]
  private val stringCodec: RedisCodec[String, String] = RedisCodec.Utf8

  override def publish(message: FederationMemberSharingFolderUpdateMessage): IO[Unit] = {
    for
      messageStr <- FederationMemberCodecs.Encoders.RedisSharingFolderUpdateMessage(message)
      _          <- publishInner(messageStr, Redis.Channels.federationMemberSharingFolderUpdate)
    yield ()
  }

  override def publish(message: FederationMemberChangelogUpdateMessage): IO[Unit] = {
    FederationMemberCodecs.Encoders
      .ChangelogUpdateMessage(message)
      .flatMap(publishInner(_, Redis.Channels.federationMemberChangelogUpdate))
  }

  override def publish(message: FederationMemberIpfsCidsDeltaUpdateMessage): IO[Unit] = {
    FederationMemberCodecs.Encoders
      .IpfsCidsDeltaUpdateMessage(message)
      .flatMap(publishInner(_, Redis.Channels.federationMemberIpfsCidsDeltaUpdate))
  }

  override def publish(message: FederationIpfsCidsDeltaUpdateMessage): IO[Unit] = {
    FederationCodecs.Encoders
      .IpfsCidsDeltaUpdateMessage(message)
      .flatMap(publishInner(_, Redis.Channels.federationIpfsCidsDeltaUpdate))
  }

  private def publishInner(event: String, redisChannel: RedisChannel[String]): IO[Unit] = {
    {
      for
        client <- Stream.resource(redisClient)
        pubSub <- Stream.resource(PubSub.mkPubSubConnection[IO, String, String](client, stringCodec))
        pub1   = pubSub.publish(redisChannel)
        stream = Stream.eval(IO(event)).through(pub1)
        _ <- stream.through({ stream => stream.evalTap(_ => IO(())) })
      yield stream
    }.compile.drain
  }
}
