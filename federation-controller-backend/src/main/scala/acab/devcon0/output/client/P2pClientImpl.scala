package acab.devcon0.output.client

import acab.devcon0.configuration.P2pConfiguration
import acab.devcon0.domain.ports.output.client.P2pClient
import acab.devcon0.inputoutput.P2pBackend
import acab.devcon0.trile.domain.codecs.P2pCodecs
import acab.devcon0.trile.domain.codecs.P2pCodecs.Encoders._
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId
import acab.devcon0.trile.domain.dtos.pubsub.P2p
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message._
import acab.devcon0.trile.domain.dtos.pubsub.P2p._
import cats.effect._

class P2pClientImpl(p2pBackend: P2pBackend[IO], p2pConfiguration: P2pConfiguration) extends P2pClient[IO] {

  private val p2pPeerId: String = p2pBackend.getHostPeerId.toBase58

  override def publish(data: FederationControllerHeartbeat): IO[Unit] = {
    val metadata: Metadata = Metadata(MessageType.Event, p2pPeerId, None)
    val topic              = p2pConfiguration.topics.heartbeat
    p2pBackend.publishMessage(data, topic, metadata)
  }

  override def publish(data: FederationMemberCheckInAck, to: P2pPeerId): IO[Unit] = {
    val metadata: Metadata = Metadata(MessageType.Ack, p2pPeerId, Some(to))
    val topic              = p2pConfiguration.topics.checkIn
    p2pBackend.publishMessage(data, topic, metadata)
  }

  override def publish(data: FederationMemberCheckInNack, to: P2pPeerId): IO[Unit] = {
    val metadata: Metadata = Metadata(MessageType.Nack, p2pPeerId, Some(to))
    val topic              = p2pConfiguration.topics.checkIn
    p2pBackend.publishMessage(data, topic, metadata)
  }

  override def publish(data: FederationMemberSyncAck, to: P2pPeerId): IO[Unit] = {
    val metadata: Metadata = Metadata(MessageType.Ack, p2pPeerId, Some(to))
    val topic              = p2pConfiguration.topics.sync
    p2pBackend.publishMessage(data, topic, metadata)
  }

  override def publish(data: FederationMemberSyncNack, to: P2pPeerId): IO[Unit] = {
    val metadata: Metadata = Metadata(MessageType.Nack, p2pPeerId, Some(to))
    val topic              = p2pConfiguration.topics.sync
    p2pBackend.publishMessage(data, topic, metadata)
  }
}
