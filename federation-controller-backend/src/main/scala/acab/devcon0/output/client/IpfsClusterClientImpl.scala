package acab.devcon0.output.client

import scala.concurrent.duration.DurationInt

import acab.devcon0.domain.ports.output.client.IpfsClusterClient
import acab.devcon0.trile.domain.codecs.IpfsClusterCodecs
import acab.devcon0.trile.domain.dtos._
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import acab.devcon0.trile.utils.EffectsUtils
import cats.effect.IO
import cats.effect.Resource
import cats.implicits.catsSyntaxMonadError
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger
import sttp.client4._
import sttp.model.Uri

class IpfsClusterClientImpl(
    apiUrl: String,
    backendResource: Resource[IO, SyncBackend]
) extends IpfsClusterClient[IO] {

  private implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def getPeers: IO[IpfsClusterPeers] = {
    val uri: Uri = uri"$apiUrl/peers"
    backendResource
      .use(backend => { IO.blocking(basicRequest.get(uri = uri).send(backend)) })
      .attemptTap(EffectsUtils.attemptTLog)
      .flatMap(response => IpfsClusterCodecs.Decoders.IpfsClusterPeers(response.body.getOrElse("")))
  }

  override def getPeer: IO[IpfsClusterPeer] = {
    val uri: Uri = uri"$apiUrl/id"
    backendResource
      .use(backend => { IO.blocking(basicRequest.get(uri = uri).send(backend)) })
      .attemptTap(EffectsUtils.attemptTLog)
      .flatMap(response => IpfsClusterCodecs.Decoders.IpfsClusterPeer(response.body.getOrElse("")))
  }

  override def getPinAllocations(ipfsCid: IpfsCid): IO[IpfsClusterPinAllocation] = {
    val uri: Uri = uri"$apiUrl/allocations/$ipfsCid"
    backendResource
      .use(backend => { IO.blocking(basicRequest.get(uri = uri).readTimeout(1.minutes).send(backend)) })
      .attemptTap(EffectsUtils.attemptTLog)
      .flatMap(response => IpfsClusterCodecs.Decoders.IpfsClusterPinAllocation(response.body.getOrElse("")))
  }

  override def addPin(ipfsCid: IpfsCid): IO[Unit] = {
    val uri: Uri = uri"$apiUrl/pins/$ipfsCid"
    backendResource
      .use(backend => { IO.blocking(basicRequest.post(uri = uri).readTimeout(1.minutes).send(backend)) })
      .attemptTap(EffectsUtils.attemptTLog)
      .void
  }

  override def removePin(ipfsCid: IpfsCid): IO[Unit] = {
    val uri: Uri = uri"$apiUrl/pins/$ipfsCid"
    backendResource
      .use(backend => { IO.blocking(basicRequest.delete(uri = uri).readTimeout(1.minutes).send(backend)) })
      .attemptTap(EffectsUtils.attemptTLog)
      .void
  }
}
