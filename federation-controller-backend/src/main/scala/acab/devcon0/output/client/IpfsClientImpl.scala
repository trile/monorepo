package acab.devcon0.output.client

import acab.devcon0.configuration.IpfsConfiguration
import acab.devcon0.domain.codecs.IpfsCodecs
import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.output.client.IpfsClient
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import acab.devcon0.trile.utils.EffectsUtils
import cats.effect.IO
import cats.effect.Resource
import cats.implicits.catsSyntaxMonadError
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger
import sttp.client4._
import sttp.model.Uri

class IpfsClientImpl(backendResource: Resource[IO, SyncBackend], ipfsConfiguration: IpfsConfiguration)
    extends IpfsClient {

  private implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]
  private val apiUrl: String              = ipfsConfiguration.apiUrl

  override def ls(ipfsCID: IpfsCid): IO[IpfsLsResponse] = {
    val uri: Uri = uri"$apiUrl/ls?arg=$ipfsCID"
    backendResource
      .use(backend => { IO.blocking(basicRequest.post(uri = uri).send(backend)) })
      .attemptTap(EffectsUtils.attemptTLog)
      .flatMap(response => IpfsCodecs.Decoders.LsResponse(response.body.getOrElse("")))
  }
}
