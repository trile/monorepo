package acab.devcon0.output.repository.federationmember

import scala.collection.immutable.List

import acab.devcon0.domain.ports.output.repository.federationmember.CidsRepository
import acab.devcon0.output.repository.redisutils
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import cats.effect.IO
import cats.effect.kernel.Resource
import cats.implicits.toTraverseOps
import dev.profunktor.redis4cats._
import dev.profunktor.redis4cats.tx.TxStore
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

object CidsRepository {

  object Index {
    private def memberPrefix(id: FederationMemberId): String = {
      String.join(":", redisutils.Redis.SetPrefixes.federationMemberCidsIndex, id, "*")
    }

    private def key(id: FederationMemberId, ipfsCid: IpfsCid): String = {
      String.join(":", redisutils.Redis.SetPrefixes.federationMemberCidsIndex, id, ipfsCid)
    }

    object Operations {
      def getAll(
          id: FederationMemberId,
          rootIpfsCid: IpfsCid
      ): RedisCommands[IO, String, String] => IO[Set[String]] = { redis =>
        redis.sMembers(key(id, rootIpfsCid))
      }

      def getDeleted(
          id: FederationMemberId,
          oldIpfsCid: IpfsCid,
          newIpfsCid: IpfsCid
      ): RedisCommands[IO, String, String] => IO[Set[String]] = { redis =>
        val oldKey = key(id, oldIpfsCid)
        val newKey = key(id, newIpfsCid)
        redis.sDiff(oldKey, newKey)
      }

      def getAdded(
          id: FederationMemberId,
          oldIpfsCid: IpfsCid,
          newIpfsCid: IpfsCid
      ): RedisCommands[IO, String, String] => IO[Set[String]] = { redis =>
        val oldKey = key(id, oldIpfsCid)
        val newKey = key(id, newIpfsCid)
        redis.sDiff(newKey, oldKey)
      }

      def add(
          id: FederationMemberId,
          rootIpfsCid: IpfsCid,
          ipfsCids: Set[IpfsCid]
      ): RedisCommands[IO, String, String] => IO[Unit] = { redis =>
        redis.sAdd(key(id, rootIpfsCid), ipfsCids.toSeq*).void
      }

      def remove(id: FederationMemberId): RedisCommands[IO, String, String] => IO[Unit] = { redis =>
        val prefix: String = memberPrefix(id)
        for
          keys <- redis.keys(prefix)
          delCommands = keys.map(key => redis.del(key).void)
          _ <- delCommands.sequence
        yield ()
      }

      def exists(
          id: FederationMemberId,
          rootIpfsCid: IpfsCid,
          ipfsCids: Set[IpfsCid]
      ): RedisCommands[IO, String, String] => IO[List[(IpfsCid, Boolean)]] = { redis =>
        val redisKey = key(id, rootIpfsCid)
        existsByKey(ipfsCids, redis, redisKey)
      }

      private def existsByKey(
          ipfsCids: Set[IpfsCid],
          redis: RedisCommands[IO, String, String],
          redisKey: String
      ): IO[List[(IpfsCid, Boolean)]] = {
        for
          existsKey <- redis.exists(redisKey)
          sMisMember <-
            if existsKey && ipfsCids.nonEmpty then redis.sMisMember(redisKey, ipfsCids.toSeq*) else IO(List())
          zipIt <- if sMisMember.isEmpty then IO(List()) else IO(ipfsCids.toList.zip(sMisMember))
        yield zipIt
      }
    }
  }
}

class CidsRepositoryImpl(val commandsApi: Resource[IO, RedisCommands[IO, String, String]]) extends CidsRepository[IO] {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def getAll(id: FederationMemberId, rootIpfsCid: IpfsCid): IO[Set[IpfsCid]] = {
    commandsApi
      .use(CidsRepository.Index.Operations.getAll(id, rootIpfsCid)(_))
  }

  override def getDeleted(id: FederationMemberId, oldIpfsCid: IpfsCid, newIpfsCid: IpfsCid): IO[Set[IpfsCid]] = {
    commandsApi
      .use(CidsRepository.Index.Operations.getDeleted(id, oldIpfsCid, newIpfsCid)(_))
  }

  override def getAdded(id: FederationMemberId, oldIpfsCid: IpfsCid, newIpfsCid: IpfsCid): IO[Set[IpfsCid]] = {
    commandsApi
      .use(CidsRepository.Index.Operations.getAdded(id, oldIpfsCid, newIpfsCid)(_))
  }

  override def add(id: FederationMemberId, rootIpfsCid: IpfsCid, ipfsCids: Set[IpfsCid]): IO[Set[IpfsCid]] = {
    commandsApi
      .use(redis => {
        val operations: TxStore[IO, String, String] => List[IO[Unit]] = { _ =>
          List(CidsRepository.Index.Operations.add(id, rootIpfsCid, ipfsCids)(redis))
        }
        redis.transact(operations).void.onError(logError)
      })
      .map(_ => ipfsCids)
  }

  override def exists(
      id: FederationMemberId,
      rootIpfsCid: IpfsCid,
      ipfsCids: Set[IpfsCid]
  ): IO[List[(IpfsCid, Boolean)]] = {
    commandsApi
      .use(CidsRepository.Index.Operations.exists(id, rootIpfsCid, ipfsCids)(_))
  }

  private def logError(throwable: Throwable): IO[Unit] = {
    logger.error(s"throwable=$throwable")
  }
}
