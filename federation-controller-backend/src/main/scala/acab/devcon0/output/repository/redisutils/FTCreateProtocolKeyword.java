package acab.devcon0.output.repository.redisutils;


import io.lettuce.core.protocol.ProtocolKeyword;

import java.nio.charset.StandardCharsets;

public enum FTCreateProtocolKeyword implements ProtocolKeyword {
    FT_CREATE;

    public final byte[] bytes;

    FTCreateProtocolKeyword() {
        bytes = "FT.CREATE".getBytes(StandardCharsets.US_ASCII);
    }

    @Override
    public byte[] getBytes() {
        return bytes;
    }
}
