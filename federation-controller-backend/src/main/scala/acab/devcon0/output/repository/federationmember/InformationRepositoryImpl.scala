package acab.devcon0.output.repository.federationmember

import scala.collection.immutable.List

import acab.devcon0.domain.codecs.FederationMemberCodecs
import acab.devcon0.domain.codecs.FederationMemberCodecs.Decoders._
import acab.devcon0.domain.codecs.FederationMemberCodecs.Encoders._
import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.output.repository.federationmember.InformationRepository
import acab.devcon0.output.repository.redisutils
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import acab.devcon0.trile.domain.dtos.aliases.IpfsClusterPeerId
import acab.devcon0.trile.domain.dtos.aliases.IpfsPeerId
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId
import cats.effect.IO
import cats.effect.kernel.Resource
import cats.implicits._
import dev.profunktor.redis4cats._
import dev.profunktor.redis4cats.tx.TxStore
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

object InformationRepository {

  def key(id: FederationMemberId): String = {
    String.join(":", redisutils.Redis.SetPrefixes.federationMemberInformation, id)
  }

  object Operations {
    def save(
        id: FederationMemberId,
        rawJson: String
    ): RedisCommands[IO, String, String] => IO[Unit] = { redis =>
      redis.set(key(id), rawJson)
    }

    def get(id: FederationMemberId): RedisCommands[IO, String, String] => IO[Option[String]] = { redis =>
      redis.get(key(id))
    }

    def getAll: RedisCommands[IO, String, String] => IO[List[String]] = { redis =>
      val prefix: String = redisutils.Redis.SetPrefixes.federationMemberInformation ++ ":*"
      redis.keys(prefix).flatMap { keys =>
        val ioList: List[IO[Option[String]]] = keys.map(redis.get)
        val ioListWithOption: IO[List[Option[String]]] =
          ioList.foldRight(IO.pure(List.empty[Option[String]])) { (io, acc) =>
            io.flatMap {
              case Some(value) => acc.map(Some(value) :: _)
              case None        => acc
            }
          }
        ioListWithOption.map(_.flatten)
      }
    }
  }

  object Index {
    object P2pPeerId {
      def key: String = {
        String.join(
          ":",
          redisutils.Redis.SetPrefixes.federationMemberInformationIndex,
          redisutils.Redis.IndexesKeys.p2pPeerId
        )
      }

      object Operations {

        def get(ipfsPeerId: IpfsPeerId): RedisCommands[IO, String, String] => IO[Option[String]] = { redis =>
          redis.hGet(key, ipfsPeerId)
        }

        def save(
            id: FederationMemberId,
            ipfsPeerId: IpfsPeerId
        ): RedisCommands[IO, String, String] => IO[Unit] = { redis =>
          redis
            .hSet(key, ipfsPeerId, id)
            .void
        }
      }
    }
    object IpfsClusterNodeId {
      def key: String = {
        String.join(
          ":",
          redisutils.Redis.SetPrefixes.federationMemberInformationIndex,
          redisutils.Redis.IndexesKeys.ipfsClusterNodeId
        )
      }

      object Operations {

        def get(ipfsPeerId: IpfsPeerId): RedisCommands[IO, String, String] => IO[Option[String]] = { redis =>
          redis.hGet(key, ipfsPeerId)
        }

        def save(
            id: FederationMemberId,
            ipfsClusterNodeId: IpfsClusterPeerId
        ): RedisCommands[IO, String, String] => IO[Unit] = { redis =>
          redis
            .hSet(key, ipfsClusterNodeId, id)
            .void
        }
      }
    }
  }
}

class InformationRepositoryImpl(val commandsApi: Resource[IO, RedisCommands[IO, String, String]])
    extends InformationRepository[IO] {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def getByIpfsClusterNodeId(ipfsClusterNodeId: IpfsClusterPeerId): IO[Option[FederationMemberInformation]] = {
    commandsApi
      .use(redis => {
        InformationRepository.Index.IpfsClusterNodeId.Operations
          .get(ipfsClusterNodeId)(redis)
          .flatMap {
            case Some(federationMemberName) => InformationRepository.Operations.get(federationMemberName)(redis)
            case None                       => IO.pure(None)
          }
      })
      .flatMap(decode)
  }

  override def getByP2pPeerId(p2pPeerId: P2pPeerId): IO[Option[FederationMemberInformation]] = {
    commandsApi
      .use(redis => {
        InformationRepository.Index.P2pPeerId.Operations
          .get(p2pPeerId)(redis)
          .flatMap {
            case Some(federationMemberName) => InformationRepository.Operations.get(federationMemberName)(redis)
            case None                       => IO.pure(None)
          }
      })
      .flatMap(decode)
  }

  override def getById(id: FederationMemberId): IO[Option[FederationMemberInformation]] = {
    commandsApi
      .use(_.get(InformationRepository.key(id)))
      .flatMap(decode)
  }

  override def getAll: IO[List[FederationMemberInformation]] = {
    commandsApi
      .use(InformationRepository.Operations.getAll(_))
      .flatMap(decode)
  }

  override def save(federationMemberInformation: FederationMemberInformation): IO[FederationMemberInformation] = {
    FederationMemberCodecs.Encoders
      .Information(federationMemberInformation)
      .flatMap(rawJson => {
        commandsApi.use(saveInner(federationMemberInformation, rawJson, _))
      })
  }

  private def saveInner(
      federationMemberInformation: FederationMemberInformation,
      rawJson: String,
      redis: RedisCommands[IO, String, String]
  ): IO[FederationMemberInformation] = {
    val operations: TxStore[IO, String, String] => List[IO[Unit]] = { _ =>
      getOperations(federationMemberInformation, rawJson, redis)
    }
    runTransaction(redis, operations)
      .flatTap(_ => logResult(federationMemberInformation))
      .map(_ => federationMemberInformation)
  }

  private def getOperations(
      federationMemberInformation: FederationMemberInformation,
      rawJson: String,
      redis: RedisCommands[IO, String, String]
  ): List[IO[Unit]] = {
    val p2pPeerId         = federationMemberInformation.p2pPeerId
    val ipfsClusterNodeId = federationMemberInformation.ipfsClusterPeerId
    val id                = federationMemberInformation.id
    List(
      InformationRepository.Operations.save(id, rawJson)(redis),
      InformationRepository.Index.P2pPeerId.Operations.save(id, p2pPeerId)(redis),
      InformationRepository.Index.IpfsClusterNodeId.Operations.save(id, ipfsClusterNodeId)(redis)
    )
  }

  private def runTransaction(
      redis: RedisCommands[IO, String, String],
      operations: TxStore[IO, String, String] => List[IO[Unit]]
  ): IO[Unit] = {
    redis
      .transact(operations)
      .void
      .onError(logError)
  }

  private def logError(throwable: Throwable): IO[Unit] = {
    logger.error(s"throwable=$throwable")
  }

  private def logResult(federationMemberInformation: FederationMemberInformation): IO[Unit] = {
    logger.debug(s"federationMemberInformation=$federationMemberInformation")
  }

  private def decode(maybeRawJson: Option[String]): IO[Option[FederationMemberInformation]] = {
    maybeRawJson match
      case Some(rawJson) => FederationMemberCodecs.Decoders.Information(rawJson).map(Some(_))
      case None          => IO.pure(None)
  }

  private def decode(listRawJson: List[String]): IO[List[FederationMemberInformation]] = {
    listRawJson.traverse(FederationMemberCodecs.Decoders.Information(_))
  }
}
