package acab.devcon0.domain.mappers

import acab.devcon0.domain.dtos.IpfsCidDto
import acab.devcon0.domain.dtos.IpfsCidType

object IpfsCidMapper {

  object NodeTree {
    def to(ipfsCidDto: IpfsCidDto): IpfsCidDto = {
      filterDto(ipfsCidDto)
    }

    private def filterDto(ipfsCidDto: IpfsCidDto): IpfsCidDto = {
      if ipfsCidDto.contents.exists(_.`type`.equals(IpfsCidType.Folder)) then
        ipfsCidDto.copy(contents = ipfsCidDto.contents.filter(_.`type`.equals(IpfsCidType.Folder)).map(filterDto))
      else ipfsCidDto.copy(contents = List())
    }
  }

  object Flat {
    def to(ipfsCidDto: IpfsCidDto): IpfsCidDto = {
      ipfsCidDto.copy(contents = ipfsCidDto.contents.map(_.copy(contents = List())))
    }
  }

  object FlatAll {
    def to(ipfsCidDto: IpfsCidDto): Set[IpfsCidDto] = {
      val ipfsCidDtos = Set(ipfsCidDto) ++ ipfsCidDto.contents.toSet.flatMap(flattenDto)
      ipfsCidDtos.map(Flat.to)
    }

    private def flattenDto(ipfsCidDto: IpfsCidDto): Set[IpfsCidDto] = {
      if ipfsCidDto.`type`.equals(IpfsCidType.Folder) then
        Set(ipfsCidDto) ++ ipfsCidDto.contents.foldLeft[Set[IpfsCidDto]](Set())((acc, dto) => acc.++(flattenDto(dto)))
      else Set(ipfsCidDto)
    }
  }
}
