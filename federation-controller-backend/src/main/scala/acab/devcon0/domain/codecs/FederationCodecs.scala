package acab.devcon0.domain.codecs

import acab.devcon0.domain.dtos._
import acab.devcon0.domain.dtos.pubsub.Redis
import acab.devcon0.domain.dtos.pubsub.Redis._
import cats.effect.IO
import io.circe._
import io.circe.generic.auto._
import io.circe.generic.semiauto.deriveDecoder
import io.circe.generic.semiauto.deriveEncoder
import io.circe.jawn.decode
import io.circe.syntax.EncoderOps

object FederationCodecs {

  object Decoders {
    object IpfsCidsDeltaUpdateMessage {
      private implicit val codec: Decoder[FederationIpfsCidsDeltaUpdateMessage] = deriveDecoder
      def apply(rawJson: String): IO[FederationIpfsCidsDeltaUpdateMessage] = IO.fromEither {
        decode[FederationIpfsCidsDeltaUpdateMessage](rawJson)
      }
    }
  }

  object Encoders {

    object IpfsCidsDeltaUpdateMessage {
      private implicit val codec: Encoder[FederationIpfsCidsDeltaUpdateMessage] = deriveEncoder
      def apply(dto: FederationIpfsCidsDeltaUpdateMessage): IO[String] = IO {
        EncoderOps[FederationIpfsCidsDeltaUpdateMessage](dto).asJson.noSpaces
      }
    }
  }
}
