package acab.devcon0.domain.ports.output.repository.federationmember

import java.time.Instant

import acab.devcon0.domain.dtos._
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId

trait ChangelogRepository[F[_]] {
  def getNewest(id: FederationMemberId): F[Option[FederationMemberChangelogItem]]
  def getPrevious(
      id: FederationMemberId,
      timestamp: Instant
  ): F[List[FederationMemberChangelogItem]]
  def add(
      id: FederationMemberId,
      federationMemberChangelogItem: FederationMemberChangelogItem
  ): F[FederationMemberChangelogItem]
  def update(
      id: FederationMemberId,
      federationMemberChangelogItemOld: FederationMemberChangelogItem,
      federationMemberChangelogItemNew: FederationMemberChangelogItem
  ): F[FederationMemberChangelogItem]
}
