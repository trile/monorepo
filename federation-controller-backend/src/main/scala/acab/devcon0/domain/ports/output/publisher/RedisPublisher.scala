package acab.devcon0.domain.ports.output.publisher

import acab.devcon0.domain.dtos.pubsub.Redis.FederationIpfsCidsDeltaUpdateMessage
import acab.devcon0.domain.dtos.pubsub.Redis.FederationMemberChangelogUpdateMessage
import acab.devcon0.domain.dtos.pubsub.Redis.FederationMemberIpfsCidsDeltaUpdateMessage
import acab.devcon0.domain.dtos.pubsub.Redis.FederationMemberSharingFolderUpdateMessage

trait RedisPublisher[F[_]] {
  def publish(message: FederationMemberSharingFolderUpdateMessage): F[Unit]
  def publish(message: FederationMemberChangelogUpdateMessage): F[Unit]
  def publish(message: FederationMemberIpfsCidsDeltaUpdateMessage): F[Unit]
  def publish(message: FederationIpfsCidsDeltaUpdateMessage): F[Unit]
}
