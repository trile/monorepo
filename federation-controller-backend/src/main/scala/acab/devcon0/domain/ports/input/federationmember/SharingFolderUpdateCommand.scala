package acab.devcon0.domain.ports.input.federationmember

import acab.devcon0.commons.Command
import acab.devcon0.commons.CommandHandler
import acab.devcon0.commons.Event
import acab.devcon0.domain.dtos.FederationMemberChangelogItem
import acab.devcon0.domain.dtos.FederationMemberInformation
import acab.devcon0.domain.dtos.pubsub.Redis.FederationMemberSharingFolderUpdateMessage

final case class SharingFolderUpdateCommand(
    message: FederationMemberSharingFolderUpdateMessage,
    federationMemberInformation: FederationMemberInformation
) extends Command[String]

sealed abstract class SharingFolderEvent[T]                    extends Event[T]
final case class SharingFolderErrorEvent(throwable: Throwable) extends SharingFolderEvent[Unit]
final case class SharingFolderKnownUpdateEvent()       extends SharingFolderEvent[Unit]
final case class SharingFolderSuccessEvent(federationMemberChangelogItem: FederationMemberChangelogItem)
    extends SharingFolderEvent[Unit]

trait SharingFolderUpdateCommandHandler
    extends CommandHandler[SharingFolderUpdateCommand, String, SharingFolderEvent[?]]
