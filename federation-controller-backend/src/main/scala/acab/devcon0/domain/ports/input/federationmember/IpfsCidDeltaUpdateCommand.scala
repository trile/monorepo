package acab.devcon0.domain.ports.input.federationmember

import acab.devcon0.commons.Command
import acab.devcon0.commons.CommandHandler
import acab.devcon0.commons.Event
import acab.devcon0.domain.dtos.FederationMemberChangelogItem
import acab.devcon0.domain.dtos.FederationMemberIpfsCidDelta
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId

final case class IpfsCidDeltaUpdateCommand(
    id: FederationMemberId,
    federationMemberChangelogItem: FederationMemberChangelogItem
) extends Command[String]

sealed abstract class IpfsCidDeltaUpdateEvent[T] extends Event[T]
final case class IpfsCidDeltaUpdateSuccessEvent(delta: FederationMemberIpfsCidDelta)
    extends IpfsCidDeltaUpdateEvent[FederationMemberIpfsCidDelta]
final case class IpfsCidDeltaUpdateErrorEvent() extends IpfsCidDeltaUpdateEvent[FederationMemberIpfsCidDelta]

trait IpfsCidDeltaUpdateCommandHandler
    extends CommandHandler[IpfsCidDeltaUpdateCommand, String, IpfsCidDeltaUpdateEvent[?]]
