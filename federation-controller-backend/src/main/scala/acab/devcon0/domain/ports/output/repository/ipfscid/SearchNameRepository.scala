package acab.devcon0.domain.ports.output.repository.ipfscid

import acab.devcon0.domain.dtos._
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid

trait SearchNameRepository[F[_]] {
  def createIndex(): F[Unit]
  def search(searchParameters: SearchParameters): F[SearchResult[IpfsCid]]
}
