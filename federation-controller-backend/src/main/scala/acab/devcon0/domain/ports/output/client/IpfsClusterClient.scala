package acab.devcon0.domain.ports.output.client

import acab.devcon0.trile.domain.dtos._
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid

trait IpfsClusterClient[F[_]] {
  def getPeers: F[IpfsClusterPeers]
  def getPeer: F[IpfsClusterPeer]
  def getPinAllocations(ipfsCid: IpfsCid): F[IpfsClusterPinAllocation]
  def removePin(pinCID: IpfsCid): F[Unit]
  def addPin(cid: IpfsCid): F[Unit]
}
