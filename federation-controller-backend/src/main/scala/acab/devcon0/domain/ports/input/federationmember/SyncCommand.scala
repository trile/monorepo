package acab.devcon0.domain.ports.input.federationmember

import acab.devcon0.commons.Command
import acab.devcon0.commons.CommandHandler
import acab.devcon0.commons.Event
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

sealed trait SyncResponseCommand                                        extends Command[String]
final case class SyncAckCommand(federationMemberId: FederationMemberId) extends SyncResponseCommand
final case class SyncNackCommand(federationMemberId: P2pPeerId)         extends SyncResponseCommand

sealed abstract class SyncResponseEvent[T]                    extends Event[T]
final case class SyncResponseErrorEvent(throwable: Throwable) extends SyncResponseEvent[Unit]
final case class SyncResponseSuccessEvent()                   extends SyncResponseEvent[Unit]

trait SyncCommandHandler extends CommandHandler[SyncResponseCommand, String, SyncResponseEvent[?]]

object SyncCommandImplicits {
  implicit class SyncAckCommandEventFlattenOps(result: IO[SyncResponseEvent[?]]) {
    private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

    def flattenEvents: IO[Unit] = {
      result.flatMap {
        case SyncResponseSuccessEvent()        => IO.unit
        case SyncResponseErrorEvent(throwable) => handleErrorCase(throwable)
      }
    }

    private def handleErrorCase(exception: Throwable): IO[Unit] = {
      logger.error(s"RedisPubSubFederationMemberSyncAckEventProcessor exception=$exception") >>
        IO.raiseError[Unit](exception)
    }
  }
}
