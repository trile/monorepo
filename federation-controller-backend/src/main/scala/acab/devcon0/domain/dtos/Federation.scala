package acab.devcon0.domain.dtos

import acab.devcon0.trile.domain.dtos.aliases.IpfsCid

final case class FederationIpfsCidDelta(additions: Set[IpfsCid], removals: Set[IpfsCid])

final case class FederationAuth(
    visibilityType: String,
    joinToken: String,
                               )
