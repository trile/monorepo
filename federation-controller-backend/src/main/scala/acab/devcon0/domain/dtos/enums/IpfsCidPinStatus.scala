package acab.devcon0.domain.dtos.enums

enum IpfsCidPinStatus {
  case PINNED, REMOVED
}
