package acab.devcon0.domain.dtos

import java.time.Instant

import acab.devcon0.trile.domain.dtos.aliases._

final case class FederationMemberInformation(
    id: FederationMemberId,
    p2pPeerId: P2pPeerId,
    nickname: FederationMemberNickname,
    ipfsClusterPeerId: IpfsClusterPeerId,
    ipfsClusterNodeName: IpfsClusterNodeName,
    ipfsPeerId: IpfsPeerId,
    sharedFolderCid: IpfsCid,
    ipfsClusterAddresses: List[String],
    ipfsIpfsAddresses: List[String],
    lastSeen: Instant
)

final case class FederationMemberChangelogItem(ipfsCid: IpfsCid, timestamp: Instant)

final case class FederationMemberIpfsCidDelta(
    federationMemberId: FederationMemberId,
    additions: Set[IpfsCid],
    removals: Set[IpfsCid]
)
