package acab.devcon0.domain.dtos.exceptions

import sttp.model.StatusCode

final case class HttpResponseException(statusCode: StatusCode, responseBody: String, message: String) extends Throwable
