package acab.devcon0.domain.adapters.federationmember

import acab.devcon0.domain.dtos.FederationMemberInformation
import acab.devcon0.domain.ports.input._
import acab.devcon0.domain.ports.input.federationmember._
import acab.devcon0.domain.service.P2pService
import acab.devcon0.domain.service.federationmember.InformationService
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberSyncAck
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberSyncNack
import acab.devcon0.trile.utils.EffectsUtils
import acab.devcon0.trile.utils.IORetry
import cats.effect.IO
import cats.implicits.catsSyntaxMonadError
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class SyncCommandHandlerImpl(p2PService: P2pService[IO], informationService: InformationService[IO])
    extends SyncCommandHandler {

  private implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def handle(cmd: SyncResponseCommand): IO[SyncResponseEvent[?]] = {
    IORetry
      .fibonacci(for
        federationMemberInformation <- getInformation(cmd)
        _                           <- publishMessage(cmd, federationMemberInformation)
      yield SyncResponseSuccessEvent())
      .attemptTap(EffectsUtils.attemptTLog)
      .onError(throwable => IO(SyncResponseErrorEvent(throwable = throwable)))
  }

  private def publishMessage(command: SyncResponseCommand, information: FederationMemberInformation): IO[Unit] = {
    command match
      case _: SyncAckCommand  => p2PService.publish(FederationMemberSyncAck(), information.p2pPeerId)
      case _: SyncNackCommand => p2PService.publish(FederationMemberSyncNack(), information.p2pPeerId)
  }

  private def getInformation(cmd: SyncResponseCommand): IO[FederationMemberInformation] = {
    cmd match
      case SyncAckCommand(federationMemberId)  => getInformation(federationMemberId)
      case SyncNackCommand(federationMemberId) => getInformation(federationMemberId)
  }

  private def getInformation(federationMemberId: FederationMemberId): IO[FederationMemberInformation] = {
    informationService
      .getById(federationMemberId)
      .flatMap(EffectsUtils.forceOptionExistence())
  }
}
