package acab.devcon0.domain.adapters.ipfscid

import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.input.ipfscid._
import acab.devcon0.domain.service.ipfscid.FacadeService
import acab.devcon0.domain.service.ipfscid.HardCopiesService
import acab.devcon0.domain.service.ipfscid.SearchNameService
import acab.devcon0.domain.service.ipfscid.SoftCopiesService
import cats.effect.IO
import cats.implicits._

class SearchQueryHandlerImpl(
    facadeService: FacadeService[IO],
    searchNameService: SearchNameService[IO],
    hardCopiesService: HardCopiesService[IO],
    softCopiesService: SoftCopiesService[IO]
) extends SearchQueryHandler {

  override def handle(query: SearchQuery): IO[IpfsCidSearchResponse] = {
    for
      searchResultIpfsCids <- searchNameService.search(query.searchParameters)
      ipfsCidDtos          <- facadeService.getFlats(searchResultIpfsCids.result)
      result               <- ipfsCidDtos.toList.traverse(buildIpfsCidMetadataDto)
    yield IpfsCidSearchResponse(result, searchResultIpfsCids.totalCount, searchResultIpfsCids.pageSize)
  }

  private def buildIpfsCidMetadataDto(dto: IpfsCidDto): IO[IpfsCidMetadataDto] = {
    for (hardCopies, softCopies) <- (hardCopiesService.get(dto.cid), softCopiesService.get(dto.cid)).parTupled
    yield IpfsCidMetadataDto(
      cid = dto.cid,
      size = dto.size,
      originalName = dto.name,
      fileCount = dto.fileCount,
      contents = dto.contents,
      timestamp = dto.timestamp,
      `type` = dto.`type`,
      copies = IpfsCidMetadataCopiesDto(hardCopies, softCopies),
      names = List()
    )
  }
}
