package acab.devcon0.domain.adapters.federation
import scala.concurrent.duration.DurationInt

import acab.devcon0.domain.ports.input.federation.IpfsCidDeltaUpdateCommand
import acab.devcon0.domain.ports.input.federation.IpfsCidDeltaUpdateCommandHandler
import acab.devcon0.domain.ports.input.federation.IpfsCidDeltaUpdateEvent
import acab.devcon0.domain.ports.input.federation.IpfsCidDeltaUpdateSuccessEvent
import acab.devcon0.domain.service._
import acab.devcon0.domain.service.ipfscid.FacadeService
import acab.devcon0.domain.service.ipfscid.HardCopiesService
import acab.devcon0.domain.service.ipfscid.SoftCopiesService
import cats.effect.IO
import cats.effect.std.Supervisor
import cats.implicits.toTraverseOps

class IpfsCidDeltaUpdateCommandHandlerImpl(
    ipfsCidFacadeService: FacadeService[IO],
    ipfsCidHardCopiesService: HardCopiesService[IO],
    ipfsCidSoftCopiesService: SoftCopiesService[IO],
    ipfsClusterService: IpfsClusterService[IO]
) extends IpfsCidDeltaUpdateCommandHandler {

  override def handle(cmd: IpfsCidDeltaUpdateCommand): IO[IpfsCidDeltaUpdateEvent[?]] = {
    for
      _ <- handleAdditions(cmd)
      _ <- handleRemovals(cmd)
    yield IpfsCidDeltaUpdateSuccessEvent()
  }

  private def handleAdditions(cmd: IpfsCidDeltaUpdateCommand): IO[Unit] = {
    for
      _ <- ipfsCidHardCopiesService.increment(cmd.delta.additions)
      _ <- ipfsClusterService.addPins(cmd.delta.additions)
      _ <- handleAdditionsSoftCopies(cmd)
    yield ()
  }

  private def handleRemovals(cmd: IpfsCidDeltaUpdateCommand): IO[Unit] = {
    for
      _               <- ipfsCidFacadeService.deleteFlats(cmd.delta.removals)
      _               <- ipfsCidHardCopiesService.decrement(cmd.delta.removals)
      _               <- ipfsClusterService.removePins(cmd.delta.removals)
      pinsAllocations <- cmd.delta.removals.toSeq.traverse(ipfsClusterService.getPinAllocations)
      _               <- pinsAllocations.traverse(pin => ipfsCidSoftCopiesService.delete(pin.cid))
    yield ()
  }

  private def handleAdditionsSoftCopies(cmd: IpfsCidDeltaUpdateCommand): IO[Unit] = {
    Supervisor[IO](await = false).use(supervisor => {
      for
        _    <- IO.sleep(10.seconds)
        pinsAllocations <- cmd.delta.additions.toSeq.traverse(ipfsClusterService.getPinAllocations)
        _    <- pinsAllocations.traverse(pin => ipfsCidSoftCopiesService.set(pin.cid, pin.allocations.size))
      yield ()
    })
  }
}
