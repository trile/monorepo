package acab.devcon0.domain.adapters.federationmember

import acab.devcon0.domain.dtos.pubsub.Redis.FederationMemberSharingFolderUpdateMessage
import acab.devcon0.domain.ports.input._
import acab.devcon0.domain.ports.input.federationmember._
import acab.devcon0.domain.ports.output.publisher.RedisPublisher
import cats.effect.IO

class BounceSharingFolderUpdateCommandHandlerImpl(redisPubSubPublisher: RedisPublisher[IO])
    extends BounceSharingFolderUpdateCommandHandler {

  override def handle(cmd: BounceSharingFolderUpdateCommand): IO[BounceSharingFolderUpdateEvent[?]] = {
    handleInner(cmd.message)
      .map(_ => BounceSharingFolderUpdateSuccessEvent())
      .onError(throwable => IO(BounceSharingFolderUpdateErrorEvent(throwable = throwable)))
  }

  private def handleInner(message: FederationMemberSharingFolderUpdateMessage): IO[Unit] = {
    redisPubSubPublisher.publish(message)
  }
}
