package acab.devcon0.domain.service.ipfscid

import acab.devcon0.domain.ports.output.repository.ipfscid.HardCopiesRepository
import acab.devcon0.domain.service.ipfscid
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import cats.effect.IO

class HardCopiesServiceImpl(repository: HardCopiesRepository[IO]) extends HardCopiesService[IO] {

  override def get(ipfsCid: IpfsCid): IO[Int] = {
    repository.get(ipfsCid)
  }

  override def increment(ipfsCids: Set[IpfsCid]): IO[Unit] = {
    if ipfsCids.isEmpty then IO.unit
    else repository.increment(ipfsCids)
  }

  override def decrement(ipfsCids: Set[IpfsCid]): IO[Unit] = {
    if ipfsCids.isEmpty then IO.unit
    else repository.decrement(ipfsCids)
  }
}
