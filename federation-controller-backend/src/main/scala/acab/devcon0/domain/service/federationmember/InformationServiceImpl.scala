package acab.devcon0.domain.service.federationmember

import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.output.repository.federationmember.InformationRepository
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import acab.devcon0.trile.domain.dtos.aliases.IpfsClusterPeerId
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId
import cats.effect.IO

class InformationServiceImpl(
    val repository: InformationRepository[IO]
) extends InformationService[IO] {

  override def getByIpfsClusterNodeId(ipfsClusterNodeId: IpfsClusterPeerId): IO[Option[FederationMemberInformation]] = {
    repository.getByIpfsClusterNodeId(ipfsClusterNodeId)
  }

  override def getByP2pPeerId(p2pPeerId: P2pPeerId): IO[Option[FederationMemberInformation]] = {
    repository.getByP2pPeerId(p2pPeerId)
  }

  override def getById(id: FederationMemberId): IO[Option[FederationMemberInformation]] = {
    repository.getById(id)
  }

  override def getAll: IO[List[FederationMemberInformation]] = {
    repository.getAll
  }

  override def save(federationMemberInformation: FederationMemberInformation): IO[FederationMemberInformation] = {
    repository.save(federationMemberInformation)
  }
}
