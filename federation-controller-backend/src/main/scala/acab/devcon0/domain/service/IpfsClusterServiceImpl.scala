package acab.devcon0.domain.service

import acab.devcon0.domain.ports.output.client.IpfsClusterClient
import acab.devcon0.trile.domain.dtos.IpfsClusterPeer
import acab.devcon0.trile.domain.dtos.IpfsClusterPeers
import acab.devcon0.trile.domain.dtos.IpfsClusterPinAllocation
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import acab.devcon0.trile.domain.dtos.aliases.IpfsClusterPeerId
import acab.devcon0.trile.utils.EffectsUtils
import acab.devcon0.trile.utils.IORetry
import cats.effect.IO
import cats.implicits.toTraverseOps
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class IpfsClusterServiceImpl(client: IpfsClusterClient[IO]) extends IpfsClusterService[IO] {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def getPeers: IO[IpfsClusterPeers] = {
    client.getPeers
  }

  override def getPeer: IO[IpfsClusterPeer] = {
    client.getPeer
  }

  override def getPeer(ipfsClusterPeerId: IpfsClusterPeerId): IO[IpfsClusterPeer] = {
    val getPeerIO: IO[IpfsClusterPeer] = client.getPeers
      .map(peers => peers.peers)
      .map(peers => peers.find(peer => peer.id.equals(ipfsClusterPeerId)))
      .flatMap(EffectsUtils.forceOptionExistence())
    IORetry.fibonacci(getPeerIO)
  }

  override def getPinAllocations(ipfsCid: IpfsCid): IO[IpfsClusterPinAllocation] = {
    client.getPinAllocations(ipfsCid)
  }

  override def addPins(ipfsCids: Set[IpfsCid]): IO[Unit] = {
    ipfsCids.toSeq.traverse(addPin).void
  }

  override def removePins(ipfsCids: Set[IpfsCid]): IO[Unit] = {
    ipfsCids.toSeq.traverse(client.removePin).void
  }
  private def addPin(ipfsCid: IpfsCid): IO[Unit] = {
    IORetry.fibonacci(client.addPin(ipfsCid))
  }
}
