package acab.devcon0.domain.service

import acab.devcon0.trile.domain.dtos.IpfsClusterPeer
import acab.devcon0.trile.domain.dtos.IpfsClusterPeers
import acab.devcon0.trile.domain.dtos.IpfsClusterPinAllocation
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId

trait IpfsClusterService[F[_]] {
  def getPeers: F[IpfsClusterPeers]
  def getPeer(p2pPeerId: P2pPeerId): F[IpfsClusterPeer]
  def getPeer: F[IpfsClusterPeer]
  def addPins(ipfsCids: Set[IpfsCid]): F[Unit]
  def getPinAllocations(ipfsCid: IpfsCid): F[IpfsClusterPinAllocation]
  def removePins(ipfsCids: Set[IpfsCid]): F[Unit]
}
