package acab.devcon0.domain.service.ipfscid
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid

trait HardCopiesService[F[_]] {
  def get(ipfsCid: IpfsCid): F[Int]
  def increment(ipfsCids: Set[IpfsCid]): F[Unit]
  def decrement(ipfsCids: Set[IpfsCid]): F[Unit]
}
