package acab.devcon0.domain.service.federationmember

import acab.devcon0.domain.dtos._
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId

trait ChangelogService[F[_]] {
  def getNewest(id: FederationMemberId): F[Option[FederationMemberChangelogItem]]
  def getPrevious(
      id: FederationMemberId,
      federationMemberChangelogItem: FederationMemberChangelogItem
  ): F[Option[FederationMemberChangelogItem]]
  def add(
      id: FederationMemberId,
      federationMemberChangelogItem: FederationMemberChangelogItem
  ): F[FederationMemberChangelogItem]
}
