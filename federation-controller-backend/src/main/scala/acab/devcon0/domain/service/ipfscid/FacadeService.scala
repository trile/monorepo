package acab.devcon0.domain.service.ipfscid

import acab.devcon0.domain.dtos._
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid

trait FacadeService[F[_]] {
  def getFileTree(ipfsCid: IpfsCid): F[Option[IpfsCidDto]]
  def saveFileTree(ipfsCidDto: IpfsCidDto): F[IpfsCidDto]
  def getFlat(ipfsCid: IpfsCid): F[Option[IpfsCidDto]]
  def getFlats(ipfsCids: Set[IpfsCid]): F[Set[IpfsCidDto]]
  def saveFlat(ipfsCidDto: IpfsCidDto): F[IpfsCidDto]
  def saveFlats(ipfsCidDtos: Set[IpfsCidDto]): F[Set[IpfsCidDto]]
  def deleteFlats(ipfsCid: Set[IpfsCid]): F[Unit]
  def getNodeTree(ipfsCid: IpfsCid): F[Option[IpfsCidDto]]
  def saveNodeTree(ipfsCidDto: IpfsCidDto): F[IpfsCidDto]
}
