package acab.devcon0.domain.service.federationmember

import acab.devcon0.domain.dtos._
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import acab.devcon0.trile.domain.dtos.aliases.IpfsClusterPeerId
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId

trait InformationService[F[_]] {
  def getByIpfsClusterNodeId(ipfsClusterNodeId: IpfsClusterPeerId): F[Option[FederationMemberInformation]]
  def getByP2pPeerId(p2pPeerId: P2pPeerId): F[Option[FederationMemberInformation]]
  def getById(id: FederationMemberId): F[Option[FederationMemberInformation]]
  def getAll: F[List[FederationMemberInformation]]
  def save(federationMemberInformation: FederationMemberInformation): F[FederationMemberInformation]
}
