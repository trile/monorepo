package acab.devcon0.domain.service

import acab.devcon0.domain.ports.output.client.P2pClient
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message._
import cats.effect.IO

class P2pServiceImpl(p2pClient: P2pClient[IO]) extends P2pService[IO] {
  override def publish(message: FederationControllerHeartbeat): IO[Unit]              = p2pClient.publish(message)
  override def publish(message: FederationMemberCheckInAck, to: P2pPeerId): IO[Unit]  = p2pClient.publish(message, to)
  override def publish(message: FederationMemberCheckInNack, to: P2pPeerId): IO[Unit] = p2pClient.publish(message, to)
  override def publish(message: FederationMemberSyncAck, to: P2pPeerId): IO[Unit]     = p2pClient.publish(message, to)
  override def publish(message: FederationMemberSyncNack, to: P2pPeerId): IO[Unit]    = p2pClient.publish(message, to)
}
