package acab.devcon0.boot.ioc

import acab.devcon0.boot.ioc
import acab.devcon0.domain.adapters.federation.HeartbeatCommandHandlerImpl
import acab.devcon0.domain.adapters.federation.IpfsCidDeltaQueryHandlerImpl
import acab.devcon0.domain.adapters.federation.IpfsCidDeltaUpdateCommandHandlerImpl
import acab.devcon0.domain.ports.input.federation.HeartbeatCommandHandler
import acab.devcon0.domain.ports.input.federation.IpfsCidDeltaQueryHandler
import acab.devcon0.domain.ports.input.federation.IpfsCidDeltaUpdateCommandHandler
import acab.devcon0.input.bootstrap.HeartbeatAdvertiser
import acab.devcon0.input.redispubsub.FederationIpfsCidDeltaUpdateListener
import acab.devcon0.input.redispubsub.FederationMemberIpfsCidDeltaUpdateListener

object FederationIoC {

  object Input {

    val heartbeatAdvertiser: HeartbeatAdvertiser = new HeartbeatAdvertiser(
      heartbeatCommandHandler = Domain.Port.heartbeatCommandHandler
    )

    val federationMemberIpfsCidDeltaUpdateRedisListener: FederationMemberIpfsCidDeltaUpdateListener = {
      new FederationMemberIpfsCidDeltaUpdateListener(
        redisListener = CommonsIoC.Input.redisListener,
        federationIpfsCidDeltaQueryHandler = Domain.Port.ipfsCidDeltaQueryHandler,
        redisPublisher = CommonsIoC.Output.redisPubSubPublisher
      )
    }
    val ipfsCidDeltaUpdateRedisListener: FederationIpfsCidDeltaUpdateListener = {
      new FederationIpfsCidDeltaUpdateListener(
        redisListener = ioc.CommonsIoC.Input.redisListener,
        ipfsCidDeltaUpdateCommandHandler = ioc.FederationIoC.Domain.Port.ipfsCidUpdateCommandHandler
      )
    }
  }

  object Domain {

    object Port {
      val heartbeatCommandHandler: HeartbeatCommandHandler = new HeartbeatCommandHandlerImpl(
        p2PService = CommonsIoC.Domain.Service.p2pService,
        ipfsClusterService = IpfsIoC.Domain.Service.ipfsClusterService
      )

      val ipfsCidUpdateCommandHandler: IpfsCidDeltaUpdateCommandHandler = {
        new IpfsCidDeltaUpdateCommandHandlerImpl(
          ipfsCidFacadeService = IpfsIoC.Domain.Service.facadeService,
          ipfsCidHardCopiesService = IpfsIoC.Domain.Service.hardCopiesService,
          ipfsCidSoftCopiesService = IpfsIoC.Domain.Service.softCopiesService,
          ipfsClusterService = IpfsIoC.Domain.Service.ipfsClusterService
        )
      }

      val ipfsCidDeltaQueryHandler: IpfsCidDeltaQueryHandler = new IpfsCidDeltaQueryHandlerImpl(
        federationMemberCidsService = FederationMemberIoC.Domain.Service.cidsService,
        federationMemberInformationService = FederationMemberIoC.Domain.Service.informationService
      )
    }
  }
}
