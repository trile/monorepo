package acab.devcon0.boot.ioc

import acab.devcon0.boot.ioc
import acab.devcon0.domain.adapters
import acab.devcon0.domain.adapters.federationmember._
import acab.devcon0.domain.ports.input.federationmember._
import acab.devcon0.domain.ports.input.{federationmembers => federationmembersports}
import acab.devcon0.domain.ports.output.repository.federationmember.ChangelogRepository
import acab.devcon0.domain.ports.output.repository.federationmember.CidsRepository
import acab.devcon0.domain.ports.output.repository.federationmember.InformationRepository
import acab.devcon0.domain.service.federationmember._
import acab.devcon0.input.p2ppubsub
import acab.devcon0.input.p2ppubsub.FederationMemberCheckInListener
import acab.devcon0.input.p2ppubsub.FederationMemberHeartbeatListener
import acab.devcon0.input.p2ppubsub.FederationMemberSyncListener
import acab.devcon0.input.redispubsub
import acab.devcon0.input.redispubsub.FederationMemberChangelogUpdateEventListener
import acab.devcon0.output.repository.federationmember.ChangelogRepositoryImpl
import acab.devcon0.output.repository.federationmember.CidsRepositoryImpl
import acab.devcon0.output.repository.federationmember.InformationRepositoryImpl
import cats.effect.IO

object FederationMemberIoC {

  object Input {

    val checkInListener: FederationMemberCheckInListener = new FederationMemberCheckInListener(
      p2pListener = CommonsIoC.Input.p2pPubSubListener,
      p2pConfiguration = CommonsIoC.Configuration.configuration.p2p,
      updateInformationCommandHandler = Domain.Port.updateInformationCommandHandler,
      bounceSharingFolderUpdateCommandHandler = Domain.Port.bounceSharingFolderUpdateCommandHandler,
      checkInAckCommandHandler = Domain.Port.checkInAckCommandHandler
    )

    val heartbeatListener: FederationMemberHeartbeatListener = new FederationMemberHeartbeatListener(
      p2pListener = CommonsIoC.Input.p2pPubSubListener,
      p2pConfiguration = CommonsIoC.Configuration.configuration.p2p,
      updateInformationCommandHandler = Domain.Port.updateInformationCommandHandler,
      informationQueryHandler = Domain.Port.informationQueryHandler
    )

    val syncListener: FederationMemberSyncListener = new FederationMemberSyncListener(
      p2pListener = CommonsIoC.Input.p2pPubSubListener,
      p2pConfiguration = CommonsIoC.Configuration.configuration.p2p,
      bounceSharingFolderUpdateCommandHandler = Domain.Port.bounceSharingFolderUpdateCommandHandler,
      informationQueryHandler = Domain.Port.informationQueryHandler,
      syncCommandHandler = Domain.Port.syncCommandHandler
    )

    val sharingFolderUpdateP2pListener: p2ppubsub.FederationMemberSharingFolderUpdateListener = {
      new p2ppubsub.FederationMemberSharingFolderUpdateListener(
        p2pListener = CommonsIoC.Input.p2pPubSubListener,
        p2pConfiguration = CommonsIoC.Configuration.configuration.p2p,
        bounceSharingFolderUpdateCommandHandler = Domain.Port.bounceSharingFolderUpdateCommandHandler,
        informationQueryHandler = Domain.Port.informationQueryHandler
      )
    }

    val sharingFolderUpdateRedisListener: redispubsub.FederationMemberSharingFolderUpdateListener = {
      new redispubsub.FederationMemberSharingFolderUpdateListener(
        redisListener = CommonsIoC.Input.redisListener,
        sharingFolderUpdateCommandHandler = Domain.Port.updateChangelogCommandHandler,
        informationQueryHandler = Domain.Port.informationQueryHandler,
        redisPublisher = CommonsIoC.Output.redisPubSubPublisher,
        syncCommandHandler = Domain.Port.syncCommandHandler
      )
    }

    val changelogUpdateRedisListener: redispubsub.FederationMemberChangelogUpdateEventListener = {
      new FederationMemberChangelogUpdateEventListener(
        redisListener = CommonsIoC.Input.redisListener,
        ipfsCidUpdateCommandHandler = Domain.Port.ipfsCidUpdateCommandHandler,
        updateInformationRefreshCommandHandler = Domain.Port.updateInformationCommandHandler,
        redisPublisher = CommonsIoC.Output.redisPubSubPublisher,
        syncCommandHandler = Domain.Port.syncCommandHandler
      )
    }
  }

  object Domain {

    object Port {

      val syncCommandHandler: SyncCommandHandler = new SyncCommandHandlerImpl(
        p2PService = CommonsIoC.Domain.Service.p2pService,
        informationService = Service.informationService
      )

      val checkInAckCommandHandler: CheckInAckCommandHandler = new CheckInAckCommandHandlerImpl(
        p2PService = CommonsIoC.Domain.Service.p2pService,
        ipfsClusterService = IpfsIoC.Domain.Service.ipfsClusterService
      )

      val federationMembersInformationQueryHandler: federationmembersports.InformationQueryHandler =
        new adapters.federationmembers.InformationQueryHandlerImpl(
          service = Service.informationService
        )

      val informationQueryHandler: InformationQueryHandler = new InformationQueryHandlerImpl(
        informationService = Service.informationService
      )

      val bounceSharingFolderUpdateCommandHandler: BounceSharingFolderUpdateCommandHandler = {
        new BounceSharingFolderUpdateCommandHandlerImpl(
          redisPubSubPublisher = CommonsIoC.Output.redisPubSubPublisher
        )
      }

      val updateChangelogCommandHandler: SharingFolderUpdateCommandHandler = new SharingFolderUpdateCommandHandlerImpl(
        changelogService = Service.changelogService
      )

      val ipfsCidUpdateCommandHandler: IpfsCidDeltaUpdateCommandHandler = new IpfsCidDeltaUpdateCommandHandlerImpl(
        ipfsCidFacadeService = IpfsIoC.Domain.Service.facadeService,
        ipfsService = IpfsIoC.Domain.Service.ipfsService,
        cidsService = Service.cidsService,
        changelogService = Service.changelogService
      )

      val updateInformationCommandHandler: UpdateInformationCommandHandler = new UpdateInformationCommandHandlerImpl(
        ipfsClusterService = IpfsIoC.Domain.Service.ipfsClusterService,
        informationService = Service.informationService
      )
    }

    object Service {
      val informationService: InformationService[IO] = new InformationServiceImpl(
        repository = Output.informationRepository
      )

      val changelogService: ChangelogService[IO] = new ChangelogServiceImpl(
        repository = Output.changelogRepository
      )

      val cidsService: CidsService[IO] = new CidsServiceImpl(
        repository = Output.cidsRepository
      )
    }
  }

  object Output {
    val informationRepository: InformationRepository[IO] = new InformationRepositoryImpl(
      commandsApi = CommonsIoC.InputOutput.redisCommandsFactory()
    )

    val changelogRepository: ChangelogRepository[IO] = new ChangelogRepositoryImpl(
      commandsApi = CommonsIoC.InputOutput.redisCommandsFactory()
    )

    val cidsRepository: CidsRepository[IO] = new CidsRepositoryImpl(
      commandsApi = CommonsIoC.InputOutput.redisCommandsFactory()
    )
  }
}
