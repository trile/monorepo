package acab.devcon0
package configuration

import cats.effect.IO
import io.circe.Encoder
import io.circe.Json
import io.circe.generic.semiauto.deriveEncoder
import io.circe.syntax.EncoderOps
import io.libp2p.core.crypto.PrivKey

object ConfigurationCodecs {
  object Encoders {
    implicit val privateKey: Encoder[PrivKey] = (* : PrivKey) => Json.fromString("***")
    implicit val p2p: Encoder[P2pConfiguration] = (p2pConfiguration: P2pConfiguration) => {
      Json.obj(
        ("privateKey", Json.fromString("***")),
        ("swarmPort", Json.fromString(p2pConfiguration.swarmPort.toString)),
        ("peerId", Json.fromString(p2pConfiguration.peerId.toBase58))
      )
    }
    implicit val ipfs: Encoder[IpfsConfiguration]               = deriveEncoder[IpfsConfiguration]
    implicit val ipfsCluster: Encoder[IpfsClusterConfiguration] = deriveEncoder[IpfsClusterConfiguration]
    implicit val http: Encoder[HttpConfiguration]               = deriveEncoder[HttpConfiguration]
    implicit val configuration: Encoder[Configuration]          = deriveEncoder[Configuration]

    object Configuration {
      def apply(dto: Configuration): IO[String] = IO {
        EncoderOps[Configuration](dto).asJson.toString
      }
    }
  }
}
