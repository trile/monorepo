package acab.devcon0
package configuration
import java.util.Base64

import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import io.libp2p.core.PeerId
import io.libp2p.core.crypto.PrivKey
import io.libp2p.core.crypto.PubKey
import io.libp2p.core.pubsub.Topic
import io.libp2p.crypto.keys.Ed25519Kt

final case class P2pTopicsConfiguration(
    sync: Topic,
    nickname: Topic,
    sharingFolderUpdate: Topic,
    checkIn: Topic,
    heartbeat: Topic
)
final case class P2pConfiguration(
    privateKey: PrivKey,
    peerId: PeerId,
    swarmPort: Integer,
    topics: P2pTopicsConfiguration
)
final case class AuthConfiguration(joinToken: String)
final case class RedisConfiguration(host: String, port: Integer)
final case class IpfsClusterConfiguration(apiUrl: String)
final case class IpfsConfiguration(apiUrl: String)
final case class HttpConfiguration(port: Integer)
final case class Configuration(
    ipfs: IpfsConfiguration,
    ipfsCluster: IpfsClusterConfiguration,
    http: HttpConfiguration,
    redis: RedisConfiguration,
    p2p: P2pConfiguration,
    auth: AuthConfiguration,
    fedvarsAbsolutePath: String
)

object TrileControllerBackendConfigFactory {

  private val rawConfig: Config         = ConfigFactory.load("application.conf")
  private val redisConfig: Config       = rawConfig.getConfig("redis")
  private val appConfig: Config         = rawConfig.getConfig("app")
  private val ipfsConfig: Config        = appConfig.getConfig("ipfs")
  private val ipfsClusterConfig: Config = appConfig.getConfig("ipfs-cluster")
  private val httpConfig: Config        = appConfig.getConfig("http")
  private val p2pConfig: Config         = appConfig.getConfig("p2p")
  private val authConfig: Config        = appConfig.getConfig("auth")

  def build(): Configuration = {
    val ipfsConfiguration: IpfsConfiguration = IpfsConfiguration(
      apiUrl = ipfsConfig.getString("api-url")
    )

    val ipfsClusterConfiguration: IpfsClusterConfiguration = IpfsClusterConfiguration(
      apiUrl = ipfsClusterConfig.getString("api-url")
    )

    val httpConfiguration: HttpConfiguration = HttpConfiguration(
      port = httpConfig.getInt("port")
    )

    val redisConfiguration: RedisConfiguration = RedisConfiguration(
      host = redisConfig.getString("host"),
      port = redisConfig.getInt("port")
    )

    val p2pConfiguration: P2pConfiguration = getP2pConfiguration

    val authConfiguration: AuthConfiguration = AuthConfiguration(
      joinToken = authConfig.getString("join-token")
    )

    Configuration(
      ipfs = ipfsConfiguration,
      ipfsCluster = ipfsClusterConfiguration,
      http = httpConfiguration,
      redis = redisConfiguration,
      p2p = p2pConfiguration,
      auth = authConfiguration,
      fedvarsAbsolutePath = appConfig.getString("fedvars-absolute-path")
    )
  }

  private def getP2pConfiguration: P2pConfiguration = {
    val swarmKeyValue              = p2pConfig.getString("swarm-key-value")
    val p2pPrivateKeyValue: String = p2pConfig.getString("private-key")
    val p2pPrivateKey: PrivKey     = Ed25519Kt.unmarshalEd25519PrivateKey(Base64.getDecoder.decode(p2pPrivateKeyValue))
    val p2pPublicKey: PubKey       = p2pPrivateKey.publicKey()
    val p2pPeerId: PeerId          = PeerId.fromPubKey(p2pPublicKey)
    P2pConfiguration(
      privateKey = p2pPrivateKey,
      peerId = p2pPeerId,
      swarmPort = p2pConfig.getInt("swarm-port"),
      topics = P2pTopicsConfiguration(
        sync = TopicBuilder("FEDERATION_MEMBER_SYNC", swarmKeyValue),
        nickname = TopicBuilder("FEDERATION_MEMBER_NICKNAME", swarmKeyValue),
        sharingFolderUpdate = TopicBuilder("FEDERATION_MEMBER_SHARING_FOLDER_UPDATE", swarmKeyValue),
        checkIn = TopicBuilder("FEDERATION_MEMBER_CHECKIN", swarmKeyValue),
        heartbeat = TopicBuilder("FEDERATION_MEMBER_HEARTBEAT", swarmKeyValue)
      )
    )
  }
}
