package acab.devcon0.input.p2ppubsub

import java.nio.charset.StandardCharsets
import java.util.function.Consumer

import acab.devcon0.inputoutput.P2pBackend
import acab.devcon0.trile.domain.codecs.P2pCodecs
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId
import acab.devcon0.trile.domain.dtos.pubsub.P2p
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Data
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Listener.Mode
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Listener.Params
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message
import acab.devcon0.trile.domain.dtos.pubsub.P2p.MessageType
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Metadata
import cats.effect._
import cats.effect.std.Supervisor
import cats.effect.unsafe.IORuntime
import cats.implicits.catsSyntaxMonadError
import io.circe.Decoder
import io.libp2p.core.pubsub.MessageApi
import io.libp2p.core.pubsub.PubsubSubscription
import io.libp2p.core.pubsub.Topic
import io.libp2p.pubsub.gossip.Gossip
import io.netty.buffer.ByteBuf
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

enum ListenerMode:
  case Broadcast, Directed

final case class ListenerParams(topic: Topic, p2pMessageKeys: List[MessageType], listenerMode: ListenerMode)

class P2pListener(p2pBackend: P2pBackend[IO]) {

  private implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]
  private implicit val runtime: IORuntime = cats.effect.unsafe.IORuntime.global
  private val hostPeerId: P2pPeerId       = p2pBackend.getHostPeerId.toBase58

  def run[T <: Data: Decoder](listenerParams: Params, callback: Message[T] => IO[Unit]): IO[Unit] = {
    logger.info(s"Creating listener with params=$listenerParams") >>
      p2pBackend.getGossip
        .evalMap(gossip => subscribe(gossip, listenerParams, callback))
        .useForever
        .void
  }

  private def subscribe[T <: Data: Decoder](
      gossip: Gossip,
      listenerParams: Params,
      callback: Message[T] => IO[Unit]
  ): IO[PubsubSubscription] = {
    IO(
      gossip.subscribe(
        new Consumer[MessageApi] {
          override def accept(messageApi: MessageApi): Unit = {
            processMessage(messageApi, listenerParams, callback)
          }
        },
        listenerParams.topic
      )
    )
  }

  private def processMessage[T <: Data: Decoder](
      messageApi: MessageApi,
      listenerParams: Params,
      callback: Message[T] => IO[Unit]
  ): Unit = {
    Supervisor[IO](await = true)
      .use(supervisor => {
        (for message: Message[T] <- decode(messageApi)
        yield {
          lazy val matchingKey: Boolean          = isMatchingKey(listenerParams.messageTypes, message.meta)
          lazy val shouldConsumeMessage: Boolean = isShouldConsumeMessage(message.meta, listenerParams.mode)
          if matchingKey && shouldConsumeMessage then callback(message).unsafeRunAndForget()
        }).void
      })
      .unsafeRunAndForget()(runtime)
  }

  private def decode[T <: Data: Decoder](messageApi: MessageApi): IO[Message[T]] = {
    P2pCodecs.Decoders
      .Message[T](getString(messageApi.getData))
      .ensure(new RuntimeException("Drift in from"))(message => message.meta.from.equals(getString(messageApi.getFrom)))
  }

  private def isShouldConsumeMessage(metadata: Metadata, listenerMode: Mode): Boolean = {
    val maybeP2pPeerId: Option[P2pPeerId] = metadata.to

    lazy val notMyMessage: Boolean = isNotMyMessage(metadata)
    if listenerMode.equals(Mode.Broadcast) && notMyMessage then true
    else if listenerMode.equals(Mode.Directed) && maybeP2pPeerId.contains(hostPeerId) then true
    else false
  }

  private def isNotMyMessage(metadata: Metadata): Boolean = {
    !hostPeerId.equals(metadata.from)
  }

  private def isMatchingKey(p2pMessageKeys: List[MessageType], metadata: Metadata): Boolean = {
    p2pMessageKeys.contains(metadata.messageType)
  }

  private def getString(byteArray: Array[Byte]): String = {
    new String(byteArray.array, StandardCharsets.UTF_8)
  }

  private def getString(byteBuf: ByteBuf): String = {
    new String(byteBuf.array, StandardCharsets.UTF_8)
  }
}
