package acab.devcon0.input.p2ppubsub
import acab.devcon0.configuration.P2pConfiguration
import acab.devcon0.domain.dtos.FederationMemberInformation
import acab.devcon0.domain.dtos.pubsub
import acab.devcon0.domain.ports.input.federationmember.BounceSharingFolderUpdateCommand
import acab.devcon0.domain.ports.input.federationmember.BounceSharingFolderUpdateCommandHandler
import acab.devcon0.domain.ports.input.federationmember.BounceSharingFolderUpdateCommandImplicits.BounceSharingFolderUpdateCommandEventFlattenOps
import acab.devcon0.domain.ports.input.federationmember.InformationByP2pPeerIdQuery
import acab.devcon0.domain.ports.input.federationmember.InformationQueryHandler
import acab.devcon0.domain.ports.input.federationmember.SyncCommandHandler
import acab.devcon0.domain.ports.input.federationmember.SyncCommandImplicits.SyncAckCommandEventFlattenOps
import acab.devcon0.domain.ports.input.federationmember.SyncNackCommand
import acab.devcon0.trile.domain.codecs.P2pCodecs
import acab.devcon0.trile.domain.codecs.P2pCodecs.Decoders.sync
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId
import acab.devcon0.trile.domain.dtos.pubsub.P2p
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Listener.Mode
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Listener.Params
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberSync
import acab.devcon0.trile.domain.dtos.pubsub.P2p.MessageType
import cats.effect._
import cats.effect.unsafe.IORuntime
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class FederationMemberSyncListener(
    p2pListener: P2pListener,
    p2pConfiguration: P2pConfiguration,
    bounceSharingFolderUpdateCommandHandler: BounceSharingFolderUpdateCommandHandler,
    informationQueryHandler: InformationQueryHandler,
    syncCommandHandler: SyncCommandHandler
) {

  private implicit val runtime: IORuntime = cats.effect.unsafe.IORuntime.global
  private val logger: Logger[IO]          = Slf4jLogger.getLogger[IO]

  def run(): IO[Unit] = {
    for
      topic <- IO(p2pConfiguration.topics.sync)
      listenerParams = Params(
        topic = topic,
        messageTypes = List(MessageType.Request),
        mode = Mode.Directed
      )
      _ <- p2pListener.run[FederationMemberSync](listenerParams, message => triggerCommandInBackground(message))
    yield ()
  }

  private def triggerCommandInBackground(message: Message[FederationMemberSync]): IO[Unit] = IO {
    val syncMessage: FederationMemberSync = message.data
    (for
      _                           <- logger.info(s"Sync message received over P2P pub/sub. from=${message.meta.from}}")
      federationMemberInformation <- informationQueryHandler.handle(InformationByP2pPeerIdQuery(message.meta.from))
      _                           <- triggerBounceCommand(syncMessage, federationMemberInformation, message.meta.from)
    yield ()).unsafeRunAndForget()
  }

  private def triggerBounceCommand(
      syncMessage: FederationMemberSync,
      federationMemberInformation: FederationMemberInformation,
      p2pPeerId: P2pPeerId
  ): IO[Unit] = {
    val ipfsPeerIpnsUpdateMessage = pubsub.Redis.FederationMemberSharingFolderUpdateMessage(
      federationMemberInformation.id,
      syncMessage.sharedFolderCid,
      syncMessage.timestamp
    )
    val command = BounceSharingFolderUpdateCommand(ipfsPeerIpnsUpdateMessage)
    bounceSharingFolderUpdateCommandHandler
      .handle(command)
      .flattenEvents
      .onError(_ => syncCommandHandler.handle(SyncNackCommand(p2pPeerId)).flattenEvents)
  }
}
