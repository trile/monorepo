package acab.devcon0.input.p2ppubsub

import acab.devcon0.configuration.P2pConfiguration
import acab.devcon0.domain.dtos.FederationMemberInformation
import acab.devcon0.domain.dtos.pubsub
import acab.devcon0.domain.dtos.pubsub.Redis.FederationMemberSharingFolderUpdateMessage
import acab.devcon0.domain.ports.input.federationmember.CheckInAckCommandImplicits.CheckInAckCommandEventFlattenOps
import acab.devcon0.domain.ports.input.federationmember.UpdateInformationCommandImplicits.UpdateInformationCommandEventFlattenOps
import acab.devcon0.domain.ports.input.federationmember._
import acab.devcon0.trile.domain.codecs.P2pCodecs
import acab.devcon0.trile.domain.codecs.P2pCodecs.Decoders.checkIn
import acab.devcon0.trile.domain.dtos.pubsub.P2p
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Data
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Listener.Mode
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Listener.Params
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberCheckIn
import acab.devcon0.trile.domain.dtos.pubsub.P2p.MessageType
import cats.effect._
import cats.effect.unsafe.IORuntime
import io.circe.Decoder
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class FederationMemberCheckInListener(
    p2pListener: P2pListener,
    p2pConfiguration: P2pConfiguration,
    updateInformationCommandHandler: UpdateInformationCommandHandler,
    bounceSharingFolderUpdateCommandHandler: BounceSharingFolderUpdateCommandHandler,
    checkInAckCommandHandler: CheckInAckCommandHandler
) {

  private implicit val runtime: IORuntime = cats.effect.unsafe.IORuntime.global
  private val logger: Logger[IO]          = Slf4jLogger.getLogger[IO]

  def run(): IO[Unit] = {
    for
      topic <- IO(p2pConfiguration.topics.checkIn)
      listenerParams = Params(
        topic = topic,
        messageTypes = List(MessageType.Request),
        mode = Mode.Directed
      )
      _ <- p2pListener.run[FederationMemberCheckIn](listenerParams, message => triggerCommandInBackground(message))
    yield ()
  }

  private def triggerCommandInBackground[T <: Data: Decoder](message: Message[FederationMemberCheckIn]): IO[Unit] = IO {
    val p2pCheckInMessage = message.data
    (for
      _ <- logger.debug(s"Check in message received over P2P pub/sub. from=${message.meta.from}}")
      command = CheckInInformationCommand(p2pCheckInMessage)
      federationMemberInformation <- updateInformationCommandHandler.handle(command).flattenEvents
      _                           <- bounceMessage(p2pCheckInMessage, federationMemberInformation)
      command1 = CheckInAckCommand(p2pCheckInMessage.p2pPeerId)
      _ <- checkInAckCommandHandler.handle(command1).flattenEvents
    yield ()).unsafeRunAndForget()
  }

  private def bounceMessage(
      p2pCheckInMessage: FederationMemberCheckIn,
      federationMemberInformation: FederationMemberInformation
  ): IO[Unit] = {
    val ipfsPeerIpnsUpdateMessage = FederationMemberSharingFolderUpdateMessage(
      federationMemberInformation.id,
      p2pCheckInMessage.sharedFolderCid,
      p2pCheckInMessage.timestamp
    )
    val command: BounceSharingFolderUpdateCommand = BounceSharingFolderUpdateCommand(ipfsPeerIpnsUpdateMessage)
    bounceSharingFolderUpdateCommandHandler.handle(command).void
  }
}
