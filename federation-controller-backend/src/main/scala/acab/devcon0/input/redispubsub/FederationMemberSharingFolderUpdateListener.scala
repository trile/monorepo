package acab.devcon0.input.redispubsub

import acab.devcon0.domain.codecs.FederationMemberCodecs
import acab.devcon0.domain.dtos.FederationMemberInformation
import acab.devcon0.domain.dtos.pubsub.Redis.FederationMemberChangelogUpdateMessage
import acab.devcon0.domain.dtos.pubsub.Redis.FederationMemberSharingFolderUpdateMessage
import acab.devcon0.domain.ports.input.federationmember.SyncCommandImplicits.SyncAckCommandEventFlattenOps
import acab.devcon0.domain.ports.input.federationmember._
import acab.devcon0.domain.ports.output.publisher.RedisPublisher
import acab.devcon0.output.repository.redisutils.Redis
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import cats.effect._
import cats.effect.std.Supervisor
import cats.effect.unsafe.IORuntime
import dev.profunktor.redis4cats.data._
import fs2.Pipe
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class FederationMemberSharingFolderUpdateListener(
    redisListener: Listener,
    sharingFolderUpdateCommandHandler: SharingFolderUpdateCommandHandler,
    informationQueryHandler: InformationQueryHandler,
    redisPublisher: RedisPublisher[IO],
    syncCommandHandler: SyncCommandHandler
) {

  cats.effect.unsafe.IORuntime.global
  implicit val logger: Logger[IO]           = Slf4jLogger.getLogger[IO]
  private val channel: RedisChannel[String] = Redis.Channels.federationMemberSharingFolderUpdate

  def run(): Unit = {
    redisListener.run(channel, processMessage())
  }

  private def processMessage(): Pipe[IO, String, Unit] = stream => {
    stream
      .evalMap(FederationMemberCodecs.Decoders.RedisSharingFolderUpdateMessage(_))
      .evalTap(clientIpnsUpdateEvent => logReceivedMessage(clientIpnsUpdateEvent))
      .evalTap(ipfsPeerIpnsUpdateMessage => triggerCommandsInBackground(ipfsPeerIpnsUpdateMessage))
      .evalMap(* => IO.unit)
  }

  private def triggerCommandsInBackground(
      ipfsPeerIpnsUpdateMessage: FederationMemberSharingFolderUpdateMessage
  ): IO[Unit] = {
    Supervisor[IO](await = true).use { supervisor =>
      val federationMemberId: FederationMemberId = ipfsPeerIpnsUpdateMessage.federationMemberId
      (for
        federationMemberInformation <- queryFederationMemberInformation(federationMemberId)
        updateChangelogEvent       <- triggerUpdateChangelogItem(ipfsPeerIpnsUpdateMessage, federationMemberInformation)
        isUpdateChangelogProcessed <- isUpdateChangelogProcessed(updateChangelogEvent)
        _ <- publishMessage(federationMemberId, updateChangelogEvent, isUpdateChangelogProcessed)
      yield ()).onError(_ => reportSyncNackToMember(federationMemberId))
    }
  }

  private def reportSyncNackToMember(federationMemberId: FederationMemberId): IO[Unit] = {
    syncCommandHandler.handle(SyncNackCommand(federationMemberId)).flattenEvents
  }

  private def publishMessage(
      federationMemberId: FederationMemberId,
      federationMemberUpdateChangelogEvent: SharingFolderEvent[?],
      isUpdateChangelogProcessed: Boolean
  ): IO[Unit] = {
    (isUpdateChangelogProcessed, federationMemberUpdateChangelogEvent) match {
      case (true, event: SharingFolderSuccessEvent) =>
        val changelogItem = event.federationMemberChangelogItem
        val message       = FederationMemberChangelogUpdateMessage(federationMemberId, changelogItem)
        redisPublisher
          .publish(message)
          .onError(logError)
      case (_, _) =>
        syncCommandHandler.handle(SyncAckCommand(federationMemberId)).flattenEvents >>
          logger.info(s"Not publishing isUpdateChangelogProcessed=$isUpdateChangelogProcessed") >>
          IO.unit
    }
  }

  private def logError(throwable: Throwable): IO[Unit] = {
    logger.error(s"throwable=$throwable")
  }

  private def isUpdateChangelogProcessed(event: SharingFolderEvent[?]): IO[Boolean] = {
    event match
      case SharingFolderSuccessEvent(_)       => IO.pure(true)
      case SharingFolderKnownUpdateEvent()    => IO.pure(false)
      case SharingFolderErrorEvent(throwable) => IO.raiseError(throwable)
  }

  private def triggerUpdateChangelogItem(
      message: FederationMemberSharingFolderUpdateMessage,
      federationMemberInformation: FederationMemberInformation
  ): IO[SharingFolderEvent[?]] = {
    val command = SharingFolderUpdateCommand(message, federationMemberInformation)
    for commandResult <- sharingFolderUpdateCommandHandler.handle(command)
    yield commandResult
  }

  private def queryFederationMemberInformation(
      federationMemberId: FederationMemberId
  ): IO[FederationMemberInformation] = {
    informationQueryHandler.handle(InformationByIdQuery(federationMemberId))
  }

  private def logReceivedMessage(message: FederationMemberSharingFolderUpdateMessage): IO[Unit] = {
    logger.info(
      s"Message received & processed over Redis pub/sub. p2pPeerId=${message.federationMemberId} ipfsCid=${message.ipfsCid}}"
    )
  }
}
