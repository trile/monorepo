package acab.devcon0.input.redispubsub

import acab.devcon0.domain.codecs.FederationMemberCodecs
import acab.devcon0.domain.dtos.FederationIpfsCidDelta
import acab.devcon0.domain.dtos.pubsub.Redis.FederationIpfsCidsDeltaUpdateMessage
import acab.devcon0.domain.dtos.pubsub.Redis.FederationMemberIpfsCidsDeltaUpdateMessage
import acab.devcon0.domain.ports.input.federation.IpfsCidDeltaQuery
import acab.devcon0.domain.ports.input.federation.IpfsCidDeltaQueryHandler
import acab.devcon0.domain.ports.output.publisher.RedisPublisher
import acab.devcon0.output.repository.redisutils.Redis
import cats.effect._
import cats.effect.std.Supervisor
import cats.implicits.toTraverseOps
import dev.profunktor.redis4cats.data._
import fs2.Pipe
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class FederationMemberIpfsCidDeltaUpdateListener(
    redisListener: Listener,
    federationIpfsCidDeltaQueryHandler: IpfsCidDeltaQueryHandler,
    redisPublisher: RedisPublisher[IO]
) {

  cats.effect.unsafe.IORuntime.global
  implicit val logger: Logger[IO]           = Slf4jLogger.getLogger[IO]
  private val channel: RedisChannel[String] = Redis.Channels.federationMemberIpfsCidsDeltaUpdate
  private val chunkSize                     = 5000

  def run(): Unit = {
    redisListener.run(channel, processMessage())
  }

  private def processMessage(): Pipe[IO, String, Unit] = stream => {
    stream
      .evalMap(FederationMemberCodecs.Decoders.IpfsCidsDeltaUpdateMessage(_))
      .evalTap(logReceivedMessage)
      .evalTap(reportDelta)
      .evalMap(* => IO.unit)
  }

  private def reportDelta(message: FederationMemberIpfsCidsDeltaUpdateMessage): IO[Unit] = {
    for
      delta <- getFederationDelta(message)
      _     <- reportAdditions(delta)
      _     <- reportRemovals(delta)
    yield ()
  }

  private def reportAdditions(delta: FederationIpfsCidDelta): IO[Unit] = {
    Supervisor[IO](await = true).use { supervisor =>
      val messages = delta.additions
        .grouped(chunkSize)
        .map(cids => FederationIpfsCidsDeltaUpdateMessage(FederationIpfsCidDelta(additions = cids, removals = Set())))
        .toList
      supervisor.supervise(messages.traverse(redisPublisher.publish))
    }.void
  }

  private def reportRemovals(delta: FederationIpfsCidDelta): IO[Unit] = {
    Supervisor[IO](await = true).use { supervisor =>
      val messages = delta.removals
        .grouped(chunkSize)
        .map(cids => FederationIpfsCidsDeltaUpdateMessage(FederationIpfsCidDelta(additions = Set(), removals = cids)))
        .toList
      supervisor.supervise(messages.traverse(redisPublisher.publish))
    }.void
  }

  private def getFederationDelta(message: FederationMemberIpfsCidsDeltaUpdateMessage): IO[FederationIpfsCidDelta] = {
    val query: IpfsCidDeltaQuery = IpfsCidDeltaQuery(
      delta = message.delta
    )
    federationIpfsCidDeltaQueryHandler.handle(query)
  }

  private def logReceivedMessage(message: FederationMemberIpfsCidsDeltaUpdateMessage): IO[Unit] = {
    val additions: Int = message.delta.additions.size
    val removals: Int  = message.delta.removals.size
    logger.info(s"Message received & processed over Redis pub/sub. additions=$additions removals=$removals")
  }
}
