package acab.devcon0.input.http

import acab.devcon0.domain.codecs.IpfsCidCodecs
import acab.devcon0.domain.codecs.IpfsCidCodecs.Encoders.dto
import acab.devcon0.domain.dtos.IpfsCidDto
import acab.devcon0.domain.dtos.IpfsCidMetadataDto
import acab.devcon0.domain.dtos.SearchParameters
import acab.devcon0.domain.ports.input.ipfscid._
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import cats.effect._
import org.http4s._
import org.http4s.circe.CirceEntityEncoder.circeEntityEncoder
import org.http4s.dsl.io._
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

object SearchTermQueryParamMatcher extends QueryParamDecoderMatcher[String]("q")
object SearchPageQueryParamMatcher extends QueryParamDecoderMatcher[Int]("page")

class IpfsCidRoute(
    ipfsCidTreeQueryHandler: TreeQueryHandler,
    ipfsCidFlatQueryHandler: FlatQueryHandler,
    ipfsCidMetadataQueryHandler: MetadataQueryHandler,
    ipfsCidSearchQueryHandler: SearchQueryHandler
) {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  val routes: HttpRoutes[IO] = HttpRoutes
    .of[IO] {
      case GET -> Root / "file-tree" / cid =>
        (for
          ipfsCidFileTree <- getIpfsCidFileTreeWithLeaves(cid)
          response        <- Ok(ipfsCidFileTree)
        yield response).handleErrorWith(_ => BadRequest())

      case GET -> Root / "node-tree" / cid =>
        (for
          ipfsCidFileTree <- getIpfsCidFileTreeNodesOnly(cid)
          response        <- Ok(ipfsCidFileTree)
        yield response).handleErrorWith(_ => BadRequest())

      case GET -> Root / "flat" / cid =>
        (for
          maybeIpfsCidDto <- getIpfsCid(cid)
          response <- maybeIpfsCidDto match {
            case Some(ipfsCid) => IpfsCidCodecs.Encoders.Json.Dto(ipfsCid).flatMap(Ok(_))
            case None          => NotFound()
          }
        yield response).handleErrorWith(_ => BadRequest())

      case GET -> Root / "meta" :? SearchTermQueryParamMatcher(searchTerm) +& SearchPageQueryParamMatcher(searchPage) =>
        (for
          searchResult <- ipfsCidSearchQueryHandler.handle(SearchQuery(SearchParameters(searchTerm, searchPage)))
          response     <- IpfsCidCodecs.Encoders.Json.SearchResponse(searchResult).flatMap(Ok(_))
        yield response).handleErrorWith {
          case throwable: IllegalArgumentException => BadRequest(throwable.getMessage)
          case _                                   => InternalServerError()
        }

      case GET -> Root / "meta" / cid =>
        (for
          maybeIpfsCidDto <- getIpfsCidMetadata(cid)
          response <- maybeIpfsCidDto match {
            case Some(ipfsCid) => IpfsCidCodecs.Encoders.Json.Metadata(ipfsCid).flatMap(Ok(_))
            case None          => NotFound()
          }
        yield response).handleErrorWith(_ => BadRequest())
    }

  private def getIpfsCidMetadata(ipfsCid: IpfsCid): IO[Option[IpfsCidMetadataDto]] = {
    val query = MetadataQuery(ipfsCid)
    ipfsCidMetadataQueryHandler
      .handle(query)
      .onError(logError)
  }

  private def logError(throwable: Throwable): IO[Unit] = {
    logger.error(s"throwable=$throwable")
  }

  private def getIpfsCid(ipfsCid: IpfsCid): IO[Option[IpfsCidDto]] = {
    val query = FlatQuery(ipfsCid)
    ipfsCidFlatQueryHandler
      .handle(query)
      .onError(logError)
  }

  private def getIpfsCidFileTreeNodesOnly(ipfsCid: IpfsCid): IO[IpfsCidDto] = {
    val query = NodesTreeQuery(ipfsCid)
    ipfsCidTreeQueryHandler
      .handle(query)
      .flatTap(logResult)
      .onError(logError)
  }

  private def logResult(ipfsCidDto: IpfsCidDto): IO[Unit] = {
    logger.debug(s"ipfsCidDto=$ipfsCidDto")
  }

  private def getIpfsCidFileTreeWithLeaves(ipfsCid: IpfsCid): IO[IpfsCidDto] = {
    val query = FileTreeQuery(ipfsCid)
    ipfsCidTreeQueryHandler
      .handle(query)
      .onError(logError)
  }
}
