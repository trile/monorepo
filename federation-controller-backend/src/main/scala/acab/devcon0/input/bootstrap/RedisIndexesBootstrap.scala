package acab.devcon0.input.bootstrap
import acab.devcon0.domain.service.ipfscid.SearchNameService
import cats.effect._
import cats.effect.unsafe.IORuntime

class RedisIndexesBootstrap(searchNameService: SearchNameService[IO]) {

  private implicit val runtime: IORuntime = cats.effect.unsafe.IORuntime.global

  def run(): Unit = {
    searchNameService
      .createIndex()
      .recoverWith(_ => IO.unit)
      .unsafeRunAndForget()
  }
}
