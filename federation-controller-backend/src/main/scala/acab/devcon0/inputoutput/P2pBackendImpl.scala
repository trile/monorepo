package acab.devcon0.inputoutput

import java.nio.charset.StandardCharsets
import java.util.UUID
import java.util.concurrent.TimeUnit

import scala.concurrent.duration.Duration
import scala.concurrent.duration.FiniteDuration

import acab.devcon0.configuration.P2pConfiguration
import acab.devcon0.trile.domain.codecs.P2pCodecs
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Data
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Metadata
import cats.effect._
import com.google.protobuf.ByteString
import io.circe.Encoder
import io.libp2p.core.Host
import io.libp2p.core.PeerId
import io.libp2p.core.dsl.BuilderJ
import io.libp2p.core.dsl.HostBuilder
import io.libp2p.core.mux.StreamMuxerProtocol
import io.libp2p.core.pubsub.Topic
import io.libp2p.pubsub.DefaultPubsubMessage
import io.libp2p.pubsub.gossip.Gossip
import io.libp2p.pubsub.gossip.GossipParams
import io.libp2p.pubsub.gossip.GossipRouter
import io.libp2p.pubsub.gossip.builders.GossipRouterBuilder
import io.libp2p.security.noise.NoiseXXSecureChannel
import io.libp2p.transport.tcp.TcpTransport
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger
import pubsub.pb.Rpc
import pubsub.pb.Rpc.Message

class P2pBackendImpl(p2pConfiguration: P2pConfiguration) extends P2pBackend[IO] {

  private val maxConnectionDuration: FiniteDuration = Duration(5, TimeUnit.SECONDS)
  private val logger: Logger[IO]                    = Slf4jLogger.getLogger[IO]
  private val gossipRouter: GossipRouter            = getRouter
  val gossip: Gossip                                = Gossip(gossipRouter)
  private val host: Host                            = getHost
  private val hostPeerIdBytes: ByteString = ByteString.copyFrom(host.getPeerId.toBase58, StandardCharsets.US_ASCII)

  override def init: IO[Unit] = {
    (for _ <- initHost
    yield ()).handleErrorWith(_ => init.delayBy(Duration(1, TimeUnit.SECONDS)))
  }

  override def getGossip: Resource[IO, Gossip] = {
    Resource.make(IO(gossip))(_ => IO.unit)
  }

  override def getHostPeerId: PeerId = {
    host.getPeerId
  }

  override def publishMessage[T <: Data: Encoder](data: T, topic: Topic, metadata: Metadata): IO[Unit] = {
    for
      message <- P2pCodecs.Encoders.Message[T](data, metadata)
      rpcMessage = buildRcpMessage(message, topic)
      _ <- IO.fromCompletableFuture(IO(gossipRouter.publish(new DefaultPubsubMessage(rpcMessage)))).void
    yield ()
  }

  private def buildRcpMessage(stringMessage: String, topic: Topic): Message = {
    Rpc.Message
      .newBuilder()
      .setFrom(hostPeerIdBytes)
      .addTopicIDs(topic.getTopic)
      .setData(ByteString.copyFrom(stringMessage, StandardCharsets.US_ASCII))
      .setSeqno(ByteString.copyFrom(UUID.randomUUID().toString, StandardCharsets.US_ASCII))
      .build()
  }

  private def initHost: IO[Unit] = {
    IO(host.start().join())
      .timeout(maxConnectionDuration)
      .flatTap(_ => logger.info(s"P2P host started for peerId=${host.getPeerId}"))
      .onError(throwable => logger.error(s"Failed to start p2p host throwable=$throwable"))
      .void
  }

  private def getRouter: GossipRouter = {
    val gossipRouterBuilder = GossipRouterBuilder()
    gossipRouterBuilder.setParams(GossipParams())
    gossipRouterBuilder.build()
  }

  private def getHost: Host = {
    new HostBuilder()
      .builderModifier((builderJ: BuilderJ) => {
        builderJ.identity(identityBuilder => {
          identityBuilder.setFactory(() => p2pConfiguration.privateKey)
          kotlin.Unit.INSTANCE
        })
      })
      .transport(new TcpTransport(_))
      .secureChannel((privateKey, muxers) => new NoiseXXSecureChannel(privateKey, muxers))
      .muxer(() => StreamMuxerProtocol.getYamux)
      .protocol(gossip)
      .listen(s"/ip4/0.0.0.0/tcp/${p2pConfiguration.swarmPort}/p2p/${p2pConfiguration.peerId}")
      .build
  }
}
